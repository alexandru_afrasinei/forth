--public
Script.frequency=60--int "Frequency" 0 10000
Script.strength=0.8--float "Strength" 0 1
Script.Recursive=true--bool
Script.enabled=true--bool "Enabled"
--private

function Script:Start()
	self:SaveIntensity(self.entity,self.Recursive)
end

--Store the original intensity values
function Script:SaveIntensity(entity,recursive)
	entity:SetKeyValue("Flicker_Intensity",entity:GetIntensity())
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:SaveIntensity(entity:GetChild(n),true)
		end
	end
end

--Apply a modified intensity value
function Script:ApplyIntensity(entity,intensity,recursive)
	local i = entity:GetKeyValue("Flicker_Intensity")
	if i~="" then
		entity:SetIntensity(tonumber(i) * intensity)
	end
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:ApplyIntensity(entity:GetChild(n),intensity,true)
		end
	end
end

--Apply a modified intensity value
function Script:HideRecurse(entity, recurse)
	entity:Hide()
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:Hide()
		end
	end
end

function Script:ShowRecurse(entity, recurse)
	entity:Show()
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:Show()
		end
	end
end


function Script:Draw()
	if self.enabled == false then
		return
	end 
	if self.color==nil then
		self.color = self.entity:GetColor()
	end
	local t = Time:GetCurrent()
	local pulse = Math:Sin(t/100.0*self.frequency)*0.5*self.strength+(1.0-self.strength*0.5)
	self:ApplyIntensity(self.entity,pulse,self.Recursive)
end


function Script:Enable()--in
	-- GUARD CONDITIONS
	if self.enabled==true then return end

	self.enabled=true
	self.component:CallOutputs("Enable")
	self.health=1
end

function Script:Disable()--in
	-- GUARD CONDITIONS
	if self.enabled == true then return end

	self.enabled=false
	self.component:CallOutputs("Disable")
	self.health=0
end



function Script:On()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("AlarmLight:On") end
	if self.enabled ~= true then return end
	self:ShowRecurse(self.entity, recurse)
	self.component:CallOutputs("On")
end

function Script:Off()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("AlarmLight:Off") end
	if self.enabled ~= true then return end
	self:HideRecurse(self.entity, true)
	self.component:CallOutputs("Off")
end