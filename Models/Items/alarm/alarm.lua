--public
Script.enabled=true--bool "Enabled"
Script.health = 1 -- int "Health Value"
Script.onSoundFile="Models/Items/alarm/alarm-on.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.offSoundFile="Models/Items/alarm/alarm-off.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.debug = false-- bool "Debug"
Script.frequency=30--int "Frequency" 0 10000
Script.strength=0.95--float "Strength" 0 1
Script.Recursive=true--bool
Script.enabled=true--bool "Enabled"
Script.beginOn = true--bool "Begin On"
--private
Script.used = false
Script.itemType="alarm"

function Script:Start()
	self:SaveIntensity(self.entity,self.Recursive)
	self.sound={}
	self.startRotation = self.entity:GetRotation(true)
	self.startPosition = self.entity:GetPosition(true)
	--self.light=self.entity:FindChild("LIT_flashing")
	--if self.light == nil then
		--Debug:Error("LIT_flashing not found")
	--end

	if self.onSoundFile == nil then
		self.onSoundFile = "Models/Items/alarm/alarm-on.wav"
	end

	if self.onSoundFile then
		self.sound.on= Sound:Load(self.onSoundFile)
	end

	if self.offSoundFile == nil then
		self.offSoundFile = "Models/Items/alarm/alarm-off.wav"
	end

	if self.offSoundFile then
		self.sound.off= Sound:Load(self.offSoundFile)
	end


	self.entity:SetKeyValue("type", self.itemType)

	
	if self.sound ~=nil then
		self.onSoundSource = Source:Create()
		self.onSoundSource:SetSound(self.sound.on)
		self.onSoundSource:SetLoopMode(true)	
		self.onSoundSource:SetRange(50)
		self.offSoundSource = Source:Create()
		self.offSoundSource:SetSound(self.sound.on)
		self.offSoundSource:SetLoopMode(false)	
		self.offSoundSource:SetRange(50)
	end


	if self.beginOn then
		self:On()
	end


end

--Store the original intensity values
function Script:SaveIntensity(entity,recursive)
	entity:SetKeyValue("Flicker_Intensity",entity:GetIntensity())
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:SaveIntensity(entity:GetChild(n),true)
		end
	end
end

function Script:Use(usedBy)
	if self.debug then System:Print("Alarm:Use") end

	-- GUARD CONDITIONS
	if not self.enabled then
		if self.debug then System:Print("Use:DENIED enabled:"..tostring(self.enabled) ) end
		return true
	end

	--self.component:CallOutputs("GiveOutHealth")	
	self.component:CallOutputs("Use")
	self:ObjectUsed()	
	
	return false

end


function Script:Toggle()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("Alarm:Toggle") end
	if self.enabled ~= true then return end

	self.on = not self.on
	if self.on then
		self:On()
	else
		self:Off()
	end		

end


function Script:On()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("Alarm:On") end
	if self.enabled ~= true then return end
	if self.onSoundSource and self.offSoundSource:GetState()~=Source.Playing then self.entity:EmitSound(self.onSoundSource) end
	self.component:CallOutputs("On")
end

function Script:Off()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("Alarm:Off") end
	if self.enabled ~= true then return end
	if self.onSoundSource~=nil and self.onSoundSource:GetState()==Source.Playing then
		self.onSoundSource:Pause()
	end
	--if self.offSoundSource then self.entity:EmitSound(self.offSoundSource) end

	self.component:CallOutputs("Off")
end

function Script:Enable()--in
	-- GUARD CONDITIONS
	if self.enabled==true then return end

	self.enabled=true
	self.component:CallOutputs("Enable")
	self.health=1
end

function Script:Disable()--in
	-- GUARD CONDITIONS
	if self.enabled == true then return end

	self.enabled=false
	self.component:CallOutputs("Disable")
	self.health=0
end

--[[
function Script:ObjectUsed()
	self.used = true
	self.component:CallOutputs("AlarmUsed")
	--self.entity:Hide()
	-- Release object	
	--self:Release()
end
--]]

function Script:Release()

	--if self.sound then self.sound:Release() end
	-- Release additional resources
	if self.offSoundSource ~= nil then
		self.offSoundSource:Release()
		self.offSoundSource = nil
	end	
	if self.onSoundSource ~= nil then
		self.onSoundSource:Release()
		self.onSoundSource = nil
	end	

end


function Script:UpdateWorld()

	if window:KeyHit(Key.U) then
		System:Print("HIT U")
		self:On()
	end

	if window:KeyHit(Key.J) then
		System:Print("HIT J")
		self:Off()
	end

end

--[[
function Script:Draw()
	if self.color==nil then
		self.color = self.entity:GetColor()
	end
		
	local t = Time:GetCurrent()
	local pulse = Math:Sin(t/100.0*self.frequency)*0.5*self.strength+(1.0-self.strength*0.5)
	self:ApplyIntensity(self.entity,pulse,self.Recursive)
end


--Apply a modified intensity value
function Script:ApplyIntensity(entity,intensity,recursive)
	local i = entity:GetKeyValue("Flicker_Intensity")
	if i~="" then
		entity:SetIntensity(tonumber(i) * intensity)
	end
	if recursive then
		local n
		for n=0,entity:CountChildren()-1 do
			self:ApplyIntensity(entity:GetChild(n),intensity,true)
		end
	end
end
]]