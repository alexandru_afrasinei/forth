SHADER version 1
@OpenGL2.Vertex
#version 400

uniform mat4 projectionmatrix;
uniform mat4 drawmatrix;
uniform vec2 offset;
uniform vec2 position[4];

in vec3 vertex_position;

void main(void)
{
	gl_Position = projectionmatrix * (drawmatrix * vec4(position[gl_VertexID]+offset, 0.0, 1.0));
}
@OpenGLES2.Vertex

@OpenGLES2.Fragment

@OpenGL4.Vertex
#version 400

uniform mat4 projectionmatrix;
uniform mat4 drawmatrix;
uniform vec2 offset;
uniform vec2 position[4];

in vec3 vertex_position;

void main(void)
{
	gl_Position = projectionmatrix * (drawmatrix * vec4(position[gl_VertexID]+offset, 0.0, 1.0));
}
@OpenGL4.Fragment
#version 400

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform bool isbackbuffer;
uniform vec2 buffersize;
uniform float currenttime;

out vec4 fragData0;

void main(void)
{

vec2 icoord = vec2(gl_FragCoord.xy/buffersize);
if (isbackbuffer) icoord.y = 1.0 - icoord.y;

	
vec2 pixelsize = 15.0/buffersize;
	
	vec4 outputcolor = texture(texture1, icoord);
	
	float ao=0.0;
	for(int dx = -3 ; dx < 3 ; dx ++ ) {
		for(int dy = -3 ; dy < 3 ; dy ++ ) {
			fragData0.rgb += texture(texture0,icoord+vec2(pixelsize.x*float(dx+.5),pixelsize.y*float(dy+.5))).rgb;
		}
	}
	fragData0.rgb /= 36.0;
	fragData0.a=outputcolor.a;
}
