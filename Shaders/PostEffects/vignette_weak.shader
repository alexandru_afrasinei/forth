SHADER version 1
@OpenGL2.Vertex
#version 400

uniform mat4 projectionmatrix;
uniform mat4 drawmatrix;
uniform vec2 offset;
uniform vec2 position[4];

in vec3 vertex_position;

void main(void)
{
	gl_Position = projectionmatrix * (drawmatrix * vec4(position[gl_VertexID]+offset, 0.0, 1.0));
}
@OpenGLES2.Vertex

@OpenGLES2.Fragment

@OpenGL4.Vertex
#version 400

uniform mat4 projectionmatrix;
uniform mat4 drawmatrix;
uniform vec2 offset;
uniform vec2 position[4];

in vec3 vertex_position;

void main(void)
{
	gl_Position = projectionmatrix * (drawmatrix * vec4(position[gl_VertexID]+offset, 0.0, 1.0));
}
@OpenGL4.Fragment
//--------------------------------------
// Vignette shader by Josh Klint (based on Shadmar's nightvision shader)
//--------------------------------------

#version 400

uniform sampler2D texture1;
uniform bool isbackbuffer;
uniform vec2 buffersize;
uniform float currenttime;

out vec4 fragData0;

//Could possiibly be uniforms to control the effect
const float noiseamount = 0.5;                          //Amount of noise                                                       //
const vec2 lensRadius 	= vec2(0.65*2.5, 0.05);              //Radius and feathering of vignette                     //
const vec3 nvcol 	= vec3(1.1, 4.0, 1.1);          //Change these values for nightvision color

void main() 
{
	vec2 tcoord = vec2(gl_FragCoord.xy/buffersize);
	if (isbackbuffer) tcoord.y = 1.0 - tcoord.y;

        //Nightvision
        vec4 texcolor = texture(texture1, tcoord);
        
        //vignette
        float dist = distance(tcoord.xy, vec2(0.5,0.5));
        float vigfin = smoothstep(lensRadius.x, lensRadius.y, dist);

	//Render
	fragData0 = texcolor * vec4(vigfin,vigfin,vigfin, 1.0);
}
