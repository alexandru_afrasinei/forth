--[[
MDG Checkpoint Trigger
]]--
------------------------------------------------------------------------------
--public
Script.enabled=true				--bool		"Enabled"
Script.destination=nil			--entity	"Destination"
Script.particleDuration=1000	--int		"Partcle Time(ms)"
Script.startSoundFile="AddOns/MDG_Teleport/Sounds/teleport_start.wav"--path "Start Sound" "Wav Files (*.wav):wav"
Script.completeSoundFile="AddOns/MDG_Teleport/Sounds/teleport_complete.wav"--path "Complete Sound" "Wav Files (*.wav):wav"
Script.itemType="teleporter"	--string	"Type"
------------------------------------------------------------------------------
--private
--Script.itemType="teleporter"
Script.entered = false
Script.exited = false
Script.hadCollision = false
Script.destinationPos = nil
Script.activateParticles = false
Script.activationEmitter = "activation_particles"
Script.sound={}	-- Store sounds here in a structured table hierarchy.
Script.mode = "" -- use 'state machine' to track our current state
Script.name=""
------------------------------------------------------------------------------
function Script:Start()

	self.name = self.entity:GetKeyValue("name")

	if self.startSoundFile == nil then
		self.startSoundFile = "AddOns/MDG_Teleport/Sounds/teleport_start.wav"
	end
	if self.startSoundFile then
		self.sound.start = Sound:Load(self.startSoundFile)
	end
	if self.sound.start==nil then Debug:Error("Teleport START sound not loaded!") end

	if self.completeSoundFile == nil then
		self.completeSoundFile = "AddOns/MDG_Teleport/Sounds/teleport_complete.wav"
	end
	if self.completeSoundFile then
		self.sound.complete = Sound:Load(self.completeSoundFile)
	end
	if self.sound.complete==nil then Debug:Error("Teleport COMPLETE sound not loaded!") end

	-- Find child particle emitter so we can turn on and off
	self.activationEmitter = tolua.cast(self.entity:FindChild(self.activationEmitter), "Emitter")
	if self.activationEmitter==nil then Debug:Error("Could not locate 'activationEmitter'!") end
	self.activationEmitter:Pause()
	self.activationEmitter:SetLoopMode(false)

	if self.destination==nil then Debug:Error("Teleport: destination not set!") end
	if self.destination ~= nil then
		-- 	self.respawnPoint = Vec3(self.entity:GetPosition().x, self.entity:GetPosition().y, self.entity:GetPosition().z)
		local temp = self.destination:GetPosition(true)
		--System:Print("x = "..temp.x)
		--System:Print("x = "..temp.x.." y= "..temp.y.." z="..temp.z)
		self.destinationPos = temp
	end

	self.entity:SetKeyValue("itemType", self.itemType)			
end
------------------------------------------------------------------------------
function Script:UpdatePhysics()
    if self.entered then
        if self.hadCollision == false then
            if self.exited == false then
                self.exited = true
                self.component:CallOutputs("OnExit")
                self.entered = false
				self.enabled = true
				--System:Print("enabled")
            end
        end
    end

    self.hadCollision = false
end
------------------------------------------------------------------------------
function Script:Collision(entity, position, normal, speed)
    self.hadCollision = true
	
    self.component:CallOutputs("OnCollide")

    if self.entered == false then
		if self.enabled == true then
			local collidedWith = entity:GetKeyValue("itemType")
			--System:Print("collidedWith="..collidedWith)
			--if  collidedWith == "player" 
				--or collidedWith == "enemy" 
			--then
				self.component:CallOutputs("OnEnter")
				--System:Print("Collision")					
				self.destination.script.enabled = false
				self:Teleport(entity)				
			--end	
		end

		self.entered = true
		self.exited = false	
	end
end
------------------------------------------------------------------------------
-- TELEPORT
function Script:Teleport(player)
	--System:Print("TELEPORT")
	self:PlaySound(self.sound.start)
	
	-- start particles at origin and destination
	self.destination.script:StartParticles()
	self:StartParticles()

	local temp = self.destinationPos
	--System:Print("x = "..temp.x.." y= "..temp.y.." z="..temp.z)
	player:SetPosition(self.destinationPos, true)
	self:PlaySound(self.sound.complete)

end
------------------------------------------------------------------------------
-- PLAY SOUND
function Script:PlaySound(sound)
	self.entity:EmitSound(sound, 50, 0.75, 1, false)
end
------------------------------------------------------------------------------
-- START PARTICLES
function Script:StartParticles()
	local result = self.activationEmitter:GetPaused()
	--System:Print("StartParticles - particles paused ="..tostring(result))
	self.activationEmitter:Reset()
	self.activationEmitter:Play()	
end
------------------------------------------------------------------------------