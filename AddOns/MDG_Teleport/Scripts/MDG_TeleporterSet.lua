--[[
MDG Teleporter Set
]]--
-- NOTE: This item may be incomplete.  Needs review.
------------------------------------------------------------------------------
--imports
import "Addons/MDG_Shared/Scripts/Functions/EntitySelection.lua"
------------------------------------------------------------------------------
--public
Script.enabled=true					--bool		"Enabled"
Script.teleportSetMode="children"	--choiceedit	"AutoScan Telports" findAll,children
Script.sourceEntity=nil				--entity	"Source Entity"
Script.sendEntity=nil				--entity	"Send Entity"
------------------------------------------------------------------------------
--private
Script.itemType="teleport_controller"
Script.teleporters={}
------------------------------------------------------------------------------
function Script:Start()
	self.entity:SetKeyValue("itemType", self.itemType)

	--self:AreaScanForTeleporters()
	--self:ChildScanForTeleporters()
	self:WorldScanForTeleporters()
end
------------------------------------------------------------------------------
function Script:AreaScanForTeleporters()

	System:Print("AreaScanForTeleporters")

	local radius = 1000
	--local entities = GetEntityNeighbors(self.entity, radius, true)
	local entities = GetEntityNeighbors(self.entity, radius, false)

	for e=1, #entities do

		local item = entities[e]
		local itemType = item:GetKeyValue("itemType")
		local itemName = item:GetKeyValue("name")
		--System:Print("itemName="..itemName.." itemType="..itemType)

		if (itemType == "teleporter") then
			System:Print("teleporter added")
			self.teleporters[e] = item
		end
	end
end
------------------------------------------------------------------------------
function Script:ChildScanForTeleporters()

	System:Print("ChildScanForTeleporters")

	-- We assume they will never disappear so the reference will always be valid
	local numItems = self.entity:CountChildren()
	--System:Print("numItems="..string(numItems))
	for i=0,numItems-1 do
		local item = self.entity:GetChild(i)
		local itemType = item:GetKeyValue("itemType")
		local itemName = item:GetKeyValue("name")
		--System:Print("itemName="..itemName.." itemType="..itemType)
		if (itemType == "teleporter") then
			System:Print("teleporter added")
			self.teleporters[i] = item
		end
	end	
end
------------------------------------------------------------------------------
function Script:WorldScanForTeleporters()
	
	System:Print("WorldScanForTeleporters")
	-- clear existing array
	self.teleporters={}

	-- We assume they will never disappear so the reference will always be valid
	local numItems = world:CountEntities()
	--System:Print("numItems="..string(numItems))
	for i=0,numItems-1 do
		local item = world:GetEntity(i)
		local itemType = item:GetKeyValue("itemType")
		local itemName = item:GetKeyValue("name")
		System:Print("** itemName="..itemName.." itemType="..itemType)
		if (itemType == "teleporter") then
			System:Print("teleporter added")
			self.teleporters[i] = item
		end
	end	
end
------------------------------------------------------------------------------
function Script:Enable()--in
	self.enabled = true
end
------------------------------------------------------------------------------
function Script:Disable()--in
	self.enabled = false
end
------------------------------------------------------------------------------
function Script:UpdatePhysics()
	if self.enabled then
		if window:KeyHit(Key.D1) then self:AreaScanForTeleporters() end
		if window:KeyHit(Key.D2) then self:ChildScanForTeleporters() end
		if window:KeyHit(Key.D3) then self:WorldScanForTeleporters() end
		if window:KeyHit(Key.D4) then
			local teleporter = ChooseNearestEntityType(self.sourceEntity, self.sendEntity, "teleporter", 10000)
			if teleporter==nil then Debug:Error("No Teleport found!") end
			local newPos = teleporter:GetPosition()
			self.sendEntity:SetPosition(newPos)
		end
	end
end
------------------------------------------------------------------------------
--[[
function Script:ChooseNearestTeleporter(sourceEntity, sendEntity, sightRadius)

	if (sourceEntity == nil) then sourceEntity = self.entity end

	local nearest = -1
	local nearestEntity = nil
	local entities = GetEntityNeighbors(sendEntity,sightRadius,false)
	local k,entity
	for k,entity in pairs(entities) do
		local itemType = entity:GetKeyValue("itemType")
		local itemName = entity:GetKeyValue("name")
		System:Print("** itemName="..itemName.." itemType="..itemType)
		if itemType~=nil and itemType=="teleporter" then
			local d = sourceEntity:GetDistance(entity)
			if nearest==-1 or d < nearest then 
				nearest=d
				nearestEntity = entity
			end
		end
	end

	return nearestEntity
end
]]--