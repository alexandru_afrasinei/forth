--imports
import "Scripts/Functions/GetEntityNeighbors.lua"

--public
Script.enabled = true --bool "Enabled"
Script.fireDelay = 3000 --int "Spawn Delay"
Script.projectileprefabpath="Prefabs/Projectiles/tracer.pfb"--path "Projectile" "Prefab (*.pfb):pfb
Script.lifetime=5000--int "Lifetime"

--private
Script.projectileprefab = nil
Script.lastshoottime = 0
Script.fireDelayTimeStart = 0
Script.starttime = 0

function Script:Start()

	self.projectileprefab = Prefab:Load(self.projectileprefabpath)
	if self.projectileprefab~=nil then
		self.projectileprefab:Hide()
	end
	if self.entity:GetMass()==0 then
		--self.entity:SetMass(10)
	end
end


function Script:Detach()
	if self.projectileprefab~=nil then
		self.projectileprefab:Release()
		self.projectileprefab=nil
	end

end

--[[
function Script:Release()

end
--]]

-- Update the state of the world
function Script:UpdateWorld()

	if window:KeyHit(Key.G) then
		System:Print("KEY G HIT")
		--self:SpawnBullet(0, Vec3(1,0,1));
		self:Fire()
	end
	
	-- Remove the explosion when it has hit its lifetime limit
	if Time:GetCurrent()-self.starttime>self.lifetime then
		System:Print("Release explosion object")
		self.entity:Release()
	end
end

-- Let physics relate to the world
function Script:UpdatePhysics()
	


end

function Script:Fire()

	if self.fireDelayTimeStart == 0 
		or (Time:GetCurrent() > (self.fireDelayTimeStart + self.fireDelay)) then
		self:DummyFire()
		self.fireDelayTimeStart =  Time:GetCurrent()	
	end
												
end

function Script:DummyFire() 
	System:Print("DUMMY FIRE")
	if self.projectileprefab~=nil then
		System:Print("SHOW EXPLOSION")
		local projectile = self.projectileprefab:Instance()
		projectile:Show()
		projectile:SetPosition(0,0,0,true)
	end
end