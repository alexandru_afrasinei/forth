--[[
MDG - Explosion Expire
]]--
------------------------------------------------------------------------------
--imports
import "Scripts/Functions/GetEntityNeighbors.lua"
------------------------------------------------------------------------------
--public 
Script.enabled=true 		--bool		"Enabled"
Script.timeOwnDeath=true	--bool		"Time own Death"
Script.lifetime=500			--int		"Lifetime"
------------------------------------------------------------------------------
--private
Script.starttime = 0
------------------------------------------------------------------------------
function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
	end
end
------------------------------------------------------------------------------
function Script:Disable()--in
	if self.enabled==true then
		self.enabled=false
	end
end
------------------------------------------------------------------------------
function Script:Start()
	
	self.starttime = Time:GetCurrent()
	self.entity:Show()

end
------------------------------------------------------------------------------
-- Update the state of the world
function Script:UpdateWorld()

	if self.enabled==false then return end
	if self.entity:Hidden() then return end

	-- Remove the explosion when it has hit its lifetime limit
	if self.timeOwnDeath then
		if self.entity and Time:GetCurrent()-self.starttime>self.lifetime then
			System:Print("Release object")
			self:Disable()
			self.entity:Hide()
			self.entity:Release()
		end
	end
end
------------------------------------------------------------------------------