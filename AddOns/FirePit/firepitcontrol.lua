function Script:Start()
		self.fluctuation=1.0
        self.smoothedfluctuation=1.0
        self.source = Source:Create() 
		sound=Sound:Load("Addons/FirePit/fire_loop_01.wav")
        self.source:SetSound(sound) 
        sound:Release() 
        self.source:SetLoopMode(true) 
		self.source:SetRange(5)
		self.source:SetVolume(.2)
        self.source:Play() 
		--self.source:SetPosition(self.entity:GetPosition()) 
end


function Script:UpdateWorld()
		--if self.fluctuation==nil then self:Start() end 
		self.fluctuation=self.fluctuation+math.random(-100,100)/50.0*Time:GetSpeed()
        self.fluctuation=math.min(1.8,self.fluctuation)
        self.fluctuation=math.max(0.2,self.fluctuation)
		local pitlightmove = Math:Curve(self.fluctuation,self.smoothedfluctuation,5/Time:GetSpeed()*110)
        self.smoothedfluctuation=Math:Curve(self.fluctuation,self.smoothedfluctuation,5.0/Time:GetSpeed())
		self.entity:SetColor(1.0*self.smoothedfluctuation,0.6*self.smoothedfluctuation,0.25*self.smoothedfluctuation,1,0)
		--self.entity:SetPosition(0,1.0+pitlightmove*0.125,0,false)
		self.source:SetPosition(self.entity:GetParent():GetPosition())
end


--[[
function Script:UpdatePhysics()
	
end
]]--

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]--

--[[
function Script:Draw()
	
end
]]--

--[[
function Script:DrawEach(camera)
	
end
]]--

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]--

--This function will be called when the entity is deleted.
function Script:Detach()
	    self.source:Stop()
        self.source:Release()
end


--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]--