function Script:Start()
	--self.entity:SetParent(nil)
end


function Script:UpdateWorld()
	self.world=World:GetCurrent()
	if self.camera==nil then
		for i=0,self.world:CountEntities()-1 do
                if self.world:GetEntity(i):GetClass()==Object.CameraClass then
				self.camera=self.world:GetEntity(i)
                                tolua.cast(self.camera,"Camera")
                                break
                        end
                end
	end
	if self.camera then
		self.entity:Point(self.camera,1,1);
	end
end

--[[
function Script:UpdatePhysics()
	
end
]]--

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]--

--[[
function Script:Draw()
	
end
]]--

--[[
function Script:DrawEach(camera)
	
end
]]--

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]--

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]--

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]--