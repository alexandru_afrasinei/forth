Script.openstate=false--bool "Start Open"
Script.enabled = true --bool "Enabled"
Script.speed  = 10 --float "Speed"
Script.target = "" --entity "Target waypoint"
Script.originPosition = nil
Script.targetPosition = nil
Script.currentPosition = nil
Script.currentRotation = nil	
Script.moving = nil
Script.oldMass = nil
Script.pauseTime = 4000 --int "pauseTime(ms)"
Script.pauseTimer = 0
Script.manualactivation=false--bool "Manual activate"


function Script:Start()

	--Disable gravity
	self.entity:SetGravityMode(false)

	--Disable Use() function is manual activation is not allowed
	if self.manualactivation==false then self.Use=nil end	

	self.originalrotation = self.entity:GetRotation()

	self.oldMass = self.entity:GetMass()

	if self.entity:GetShadowMode()>0 then
		self.entity:SetShadowMode(2)--Light.Dynamic
	end

	if self.enabled then
		if self.entity:GetMass()==0 then
			Debug:Error("Entity mass must be greater than zero.")
		end
	else
		self.entity:SetMass(0)
	end
	self.entity:SetGravityMode(false) 	
	self.entity:SetCollisionType(Collision.Scene)

	if self.target == nil then
		Debug:Error("No target assigned")
	end
	
	self:SetTarget(self.target)
end

function Script:SetTarget(newTarget)
	self.currentPosition = self.entity:GetPosition()
	self.originPosition = self.entity:GetPosition()	
	self.targetPosition = newTarget:GetPosition()
	--self.distance = self.originPosition:DistanceToPoint(self.targetPosition)
	self.target = newTarget
	
	local pos = self.entity:GetPosition(true)
	local targetpos = self.target:GetPosition(true)
	local pin = pos - targetpos
	self.distance = pin:Length()
	pin = pin:Normalize()
	
	if self.joint then
		self.joint:DisableMotor()
		self.joint:Release()
	end
	self.joint=Joint:Slider(targetpos.x,targetpos.y,targetpos.z,pin.x,pin.y,pin.z,self.entity,nil)
	self.joint:EnableMotor()
	self.joint:SetMotorSpeed(self.speed)
	self.joint:SetAngle(-self.distance)
end

function Script:Enable()--in
	self.enabled = true
end

function Script:Disable()--in
	self.enabled = false
	self.entity:SetMass(0)
end

function Script:UpdatePhysics()
	if self.enabled then
		
		if self.pauseTimer>0 then
			local time = Time:GetCurrent()
			if time-self.pauseTimer>self.pauseTime then
				self:NextTarget()
			end
		end		
		
		--Calculate movement
		local currentpos = self.entity:GetPosition(true)
		local targetpos = self.target:GetPosition(true)
		local d = currentpos:DistanceToPoint(targetpos)
		if d<0.1 then		
			if self.pauseTimer == 0 then
				if self.pauseTime > 0 then
					System:Print("PAUSE")
					self:PauseMovement()
				else
					System:Print("NEXT TARGET 1")									
					self:NextTarget()
				end					
			end
		end
			
	else
		--self.entity:PhysicsSetPosition(self.currentPosition.x, self.currentPosition.y, self.currentPosition.z)
		--self.entity:PhysicsdSetRotation(self.currentRotation.x, self.currentRotation.y, self.currentRotation.z)
	end
end

function Script:PauseMovement()
	self.pauseTimer = Time:GetCurrent()
end

function Script:ResumeAfterPause()
	self:NextTarget()
end

function Script:NextTarget()
	System:Print("NEXT TARGET")
	System:Print("self.pauseTime=" .. self.pauseTime)	
	self.pauseTimer = 0	
	--When the target has been reached
	--self.entity:PhysicsSetPosition(self.targetPosition.x, self.targetPosition.y, self.targetPosition.z)
	--self.enabled = false
	self.component:CallOutputs("WaypointReached")
	--Check if the target that we have reached also has a target, which is then our new target/waypoint	
	if self.target.script.target ~= nil then	
		self:SetTarget(self.target.script.target)
	end

end

--Use function - this will be called when the player hits the "use" key when looking at this
function Script:Use()
	--Only allow this if the object is enabled
	if self.enabled then
		if self.openstate then
			--Make the door close
			self:Close()
		else
			--Make the door open
			self:Open()
		end
	end
end


function Script:Open()--in
	if self.enabled then
		self.opentime = Time:GetCurrent()
		if self.openstate==false then
			self.openstate=true			
			if self.opensound then
				self.entity:EmitSound(self.opensound)
			end
			self.joint:SetAngle(self.openangle)		
			self.component:CallOutputs("Open")
		end
	end
end

function Script:Close()--in
	if self.enabled then
		if self.openstate then
			self.openstate=false
			if self.loopsource then
				self.loopsource:Release()
				self.loopsource=nil
			end
			if self.closesound then
				self.entity:EmitSound(self.closesound)
			end
			self.joint:SetAngle(self.closedangle)
			self.component:CallOutputs("Close")
		end
	end
end