--[[
MDG - Test Spawn Item Script
]]--
------------------------------------------------------------------------------
--import
import "Scripts/Functions/ReleaseTableObjects.lua"
------------------------------------------------------------------------------
--public
Script.enabled=true	--bool	"Enabled"
Script.itemType="player"	--string "Item Type"
Script.lifetime = 2000		--int "Lifetime"
------------------------------------------------------------------------------
--private
Script.starttime=nil
------------------------------------------------------------------------------
function Script:Start()

	local refCount = self.entity:GetRefCount()
	System:Print("spawn item refCount = "..refCount)
	self.name = self.entity:GetKeyValue("name")
	self.name = self.entity:SetKeyValue("itemType", self.itemType)
	--System:Print("Starting item type:"..self.entity:GetKeyValue("itemType"))
	self.starttime=Time:GetCurrent()

end
------------------------------------------------------------------------------
function Script:ReleaseItem()
	System:Print("Release Object")

	if self.sound ~= nil then
		ReleaseTableObjects(self.sound)
	end
	if self.entity ~= nil then
		self.entity:Hide()
		self.entity:Release()
	end

end
------------------------------------------------------------------------------
function Script:UpdateWorld()

	if self.enabled==false then return end
	if self.entity:Hidden() then return end
	-- Remove the missile when it has hit its lifetime limit
	if self.lifetime > 0 and Time:GetCurrent()-self.starttime>self.lifetime then
		if self.entity then
			System:Print("Release projectile object")

			self:ReleaseItem()
		else
			System:Print("Entity no longer existing - Release not possible")
		end
	end
end
------------------------------------------------------------------------------