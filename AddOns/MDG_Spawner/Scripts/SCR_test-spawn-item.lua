--[[
MDG - Test Spawn Item Script
]]--
------------------------------------------------------------------------------
--import
import "Scripts/Functions/ReleaseTableObjects.lua"
------------------------------------------------------------------------------
--public
Script.enabled=true					--bool	"Enabled"
Script.itemType="test-spawn-item"	--string "Item Type"
------------------------------------------------------------------------------
--private
------------------------------------------------------------------------------
function Script:Start()

	self.name = self.entity:GetKeyValue("name")
	self.name = self.entity:SetKeyValue("itemType", self.itemType)
	System:Print("Spawning item type:"..self.entity:GetKeyValue("itemType"))
end
------------------------------------------------------------------------------