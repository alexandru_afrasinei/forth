--[[
MDG - Spawn Controller
]]--
------------------------------------------------------------------------------
--import
import "Scripts/Functions/ReleaseTableObjects.lua"
------------------------------------------------------------------------------
--public
Script.enabled=true				--bool		"Enabled"
Script.spawnTimerActive=true	--bool		"Timer Active"
Script.spawnDelay=2000			--int		"Spawn Delay(ms)"
Script.spawnMax=5				--int		"Spawn Max"
Script.lifeTime=5000			--int		"Lifetime(ms)"
Script.spawnPoint=nil			--entity	"Spawn Point"
Script.itemPrefabPath="AddOns/MDG_Spawner/Prefabs/pfb_test-spawn-item.pfb"--path "Spawn Item" "Prefab (*.pfb):pfb
Script.collisionsOff=false		--bool		"Collisions Off"
Script.disablePrefab=true		--bool		"Disable Prefab"
Script.enableInstances=true		--bool		"Enable Instances"
Script.destroyChildren=true		--bool		"Destroy Children"
--Script.noLifeTime=false			--bool		"No Life Time"
------------------------------------------------------------------------------
--private
Script.itemType="spawn_controller"
Script.spawnItems={}
Script.itemPrefab=nil 
Script.lastSpawnTime=0
Script.spawnLocationPos=nil
------------------------------------------------------------------------------
function Script:Start()

	self.name = self.entity:GetKeyValue("name")

	if self.itemPrefabPath == nil then
		self.itemPrefabPath = "AddOns/MDG_Spawner/Prefabs/pfb_test-spawn-item.pfb"
	end
	if self.itemPrefabPath then
		self.itemPrefab = Prefab:Load(self.itemPrefabPath)
		if self.disablePrefab and type(self.itemPrefab.script["lifeTime"])=="number" then
			self.itemPrefab.lifeTime = -1
		end		
		self.itemPrefab:AddRef()
		self.itemPrefab:Hide()
		if self.disablePrefab and type(self.itemPrefab.script["Disable"])=="function" then
			self.itemPrefab.script:Disable()
		end

	end
	if self.itemPrefab==nil then Debug:Error("Item pefab could not loaded!") end
	
	-- Set start spawn location
	if self.spawnPoint==nil then Debug:Error("Spawn Point must be set!") end
	self.spawnLocationPos = self.spawnPoint:GetPosition()

end
------------------------------------------------------------------------------
function Script:UpdateWorld()

	if self.enabled then
		-- Do Spawn if time
		local numSpawnItems = #self.spawnItems		
		if self.spawnTimerActive == true then
			if Time:GetCurrent() > self.lastSpawnTime + self.spawnDelay then
				self.lastSpawnTime = Time:GetCurrent()
				self:SpawnItem()
			end
		end

		if (window:KeyHit(Key.G)) then 
			self:SpawnItem()
		end

	end

	if #self.spawnItems > 0 then
		self:CheckExpiringItems()
	end

end
------------------------------------------------------------------------------
function Script:SpawnItem()--in

	local numSpawnItems = #self.spawnItems			
	if numSpawnItems >= self.spawnMax then return end
	System:Print("** SPAWN ITEM**")
	local item = self.itemPrefab:Instance()
	if self.enableInstances and type(item.script["Enable"])=="function" then
		item.script:Enable()
	end
	if self.enableInstances and type(item.script["timeOwnDeath"])=="boolean" then
		item.script.timeOwnDeath = not self.destroyChildren
	end	
	
	System:Print("**SET POSITION** x:"..self.spawnLocationPos.x.." y:"..self.spawnLocationPos.y.." z:"..self.spawnLocationPos.z)
	item:SetPosition(self.spawnLocationPos)
	if self.collisionsOff then
		item:SetCollisionType(Collision.None) 
	end
	item:Show()
	itemData = {}
	itemData.createTime = Time:GetCurrent()
	itemData.item = item
	self.spawnItems[numSpawnItems+1] = itemData 
end
------------------------------------------------------------------------------
function Script:CheckExpiringItems()
	--System:Print("CheckExpiringItems")
	if not self.destroyChildren then return end

	for i=1, #self.spawnItems do
		if self.spawnItems[i] then
			if Time:GetCurrent() > self.spawnItems[i].createTime + self.lifeTime then
				if self.spawnItems[i].item then
					self.spawnItems[i].item:Release()
					System:Print("RELEASE ITEM")
				else
					System:Print("NO ITEM TO RELEASE")
				end
				table.remove(self.spawnItems,i)
				System:Print("REMOVED ITEM")
			end
		end
	end

end
------------------------------------------------------------------------------
function Script:Release()
	ReleaseTableObjects(self.spawnItems)
end
------------------------------------------------------------------------------
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)

	if self.enabled then
		context:SetBlendMode(1)
		context:SetColor(1,0,0,1)
		
		--self:DisplayInfo(context , 2, 140)
	end	
end
------------------------------------------------------------------------------
function Script:DisplayInfo(context, x, y)

	-- Store previous colour
	local prevColor = context:GetColor()		
	local prevfont = context:GetFont()
	local separator = 15
	prevfont:AddRef()
	context:SetFont(self.smallfont)
	context:DrawText("self.name=" .. tostring(self.name) ,x,y)
	y = y + separator
	context:DrawText("self.itemType=" .. tostring(self.itemType) ,2,y)
	y = y + separator
	context:DrawText("#self.spawnItems=" .. #self.spawnItems,2,y)
	
	-- Restore previous settings
	context:SetFont(prevfont)
	context:SetColor(prevColor)	
	
end
------------------------------------------------------------------------------