--[[
MDG - Missile
]]--
------------------------------------------------------------------------------
--imports
import "Scripts/Functions/ReleaseTableObjects.lua"
import "Scripts/Functions/GetEntityNeighbors.lua"
import "AddOns/MDG_Missile/Scripts/Functions/ChooseTargets.lua"
------------------------------------------------------------------------------
--public 
Script.movespeed=-35		--float 	"Move Speed"
Script.pickradius=0			--int		"Pick Radius"
Script.damage=25			--float 	"Damage"
Script.damageRadius=1.25	--float		"Damage Radius"
Script.lifeTime=5000		--int 		"lifeTime"
Script.enabled=false		--bool 		"Enabled"
Script.explosionPrefabPath="Addons/MDG_Explosion/Prefabs/pfb_explosion.pfb"--path "Projectile" "Prefab (*.pfb):pfb
Script.explosionSoundPath="AddOns/MDG_Explosion/Sounds/explosion02.wav"--path "Projectile" "Prefab (*.pfb):pfb
Script.teamid=2--choice "Team" "Neutral,Good,Bad"
Script.timeOwnDeath=true	--bool		"Time Own Death"
------------------------------------------------------------------------------
--private
--Script.nolifeTime=false
Script.explosionPrefab=nil
Script.owner=nil
------------------------------------------------------------------------------
function Script:Start()
	self.sound={}	
	self.sound.ricochet={}
	self.sound.ricochet[1]=Sound:Load("Sound/Ricochet/bullet_impact_dirt_01.wav")
	self.sound.ricochet[2]=Sound:Load("Sound/Ricochet/bullet_impact_dirt_02.wav")
	self.sound.ricochet[3]=Sound:Load("Sound/Ricochet/bullet_impact_dirt_03.wav")
	self.starttime=Time:GetCurrent()
	--self.entity:EmitSound(self.sound.ricochet[1])

	if self.explosionSoundPath == nil then
		self.explosionSoundPath = "AddOns/MDG_Explosion/Sounds/explosion02.wav"
	end
	if self.explosionSoundPath then
		self.sound.explosion= Sound:Load(self.explosionSoundPath)
		--self.explosionSound:AddRef()
	end
	if self.sound.explosion==nil then Debug:Error("self.sound.explosion could not loaded!") end

	if self.explosionPrefabPath == nil then
		self.explosionPrefabPath = "AddOns/MDG_Spawner/Prefabs/pfb_test-spawn-item.pfb"
	end
	if self.explosionPrefabPath then
		self.explosionPrefab = Prefab:Load(self.explosionPrefabPath)
		self.explosionPrefab.lifeTime = -1
		self.explosionPrefab:AddRef()
		self.explosionPrefab:Hide()
		if self.disablePrefab and type(self.explosionPrefab.script["Disable"])=="function" then
			self.explosionPrefab.script:Disable()
		end
	end
	if self.explosionPrefab==nil then Debug:Error("Item pefab could not loaded!") end

end
------------------------------------------------------------------------------
function Script:Enable()--in

	System:Print("Projectile Enabled")
	System:Print("self.lifeTime="..self.lifeTime)
	
	if self.enabled==false then	
		self.enabled=true
	end
end
------------------------------------------------------------------------------
function Script:Disable()--in
	if self.enabled==true then
		System:Print("Projectile Disabled")
		self.enabled=false
	end
end
------------------------------------------------------------------------------
function Script:FindScriptedParent(entity,func)
	while entity~=nil do
		if entity.script then
			if type(entity.script[func])=="function" then
				return entity
			end
		end
		entity = entity:GetParent()
	end
	return nil
end
------------------------------------------------------------------------------
function Script:ReleaseItem()
	System:Print("Release Homing Missile")

	--if not self.timeOwnDeath then return end 

	if self.sound ~= nil then
		ReleaseTableObjects(self.sound)
	end
	if self.entity ~= nil then
		self.entity:Release()
	end

	if self.explosionPrefab~=nil then
		self.explosionPrefab:Release()
		self.explosionPrefab=nil
	end

end
------------------------------------------------------------------------------
function Script:UpdateWorld()
	if self.enabled==false then return end
	if self.entity:Hidden() then return end

	local pickinfo=PickInfo()	
	local pos = self.entity:GetPosition(true)
	-- Move to our next position we should be at given our speed
	local targetpos = Transform:Point(0,0,self.movespeed/60.0 * Time:GetSpeed(),self.entity,nil)
	-- Find stomething to hit
	local result = self.entity.world:Pick(pos,targetpos,pickinfo,self.pickradius,true,Collision.Projectile)
	if result then
		local damageTargets = ChooseTargets(self.entity, self.damageRadius)	
		-- STRUCTURE: entityData = {item=[entity], hit=[bool]}	
		for k,entityData in pairs(damageTargets) do
			self:DoHurt(entityData)
		end

		self:Explosion()
		
	else
		-- Didn't hit anything move to projected position
		self.entity:SetPosition(targetpos)
	end

	if not self.timeOwnDeath then return end 
	
	-- Remove the missile when it has hit its lifeTime limit	
	if self.lifeTime > 0 and Time:GetCurrent()-self.starttime>self.lifeTime then
		if self.entity then
			System:Print("Release expired projectile object")
			self:Disable()
			self:ReleaseItem()
		else
			System:Print("Entity no longer existing - Release not possible")
		end
	end
end
------------------------------------------------------------------------------
function Script:Explosion()

	-- Do Explosion
	if self.explosionPrefab~=nil then
		local explosion = self.explosionPrefab:Instance()
		explosion:SetPosition(self.entity:GetPosition())
		explosion:Show()
		-- Do explosion sound
		--explosion:EmitSound(self.sound.explosions[math.random(#self.sound.explosions)],30)
		explosion:EmitSound(self.sound.explosion,30)	
	end

	if self.entity then
		System:Print("Release exploded projectile object")
		self:ReleaseItem()
	else
		System:Print("Entity no longer existing - Release not possible")
	end			
		
end
------------------------------------------------------------------------------
function Script:DoHurt(entityData)

	local entityName = entityData.item:GetKeyValue("name")
	local hurtValue = self.damage
	-- If hit wasn't line of sight reduce damage significantly
	if not entityData.lineOfSightHit then
		hurtValue = hurtValue/4
	end

	System:Print("entityName="..entityName)

	local enemy = self:FindScriptedParent(entityData.item,"Hurt")
	if enemy then
		if enemy.script.health>0 then
			enemy.script:Hurt(self.damage,self.owner)
		end
	end

	local result = false
	if result then
		local enemy = self:FindScriptedParent(pickinfo.entity,"Hurt")
		if enemy then
			if self.owner then
				--if self.owner.teamid==enemy.script.teamid then
				--	result=false
				--end
			end
			if result then
				if enemy.script.health>0 then
					enemy.script:Hurt(self.damage,self.owner)
				end
			end	
		end

		-- DECAL-BEGIN
		--Bullet mark decal
		local mtl
		local scale = 0.1
		if enemy~=nil then
			mtl = Material:Load("Materials/Decals/wound.mat")
			scale = 0.1
		else
			if pickinfo.surface~=nil then
				local pickedmaterial = pickinfo.surface:GetMaterial()
				if pickedmaterial~=nil then
					rendermode = pickedmaterial:GetDecalMode()
				end
			end
			mtl = Material:Load("Materials/Decals/bulletmark.mat")
		end
		local decal = Decal:Create(mtl)
		decal:AlignToVector(pickinfo.normal,2)
		decal:Turn(0,0,Math:Random(0,360))
		decal:SetScript("Scripts/Objects/Effects/BulletMark.lua")
		if mtl~=nil then mtl:Release() end
		decal:SetPosition(pickinfo.position)
		decal:SetParent(pickinfo.entity)
		
		--Apply global scaling
		local mat = decal:GetMatrix()
		mat[0] = mat[0]:Normalize() * scale
		mat[1] = mat[1]:Normalize() * scale
		mat[2] = mat[2]:Normalize() * scale	
		decal:SetMatrix(mat)

		--Play sound
		decal:EmitSound(self.sound.ricochet[math.random(#self.sound.ricochet)],30)
		-- DECAL-END
	end

end
------------------------------------------------------------------------------