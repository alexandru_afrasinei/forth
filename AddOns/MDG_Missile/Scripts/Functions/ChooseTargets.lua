function ChooseTargets(searchingEntity, pickRadius)

	-- local obj structure
	-- {item=[entity], lineOfSightHit=[bool]}	
	local targets={}

	--local entities = GetEntityNeighbors(self.entity,self.pickRadius,true)
	local entities = GetEntityNeighbors(searchingEntity,pickRadius,true)
	local k,entity
	for k,entity in pairs(entities) do
		--if entity.script.teamid~=nil and entity.script.teamid~=0 and entity.script.teamid~=self.teamid then
			--if entity.script.health>0 then
				local pos = Transform:Point(entity:GetPosition(true),nil,searchingEntity)
				-- Check if entity is too high above us
				--if pos.z<0 or entity:GetDistance(searchingEntity)<pickRadius then
					local d = searchingEntity:GetDistance(entity)
					-- check if we have a LOS to entity
					local pickinfo=PickInfo()
					local hit = false
					if searchingEntity.world:Pick(searchingEntity:GetPosition()+Vec3(0,0,0),entity:GetPosition()+Vec3(0,1.6,0),pickinfo,0,false,Collision.LineOfSight)==false then
						--return entity.script
						hit = true
					end
					local target = {}
					target.item = entity
					target.lineOfSightHit = hit
					targets[#targets+1] = target
				--end
			--end
		--end
	end
	return targets
end