-- public 
--Script.soundfileLock=""--path "Off Material" "Material Files (*.mat):mat"
Script.materialOn=""--path "On Material" "Material Files (*.mat):mat"
--private 
Script.materials={}

function Script:Start()
	if self.materialOn ~= nil then
		self.materials.on  = Material:Load(self.materialOn)
		self.entity:SetMaterial(self.materials.on)
	end	
end


--[[
function Script:UpdateWorld()
	
end
]]

--[[
function Script:UpdatePhysics()
	
end
]]

--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]