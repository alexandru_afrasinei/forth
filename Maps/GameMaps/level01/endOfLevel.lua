Script.Platform = nil --entity

function Script:Start()
	---------------------------------------------------------------------------
	--We want the trigger visible in the editor, but invisible in the game
	--We can achieve this by creating a material, setting the blend mode to make
	--it invisible, and applying it to the model.
	---------------------------------------------------------------------------
	local material = Material:Create()
	material:SetBlendMode(5)--Blend.Invisible
	self.entity:SetMaterial(material)
	material:Release()
	self.entity:SetShadowMode(0)
end

function Script:Collision(entity, position, normal, speed)
	if (entity:GetKeyValue("type") == "player") then
		self.Platform:SetMass(10)
		self.Platform:SetOmega(2,0,0)
		self.component:CallOutputs("Collision")
	end
end