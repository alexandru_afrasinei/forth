--[[
This script will act as a trigger to change the current map.
Place this at the end of your map to make the player progress
to the next level.
]]--

Script.mapname=""--string "Map Name"
Script.enabled = true--bool

function Script:Start()
	local material = Material:Create()
	material:SetBlendMode(5)--Blend.Invisible
	self.entity:SetMaterial(material)
	material:Release()
	self.entity:SetShadowMode(0)
end
--[[
	if self.enabled then
		gstate:set(LEVEL2_STATE)
		changenextstate = LEVEL2_STATE
		guimanager:ShowStartLevel()
		changemapname=self.mapname 
	end
--]]
function Script:Collision(entity, position, normal, speed)
	if self.enabled then
		gstate:set(LEVEL2_STATE)
		changenextstate = LEVEL2_STATE
		guimanager:ShowStartLevel()
		changemapname=self.mapname 
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
	end
end