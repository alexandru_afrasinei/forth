Script.open = false
Script.moving = false
--Script.maxAngle = 90
--Script.startAngle = 0
--Script.startPosition = null
--Script.endPosition = null
Script.lid_entity = null
Script.pvt_start_pos = null
Script.pvt_end_pos = null

function Script:Start()	
	-- Get child pivot positions
	--lidPivot = self.entity:FindChild("PVT_start")
	self.lid_entity = self.entity:FindChild("crate_top")
	self.pvt_start_pos = self.entity:FindChild("PVT_start"):GetPosition(true)
	self.pvt_end_pos = self.entity:FindChild("PVT_start"):GetPosition(true)		
		
end


function Script:UpdateWorld()
	if window:KeyDown(Key.T) then
		self.lid_entity:SetPosition(self.pvt_end_pos)
	end	
	if window:KeyDown(Key.G) then
		self.lid_entity:SetPosition(self.pvt_start_pos)
	end	
end


--[[
function Script:UpdatePhysics()
	
end
]]

--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]