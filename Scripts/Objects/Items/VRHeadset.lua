-- VR HeadSet
-- Author: Michael Gunn
--public
Script.enabled=true--bool "Enabled"
Script.health = 1 -- int "Health Value"
Script.activateSoundFile="Prefabs/Architecture/vr-unit/vr-headset-activate.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.deactivateSoundFile="Prefabs/Architecture/vr-unit/vr-headset-deactivate.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.debug = false-- bool "Debug"
Script.activateRotateAmount = 180 -- float "Rotate Amount"
Script.activateMoveAmount = 0.25 -- float "activateMove Amount"
--private
Script.used = false
Script.itemType="vr-headset"
Script.pauseDuration=1000
Script.pauseTime=nil
Script.on = false

function Script:Start()
	self.sound={}
	self.startRotation = self.entity:GetRotation(true)
	self.startPosition = self.entity:GetPosition(true)

	if self.activateSoundFile == nil then
		self.activateSoundFile = "Prefabs/Architecture/vr-unit/vr-headset-activate.wav"
	end

	if self.activateSoundFile then
		self.sound.activate= Sound:Load(self.activateSoundFile)
	end

	if self.deactivateSoundFile == nil then
		self.deactivateSoundFile = "Prefabs/Architecture/vr-unit/vr-headset-deactivate.wav"
	end

	if self.deactivateSoundFile then
		self.sound.deactivate= Sound:Load(self.deactivateSoundFile)
	end


	self.entity:SetKeyValue("type", self.itemType)

	--[[
	if self.sound ~=nil then
		self.healthSoundSource = Source:Create()
		self.healthSoundSource:SetSound(self.sound)
		self.healthSoundSource:SetLoopMode(false) -- refuel should not loop	
		self.healthSoundSource:SetRange(50)
	end
	]]--

end

function Script:Use(usedBy)
	if self.debug then System:Print("VRHeadset:Use") end

	-- GUARD CONDITIONS
	if not self.enabled then
		if self.debug then System:Print("Use:DENIED enabled:"..tostring(self.enabled) ) end
		return true
	end

	--self.component:CallOutputs("GiveOutHealth")	
	self.component:CallOutputs("Use")
	self:Toggle()	
	return false

end


function Script:Toggle()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("VRHeadset:Toggle") end
	if self.enabled ~= true then return end

	self.on = not self.on
	if self.on then
		self:On()
	else
		self:Off()
	end		

end


function Script:On()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("VRHeadset:On") end
	if self.enabled ~= true then return end
	if self.sound.activate then self.entity:EmitSound(self.sound.activate) end
	self.pauseTime= Time:GetCurrent()

	self.entity:SetRotation(self.startRotation.x, self.startRotation.y+self.activateRotateAmount, self.startRotation.z,true)
	self.entity:SetPosition(self.startPosition.x, self.startPosition.y+self.activateMoveAmount, self.startPosition.z+self.activateMoveAmount,true)

	self.component:CallOutputs("On")
end

function Script:Off()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("VRHeadset:Off") end
	if self.enabled ~= true then return end
	if self.sound.deactivate then self.entity:EmitSound(self.sound.deactivate) end
	self.entity:SetRotation(self.startRotation.x, self.startRotation.y, self.startRotation.z, true)
	self.entity:SetPosition(self.startPosition.x, self.startPosition.x, self.startPosition.z,true)

	self.component:CallOutputs("Off")
end

function Script:Enable()--in
	-- GUARD CONDITIONS
	if self.enabled==true then return end

	self.enabled=true
	self.component:CallOutputs("Enable")
	self.health=1
end

function Script:Disable()--in
	-- GUARD CONDITIONS
	if self.enabled == true then return end

	self.enabled=false
	self.component:CallOutputs("Disable")
	self.health=0
end


function Script:Used()
	if self.debug then System:Print("VRHeadset:Used") end
	self.used = true
	self.component:CallOutputs("Used")
	self.entity:Hide()
	-- Release object	
	--self:Release()
end

function Script:Release()

	ReleaseTableObjects(self.sound)

	--if self.sound then self.sound:Release() end
	-- Release additional resources
	--[[
	if self.healthSoundSource ~= nil then
		self.healthSoundSource:Release()
		self.healthSoundSource = nil
	end	
	--]]
end


function Script:UpdateWorld()

	--[[
	if window:KeyHit(Key.U) then
		System:Print("HIT U")
		self:On()
	end

	if window:KeyHit(Key.J) then
		System:Print("HIT J")
		self:Off()
	end
	--]]

	local currenttime = Time:GetCurrent()
	if self.pauseTime ~= nil and currenttime > self.pauseTime + self.pauseDuration then
		self:Used()
	end

end