--public
Script.enabled=true--bool "Enabled"
Script.health = 50 -- int "Health Value"
Script.soundfile="Sound/Player/Pick up.ogg"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.usedUpSoundfile="Sound/Impact/body_punch_02.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.debug = false-- bool "Debug"
--Script.pickupIfOverMax = false -- bool "Pickup Over Max"
--private
Script.used = false
Script.itemType="health"


function Script:Start()

	if self.soundfile == nil then
		self.soundfile = "Sound/Player/Pick up.ogg"
	end

	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end

	if self.usedUpSoundfile == nil then
		self.usedUpSoundfile = "Sound/Impact/body_punch_02.wav"
	end

	if self.usedUpSoundfile then
		self.usedUpSound = Sound:Load(self.usedUpSoundfile)
	end


	self.entity:SetKeyValue("type", self.itemType)

	--[[
	if self.sound ~=nil then
		self.healthSoundSource = Source:Create()
		self.healthSoundSource:SetSound(self.sound)
		self.healthSoundSource:SetLoopMode(false) -- refuel should not loop	
		self.healthSoundSource:SetRange(50)
	end
	]]--

end

function Script:Use(usedBy)
	if self.debug then System:Print("USE HEALTH") end

	-- GUARD CONDITIONS
	if not self.enabled or self.used == true then
		if self.debug then System:Print("enabled:"..tostring(self.enabled).." OR used:"..tostring(self.used)) end
		if self.used == true then
			self.entity:EmitSound(self.usedUpSound, 50, 0.5, 8, false) 
		end
		return 0
	end
	
	-- Check if user should be allowed to use the item up
	if usedBy.maxHealth ~= nil and usedBy.health >= usedBy.maxHealth then
		if self.debug then System:Print("not giving health - usedBy.health:"..tostring(usedBy.health).." usedBy.maxHealth:"..tostring(usedBy.maxHealth)) end
		return 0 
	end
	
	local itemType = usedBy.entity:GetKeyValue("type")
	System:Print("usedBy itemType:"..itemType)

	if type(usedBy.ReceiveHealth)=="function" then
		usedBy:ReceiveHealth(self.health, self.entity)
	end	

	if self.sound then 
		self.entity:EmitSound(self.sound, 50, 0.5, 8, false) 
	end

	--self.component:CallOutputs("GiveOutHealth")	
	self.component:CallOutputs("Use")
	self:ObjectUsed()	

	return self.health
	
end

-- got notification from donor that they got it
function Script:HealthReceived(receivedBy) -- in
	local itemType = receivedBy:GetKeyValue("type")
	System:Print("PickupHealth:HealthReceived by:"..itemType)
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:ObjectUsed()
	self.used = true
	self.component:CallOutputs("HealthStationUsed")
	--self.entity:Hide()
	-- Release object	
	--self:Release()
end

function Script:Release()

	if self.sound then self.sound:Release() end
	-- Release additional resources
	--[[
	if self.healthSoundSource ~= nil then
		self.healthSoundSource:Release()
		self.healthSoundSource = nil
	end	
	]]--
end