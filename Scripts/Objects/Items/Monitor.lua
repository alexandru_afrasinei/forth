--public
Script.enabled=true--bool "Enabled"
Script.health = 1 -- int "Health Value"
Script.debug = false-- bool "Debug"
Script.screen1File="Models/MonitorScreens/screen1.tex"--path "Screen1" "All Supported Files:tex;Texture (*.tex):tex|Texture"
Script.screen2File="Models/MonitorScreens/screen2.tex"--path "Screen2" "All Supported Files:tex;Texture (*.tex):tex|Texture"


--private
Script.used = false
Script.itemType="cryo_pod"
Script.pauseDuration=200
Script.pauseTimer=0
Script.on = false--bool "Begin On"
Script.currentScreen = 0

function Script:Start()

	self.screens = {}
	if self.screen1File == nil then
		self.screen1File = "Models/MonitorScreens/screen1.tex"
	end

	if self.screen1File then
		self.screens.screen1= Texture:Load(self.screen1File)
	end

	if self.screen2File == nil then
		self.screen2File = "Models/MonitorScreens/screen2.tex"
	end

	if self.screen2File then
		self.screens.screen2= Texture:Load(self.screen2File)
	end


	self.entity:SetKeyValue("type", self.itemType)

	--[[
	if self.sound ~=nil then
		self.healthSoundSource = Source:Create()
		self.healthSoundSource:SetSound(self.sound)
		self.healthSoundSource:SetLoopMode(false) -- refuel should not loop	
		self.healthSoundSource:SetRange(50)
	end
	]]--

end

function Script:Use(usedBy)
	if self.debug then System:Print("CryoPod:Use") end

	-- GUARD CONDITIONS
	if not self.enabled then
		if self.debug then System:Print("Use:DENIED enabled:"..tostring(self.enabled) ) end
		return true
	end

	--self.component:CallOutputs("GiveOutHealth")	
	self.component:CallOutputs("Use")
	self:ObjectUsed()	
	
	return false

end


function Script:Toggle()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:Toggle") end
	if self.enabled ~= true then return end

	self.on = not self.on
	if self.on then
		self:On()
	else
		self:Off()
	end		

end


function Script:On()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:On") end
	if self.enabled ~= true then return end
	--if self.sound.open then self.entity:EmitSound(self.sound.open) end
	self.component:CallOutputs("On")
end

function Script:Off()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:Off") end
	if self.enabled ~= true then return end
	--if self.sound.close then self.entity:EmitSound(self.sound.close) end

	self.component:CallOutputs("Off")
end

function Script:Enable()--in
	-- GUARD CONDITIONS
	if self.enabled==true then return end

	self.enabled=true
	self.component:CallOutputs("Enable")
	self.health=1
end

function Script:Disable()--in
	-- GUARD CONDITIONS
	if self.enabled == true then return end

	self.enabled=false
	self.component:CallOutputs("Disable")
	self.health=0
end

--[[
function Script:ObjectUsed()
	self.used = true
	self.component:CallOutputs("CryoPodUsed")
	--self.entity:Hide()
	-- Release object	
	--self:Release()
end
--]]

function Script:Release()

	--if self.screens then self.screens:Release() end
	-- Release additional resources
	--[[
	if self.healthSoundSource ~= nil then
		self.healthSoundSource:Release()
		self.healthSoundSource = nil
	end	
	--]]
end


function Script:UpdateWorld()

	if window:KeyHit(Key.U) then
		System:Print("HIT U")
		self:On()
	end

	if window:KeyHit(Key.J) then
		System:Print("HIT J")
		self:Off()
	end



	--[[
	local currenttime = Time:GetCurrent()
	if self.pauseTime ~= nil and currenttime > self.pauseTimer + self.pauseDuration then
		self.listening = true
	end
	--]]
end