--public
Script.enabled = true --bool "Enabled"
Script.startOn = false --bool "Start On"
-- private
Script.light=nil
Script.flare=nil


function Script:Start()
	self:Init()
	self.enabled = false

end

function Script:Init()
	self.light = self.entity:GetChild(0)
	self.flare = self.entity:GetChild(0):GetChild(0)

	if self.light == nil or self.flare == nil then
		Assert:Error("could not find light or flare child entities")
	end

	if self.startOn == false then
		self.light:Hide()		
	end

end


function Script:Enable()--in
	self:Init()
	if self.enabled==false then
		self.enabled=true
		self.light:Show()
		self.component:CallOutputs("Enable")
	end
end

function Script:Disable()--in
	self:Init()
	if self.enabled == true then
		self.enabled=false
		self.light:Hide()
		self.component:CallOutputs("Disable")
	end
end