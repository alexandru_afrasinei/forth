-- enemy cube
Script.startOpen = false
Script.open = false

function Script:Start()
	
end

function Script:Open()
	System:Print("OPEN")
	if self.open == false then
		System:Print("OPENING")
		self.entity:PlayAnimation("Open",0.05,300,1,"Opened")
	end
end

function Script:Close()
	System:Print("CLOSE")
	if self.open then
		System:Print("CLOSING")
		self.entity:PlayAnimation("Close",0.05,300,1,"Closed")

	end	
end


function Script:Opened()
	System:Print("Opened")
	self.open = true
end

function Script:Closed()
	System:Print("Closed")
	self.open = false
end


function Script:UpdateWorld()
	
end



function Script:UpdatePhysics()

	if window:KeyHit(Key.R) then	
		self:Open()
	end

	if window:KeyHit(Key.F) then	
		self:Close()
	end
	
end


--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]