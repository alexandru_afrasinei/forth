--public
Script.enabled=true--bool "Enabled"
Script.health = 1 -- int "Health Value"
Script.openSoundFile="Models/Items/cryopod/forth-cryo-pod-open.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.closeSoundFile="Models/Items/cryopod/forth-cryo-pod-close.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.debug = false-- bool "Debug"
Script.rotateAmount = 45 -- float "Rotate Amount"
--Script.pickupIfOverMax = false -- bool "Pickup Over Max"
--private
Script.used = false
Script.itemType="cryo_pod"
Script.pauseDuration=200
Script.pauseTimer=0
Script.on = false--bool "Begin On"

function Script:Start()
	self.sound={}
	self.startRotation = self.entity:GetRotation(true)
	self.startPosition = self.entity:GetPosition(true)

	if self.openSoundFile == nil then
		self.openSoundFile = "Sound/Items/open_pod.wav"
	end

	if self.openSoundFile then
		self.sound.open= Sound:Load(self.openSoundFile)
	end

	if self.closeSoundFile == nil then
		self.closeSoundFile = "Sound/Items/close_pod.wav"
	end

	if self.closeSoundFile then
		self.sound.close= Sound:Load(self.closeSoundFile)
	end


	self.entity:SetKeyValue("type", self.itemType)

	--[[
	if self.sound ~=nil then
		self.healthSoundSource = Source:Create()
		self.healthSoundSource:SetSound(self.sound)
		self.healthSoundSource:SetLoopMode(false) -- refuel should not loop	
		self.healthSoundSource:SetRange(50)
	end
	]]--

end

function Script:Use(usedBy)
	if self.debug then System:Print("CryoPod:Use") end

	-- GUARD CONDITIONS
	if not self.enabled then
		if self.debug then System:Print("Use:DENIED enabled:"..tostring(self.enabled) ) end
		return true
	end

	--self.component:CallOutputs("GiveOutHealth")	
	self.component:CallOutputs("Use")
	self:ObjectUsed()	
	
	return false

end


function Script:Toggle()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:Toggle") end
	if self.enabled ~= true then return end

	self.on = not self.on
	if self.on then
		self:On()
	else
		self:Off()
	end		

end


function Script:On()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:On") end
	if self.enabled ~= true then return end
	if self.sound.open then self.entity:EmitSound(self.sound.open) end

	self.entity:SetRotation(self.startRotation.x+self.rotateAmount, self.startRotation.x, self.startRotation.z,true)

	self.component:CallOutputs("On")
end

function Script:Off()--in
	-- GUARD CONDITIONS
	if self.debug then System:Print("CryoPod:Off") end
	if self.enabled ~= true then return end
	if self.sound.close then self.entity:EmitSound(self.sound.close) end
	self.entity:SetRotation(self.startRotation.x, self.startRotation.y, self.startRotation.z, true)

	self.component:CallOutputs("Off")
end

function Script:Enable()--in
	-- GUARD CONDITIONS
	if self.enabled==true then return end

	self.enabled=true
	self.component:CallOutputs("Enable")
	self.health=1
end

function Script:Disable()--in
	-- GUARD CONDITIONS
	if self.enabled == true then return end

	self.enabled=false
	self.component:CallOutputs("Disable")
	self.health=0
end

--[[
function Script:ObjectUsed()
	self.used = true
	self.component:CallOutputs("CryoPodUsed")
	--self.entity:Hide()
	-- Release object	
	--self:Release()
end
--]]

function Script:Release()

	if self.sound then self.sound:Release() end
	-- Release additional resources
	--[[
	if self.healthSoundSource ~= nil then
		self.healthSoundSource:Release()
		self.healthSoundSource = nil
	end	
	--]]
end


function Script:UpdateWorld()

	if window:KeyHit(Key.U) then
		System:Print("HIT U")
		self:On()
	end

	if window:KeyHit(Key.J) then
		System:Print("HIT J")
		self:Off()
	end



	--[[
	local currenttime = Time:GetCurrent()
	if self.pauseTime ~= nil and currenttime > self.pauseTimer + self.pauseDuration then
		self.listening = true
	end
	--]]
end