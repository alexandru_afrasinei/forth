--public
Script.enabled=true--bool "Enabled"
Script.energy = 10 -- int "Energy Value"
Script.soundfile="Sound/Player/misc01.wav"--path "Sound" "Wav Files (*.wav):wav|Ogg Files (*.ogg):ogg"
Script.canCollideToPickup=true--bool "Collide Can Pickup"
Script.debug = false-- bool "Debug"
--Script.pickupIfOverMax = false -- bool "Pickup Over Max"
--private
Script.used = false
Script.itemType="energy"


function Script:Start()

	if self.soundfile == nil then
		self.soundfile = "Sound/Player/misc01.wav"
	end

	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end

	self.entity:SetKeyValue("type", self.itemType)

	--[[
	if self.sound ~=nil then
		self.energySoundSource = Source:Create()
		self.energySoundSource:SetSound(self.sound)
		self.energySoundSource:SetLoopMode(false) -- refuel should not loop	
		self.energySoundSource:SetRange(50)
	end
	]]--

end

function Script:Collision(entity, position, normal, speed)

	-- GUARD CONDITIONS
	if not self.enabled or self.used == true or self.canCollideToPickup == false then
		if self.debug then System:Print("enabled:"..tostring(self.enabled).." OR used:"..tostring(self.used).." OR canCollideToPickup:"..tostring(self.canCollideToPickup)) end
		return 0
	end

	if self.enabled and self.used == false then
		if self.debug then System:Print("COLLIDE WITH ENERGY") end
		self:Use(entity.script)
		--if entity.script~=nil then self:Use(entity.script) end
	end
end

function Script:Use(usedBy)
	--System:Print("USE ENERGY")

	-- GUARD CONDITIONS
	if not self.enabled or self.used == true or self.canCollideToPickup == false  then
		if self.debug then System:Print("enabled:"..tostring(self.enabled).." OR used:"..tostring(self.used).." OR canCollideToPickup:"..tostring(self.canCollideToPickup)) end
		return 0
	end
	
	-- Check if user should be allowed to use the item up
	if usedBy.maxEnergy ~= nil and usedBy.energy >= usedBy.maxEnergy then
		if self.debug then System:Print("not giving energy - usedBy.energy:"..tostring(usedBy.energy).." usedBy.maxEnergy:"..tostring(usedBy.maxEnergy)) end
		return 0 
	end
	
	local itemType = usedBy.entity:GetKeyValue("type")
	System:Print("usedBy itemType:"..itemType)

	if type(usedBy.ReceiveEnergy)=="function" then
		usedBy:ReceiveEnergy(self.energy, self.entity)
	end	

	if self.sound then 
		self.entity:EmitSound(self.sound, 50, 0.5, 8, false) 
	end

	--self.component:CallOutputs("GiveOutEnergy")	
	self.component:CallOutputs("Use")
	self:ObjectUsed()	

	return self.energy
	
end

-- got notification from donor that they got it
function Script:EnergyReceived(receivedBy) -- in
	local itemType = receivedBy:GetKeyValue("type")
	System:Print("PickupEnergy:EnergyReceived by:"..itemType)
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.energy=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.energy=0
	end
end

function Script:ObjectUsed()
	self.used = true
	self.component:CallOutputs("EnergyUsed")
	self.entity:Hide()
	-- Release object	
	self:Release()
end

function Script:Release()

	if self.sound then self.sound:Release() end
	-- Release additional resources
	--[[
	if self.energySoundSource ~= nil then
		self.energySoundSource:Release()
		self.energySoundSource = nil
	end	
	]]--
end