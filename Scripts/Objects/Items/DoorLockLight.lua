--public
Script.startOn = false --bool "Start On"
-- private
Script.enabled = false
Script.light=nil
Script.flare=nil
Script.on = false
Script.lights = {}

function Script:Start()
	self:Init()
end

function Script:Init()
	self.lights.on = self.entity:FindChild("light-on")
	self.lights.off = self.entity:FindChild("light-off")
	--Debug:Assert(self.lights.on == nil or self.lights.off == nil, "could not find light or flare child entities")
	self.enabled = true
end

function Script:On()--in
	self:Init()
	if self.enabled then
		self.on=true
		self.lights.on:Show()
		self.lights.off:Hide()
		self.component:CallOutputs("On")
	end
end

function Script:Off()--in
	self:Init()
	if self.enabled then
		self.on=false
		self.lights.on:Hide()
		self.lights.off:Show()
		self.component:CallOutputs("Off")
	end
end