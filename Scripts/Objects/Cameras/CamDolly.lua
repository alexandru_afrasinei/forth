-- Public
Script.enabled = true--bool "Enabled"
Script.loop = false--bool "Loop"
Script.steps = 100--int "Steps"
Script.pointCamAt = nil--entity "Point Cam at"
--Script.stepDelay = 0 --int "Step Delay"
Script.smoothness = 10 -- int "Smoothness"
Script.fadeAtEnd = true -- bool "Fade At End"
Script.fadeOut = true -- bool "Fade Out"
Script.colour = Vec4(0,0,0) -- color "Fade Colour"
-- Private
Script.currentPosition = Vec3(0)
Script.currentStep = 1
Script.paused = false
Script.pauseDelay = 5000 -- int "Pause Delay"
Script.pauseStartTime = 0
--Script.oldDollyTime = 0
--Script.dollySpeed = 10
Script.fading = false
Script.fadeValue = 0
Script.fadeSpeed = 50
Script.fadeComplete = false

Script.fadeTriggered = false

function Script:TriggerFade()--in
	if self.fadeTriggered == false then
		self.fadeComplete = false
		self.fadeTriggered = true
		self.fading = true		
	end
end

function Script:Round(val, decimal)
  local exp = decimal and 10^decimal or 1
  return math.ceil(val * exp - 0.5) / exp
end

function Script:RoundVec3(val, decimal)
	return Vec3(self:Round(val.x, decimal), self:Round(val.y, decimal), self:Round(val.z, decimal))
end

function Script:smooth( points, steps )
 
	if #points < 3 then
		return points
	end
 
	local steps = steps or 5
 
	local spline = {}
	local count = #points - 1
	local p0, p1, p2, p3, x, y, z
	local rx, ry, rz

	local name, event
 
	for i = 1, count do

		--name = nil
		event = false
 
		if i == 1 then
			p0, p1, p2, p3 = points[i], points[i], points[i + 1], points[i + 2]
		elseif i == count then
			p0, p1, p2, p3 = points[#points - 2], points[#points - 1], points[#points], points[#points]
		else
			p0, p1, p2, p3 = points[i - 1], points[i], points[i + 1], points[i + 2]
		end

		-- add name and event
		name = points[i].name
		event = points[i].event
 
		for t = 0, 1, 1 / steps do
 
 			-- 3d position
			x = 0.5 * ( ( 2 * p1.pos.x ) + ( p2.pos.x - p0.pos.x ) * t + ( 2 * p0.pos.x - 5 * p1.pos.x + 4 * p2.pos.x - p3.pos.x ) * t * t + ( 3 * p1.pos.x - p0.pos.x - 3 * p2.pos.x + p3.pos.x ) * t * t * t )
			y = 0.5 * ( ( 2 * p1.pos.y ) + ( p2.pos.y - p0.pos.y ) * t + ( 2 * p0.pos.y - 5 * p1.pos.y + 4 * p2.pos.y - p3.pos.y ) * t * t + ( 3 * p1.pos.y - p0.pos.y - 3 * p2.pos.y + p3.pos.y ) * t * t * t )
			z = 0.5 * ( ( 2 * p1.pos.z ) + ( p2.pos.z - p0.pos.z ) * t + ( 2 * p0.pos.z - 5 * p1.pos.z + 4 * p2.pos.z - p3.pos.z ) * t * t + ( 3 * p1.pos.z - p0.pos.z - 3 * p2.pos.z + p3.pos.z ) * t * t * t )

			-- 3d rotation
			rx = 0.5 * ( ( 2 * p1.rot.x ) + ( p2.rot.x - p0.rot.x ) * t + ( 2 * p0.rot.x - 5 * p1.rot.x + 4 * p2.rot.x - p3.rot.x ) * t * t + ( 3 * p1.rot.x - p0.rot.x - 3 * p2.rot.x + p3.rot.x ) * t * t * t )
			ry = 0.5 * ( ( 2 * p1.rot.y ) + ( p2.rot.y - p0.rot.y ) * t + ( 2 * p0.rot.y - 5 * p1.rot.y + 4 * p2.rot.y - p3.rot.y ) * t * t + ( 3 * p1.rot.y - p0.rot.y - 3 * p2.rot.y + p3.rot.y ) * t * t * t )
			rz = 0.5 * ( ( 2 * p1.rot.z ) + ( p2.rot.z - p0.rot.z ) * t + ( 2 * p0.rot.z - 5 * p1.rot.z + 4 * p2.rot.z - p3.rot.z ) * t * t + ( 3 * p1.rot.z - p0.rot.z - 3 * p2.rot.z + p3.rot.z ) * t * t * t )

 
			--prevent duplicate entries
			if not(#spline > 0 and spline[#spline].x == x and spline[#spline].y == y and spline[#spline].z == z) then
				table.insert( spline , {pos = { x = x , y = y, z = z }, rot = { x = rx, y = ry, z = rz }, name = name, event = event} )
				--table.insert( spline , Vec3(x,y,z) )
			end	
			
		end
 
	end	
 
	return spline
 
end

function Script:TableDeepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function Script:Enable()--in
	if self.enabled == false then
		self.entity:Show()
		self.enabled = true
		self.active = true
		self.pauseStartTime = Time:GetCurrent()
		self.fadeTriggered = false 		
	end
end
function Script:Disable()--in
	if self.enabled == true then
		self.entity:Hide()
		self.enabled = false
	end
end



function Script.Pause() --in
	self.enabled = true
	self.active = true
end

function Script.Unpause() --in 
	self.enabled = false
	self.active = false
end



function Script:Vec3Curve(vec3target, vec3current, smoothness)
	local newVec3 = self:TableDeepcopy(vec3target)
	newVec3.x = Math:Curve(vec3target.x,vec3current.x,smoothness / Time:GetSpeed())
	newVec3.y = Math:Curve(vec3target.y,vec3current.y,smoothness / Time:GetSpeed())
	newVec3.z = Math:Curve(vec3target.z,vec3current.z,smoothness / Time:GetSpeed())

	return newVec3
end


function Script:Init()
	if self.fadeOut then
		self.fadeValue =  0	
	else
		self.fadeValue =  self.fadeSpeed

	end
	
	self.active = true

	self.currentPosition = self.entity:GetPosition(true)

	self.targetCount = self.entity:CountChildren()

	--self.smoothness = 10

	local targetPoints = {}
	local pointName = "p0"
	local hasEvent = false
	local foundChild = nil

	table.insert(targetPoints, {name = "cam", pos = self.entity:GetPosition(true), rot = self.entity:GetRotation(true), event = false} )
	for i=0,self.targetCount-1 do
		hasEvent = false
		pointName = "p"..i

		if self.entity:FindChild(pointName) then
			foundChild = self.entity:FindChild(pointName)
			if foundChild.script ~= nil then
				if type(foundChild.script.CallEvent)=="function" then
					hasEvent = true
				end
			end
			table.insert(targetPoints, {name = pointName, pos = self.entity:FindChild(pointName):GetPosition(true), rot = self.entity:FindChild(pointName):GetRotation(true), event = hasEvent} )
			self.entity:FindChild(pointName):Hide()
		end
	end

	self.targetPositions = self:smooth(targetPoints, self.steps)


	self.pivot = Pivot:Create()
	self.pivot:SetPosition(self.currentPosition, true)

	self.pointerPivot = Pivot:Create()

end


function Script:Start()
	self:Init()
end

function Script:vectorAligner(targetVec,sourceVec)
	--Get the vector between the mouse position and the center of the screen
    local v = targetVec
    local dx = v.x - sourceVec.x
    --local dy = sourceVec.y - v.y
    local dy = v.y - sourceVec.y 
    local dz = v.z - sourceVec.z
    --v = Vec3(dx,dy,dz):Normalize()
    v = Vec3(dx,dy,dz):Normalize()

    --return v:Inverse()
    return v
end

--[[
function Script:UpdateWorld()

end
]]--



function Script:UpdatePhysics()

	local t = Time:GetCurrent()
	if (self.pauseStartTime + self.pauseDelay) > t then
		return
	end
	
	if self.paused then
		return
	end

	--local scrollSpeedFPS = self.dollySpeed * Time:GetSpeed() --make scrollspeed frameindependen
	local roundNum = 0
	local stop = false
	local curPosition = self.pivot:GetPosition(true)
	local newPosition = self.pivot:GetPosition(true)

	if self.enabled and self.active then

		self.entity:SetPosition(self.pivot:GetPosition())

		local nextStepPos = self.targetPositions[self.currentStep].pos
		local nextStepRot = self.targetPositions[self.currentStep].rot
		if self.currentStep + 1 <= #self.targetPositions then
			nextStepPos = self.targetPositions[self.currentStep + 1].pos
			nextStepRot = self.targetPositions[self.currentStep + 1].rot
		end

		if ( self:RoundVec3(curPosition, roundNum) == self:RoundVec3(nextStepPos, roundNum) ) then

			if self.targetPositions[self.currentStep].event then
				self.entity:FindChild(self.targetPositions[self.currentStep].name).script:CallEvent()
			end

			-- find next stepNr
			if self.loop then
				if self.currentStep + 1 > #self.targetPositions then
					self.currentStep = 1
				else
					self.currentStep = self.currentStep + 1
				end
			else
				if self.currentStep + 1 > #self.targetPositions then
					self.currentStep = 1
					--self:Disable()

					self.active = false
					if self.fadeAtEnd and not self.fadeTriggered then
						self.fading = true
					end
					self.component:CallOutputs("ReachedEnd")
				else
					self.currentStep = self.currentStep + 1
				end
			end

		end


		self.currentPosition = newPosition

		self.pivot:SetPosition(nextStepPos.x, nextStepPos.y, nextStepPos.z, true)


		if self.pointCamAt ~= nil then
			self.entity:Point(self.pointCamAt)
		else
			self.entity:SetRotation(nextStepRot.x, nextStepRot.y, nextStepRot.z, true)
		end

	end
	
end

--[[
function Script:UpdateWorld()
	local currentDollyTime = Time:GetCurrent()
	if currentDollyTime >= self.oldDollyTime+self.dollySpeed then
		self.oldDollyTime = Time:GetCurrent()
	end
end
]]--


function Script:PostRender(context)
	if self.fading then
		self:FadeScreen(true,self.fadeSpeed)
	end
end


-- fades screen to black
-- fade 0 fade to black
-- fade 1 fade from black
-- speed eg. 10=fast 100 slow
function Script:FadeScreen(fadeOut,speed)

	local screenWidth = context:GetWidth()
	local screenHeight = context:GetHeight()
	
	if fadeOut then

		-- fade to black
		if self.fadeValue == nil then self.fadeValue=1 end

		local alpha = self.fadeValue/speed
		if alpha>1 then alpha=1 end
		context:SetBlendMode(Blend.Alpha)
		--context:SetColor(Vec4(0,0,0,alpha))
		local colour = Vec4(self.colour.x, self.colour.y, self.colour.z,alpha)
		context:SetColor(colour)
		context:DrawRect(0,0,screenWidth,screenHeight)

		System:Print("self.fadeValue:"..self.fadeValue.." speed:"..speed)
		self.fadeValue = self.fadeValue+1
		if self.fadeValue >= speed then
			System:Print("self.fadeValue:"..self.fadeValue.." speed:"..speed)
			--self.fadeValue = nil
			self.fading = false
			return true
		end



	else
		-- fade from black
		if self.fadeValue == nil then self.fadeValue=speed end

		local alpha = self.fadeValue/speed
		if alpha<=0 then alpha=0 end
		context:SetBlendMode(Blend.Alpha)
		local colour = Vec4(self.colour.x, self.colour.y, self.colour.z,alpha)
		context:SetColor(colour)
		context:DrawRect(0,0,screenWidth,screenHeight)

		self.fadeValue = self.fadeValue-1
		if self.fadeValue <= 0 then
			--self.fadeValue = nil
			self.fading = false
			return true
		end

		
	end

	return false
end