Script.nextPlatform = nil --entity "Platform Readdesc"
-- this is a reference to the next platform that also should get reset when player falls off map
-- if you add a new platform, add the new one to this value the previous one

Script.gravity = 0.0 --do not modify
Script.mass = 0.0 --do not modify
Script.lastupdatetime = 0

Script.jumpSound = nil --path "Jump Sound"

Script.gravityEnabled = false --set this to true if you want to make it fall down again

Script.fallOffset = 1.0 --float "delay falldown" -- seconds to make it fall after colission with player
Script.enableUpAndDownFlight = true --bool "Fly Up and Down" -- make motion like a mario star or something
Script.lerpTime = 1 --float "Fall acceleration Multiplier" -- the smaller the number, the faster entity will start to fall when touched by player

Script.falltime = -1
Script.checkForFallTime = false

Script.source = nil
Script.lastTimePlayed = -1000

function Script:Start()
	self.gravity = world:GetGravity().y
	self.mass = self.entity:GetMass()
	if(self.mass <= 0) then
		self.entity:SetMass(10)
		--self.mass = 100 --is a good value for interacting with
	end
	self.originPos = self.entity:GetPosition(true)
	self.originRot = self.entity:GetRotation(true)
	
	local jumpS = Sound:Load(self.jumpSound)
	self.source = Source:Create() 
	self.source:SetSound(jumpS) 
	if jumpS then jumpS:Release() end
	self.source:SetVolume(0.01)
	
	self.platformStartPos = self.entity:GetPosition(true)
	self.platformStartRot = self.entity:GetRotation(true)
	self.entity:SetGravityMode(gravityEnabled)
end

function Script:UpdatePhysics()

	--cancel out world gravity by adding backward force up
	--self.entity:AddForce(0,self.mass *- self.gravity + (self.offset) ,0,true)
	
	local pos = self.entity:GetPosition()
	
	-- the following two lines will make the object stop, to save resources
	if(pos.y < -300) then self.entity:SetVelocity(0,0,0) self.entity:SetOmega(0,0,0) local releasetime = Time:GetCurrent() end
	if(releasetime) and Time:GetCurrent()-releasetime > 1000 then self.entity:Release() end 
	
	if self.enableUpAndDownFlight == true and self.gravityEnabled == false then
		-- we get the entity pos as a vector
		-- we take the y value of our vector and add a sin wave (-1 to 1) to make it fly up and down
		-- by using the math function on the current time which is a value that goes up
		-- and divide it down to a small fractor because sin wave is which is rather "small"
		self.entity:PhysicsSetPosition(pos.x, self.platformStartPos.y+Math:Sin(Time:GetCurrent()*.1)*0.1, pos.z)
		-- we use physicsSetPosition to disable clipping trough objects
	end
	
	if(self.checkForFallTime and self.falltime <= Time:GetCurrent()+self.fallOffset) then
		self.entity:SetGravityMode(true)
		self.checkForFallTime = false
	end
end

function Script:Collision(entity, position, normal, speed)
	--if we collide we dont want the comical physics to interfere with "real" physics
	self.enableUpAndDownFLight = false
	self.entity:SetOmega(0,0,0)
	--if we collide with the player the object will fall down, for gameplay purposes
	if (entity:GetKeyValue("type") == "player") then
		self:makeFallAfterAbit()
		self:PlaySound()
	end
end

function Script:PlaySound()
	if(Time:GetCurrent() - self.lastTimePlayed > 1000) then
		if self.jumpSound~=nil then 
			self.source:Play()
		end
		self.lastTimePlayed = Time:GetCurrent()
	end
end

function Script:makeFallAfterAbit()
	self.falltime = Time:GetCurrent()
	self.checkForFallTime = true
end

function Script:Release()
	self.source:Release()
end