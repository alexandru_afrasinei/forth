function GravityCast(entity,extra)
	--this function needs to be global, for the aabb in the player script (Somewhere around ...Key.Q... line)
	if (entity:GetClass()==Object.ModelClass) then
		local spellForce = 15 --edit this if the push is too hard
		if(entity:GetKeyValue("phys") == "y") then 
			entity:SetVelocity(Vec3(spellForce, spellForce, spellForce) * (entity:GetPosition()-extra):Normalize())  
		end
	end
end

function RangeDmg(entity, distributor)
	if (entity:GetClass()==Object.ModelClass) then
		--System:Print("Range damage given to "..tostring(entity:GetKeyValue("name")))
		if(entity:GetKeyValue("type") == "enemy") then
			if(entity.script.health > 0 ) then distributor.script:CastRay(entity) end --distributor is electro ball pfb
			entity.script:Hurt(10)
		end
	end
end