Script.angleCounter = 0 --int "angle"
function Script:UpdatePhysics()
	--increase angle counter
	
	self.angleCounter = self.angleCounter + (1*Time:GetSpeed())
	
	--apply rotation
	
	self.entity:SetRotation(0,0,self.angleCounter)
	
	--Check to see if we made a full circle
	
	if self.angleCounter > 360 then
		
		self.component:CallOutputs("Done rotating")
		
		self.angleCounter = 0
		
	end
	
end