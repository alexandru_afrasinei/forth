--[[
This script will spin any entity around an axis using physics forces.
]]--
Script.enabled=true--bool "Enabled"
Script.axis = 1--choice "Axis" "X,Y,Z"
Script.turnspeed = 60--float "Speed"
Script.angle = 0
Script.oldAngle = 0

function Script:Start()

	local axisx=0
	local axisy=0
	local axisz=0
	if self.axis==0 then axisx=1 end
	if self.axis==1 then axisy=1 end
	if self.axis==2 then axisz=1 end	
end

function Script:UpdatePhysics()
	if self.enabled then
		self.oldAngle = self.angle
		self.angle = self.angle + self.turnspeed / 60.0
		self.entity:Turn(0, self.turnspeed, self.turnspeed, true)
	end
end

function Script:Disable()--in
	self.enabled=false
end

function Script:Enable()--in
	self.enabled=true
end

function Script:Release()
	if self.joint then
		self.joint:Release()
		self.joint=nil
	end
end