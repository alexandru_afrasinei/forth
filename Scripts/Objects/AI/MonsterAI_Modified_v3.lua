--DESCRIPTION: MDG extended MonsterAI
--DATE: 20/04/2019
--EXTENDED FROM: The Hankinator's extended MonsterAI

import "Scripts/Functions/GetEntityNeighbors.lua"

--Public values
Script.health=40--int "Health"
Script.enabled=true--bool "Enabled"
Script.target=nil--entity "Target"
Script.sightradius=30--float "Sight Range"
Script.senseradius=2--float "Hearing Range"
Script.speed=4 -- float "Chase Speed"
Script.teamid=2--choice "Team" "Neutral,Good,Bad"
Script.attackdelay=300--int "Attack delay"
Script.animspeedrun=0.04--float "Run anim speed"
Script.attack1sound=""--path "Attack 1 sound" "Wav file (*.wav):wav|Sound"
Script.attack2sound=""--path "Attack 2 sound" "Wav file (*.wav):wav|Sound"
Script.alertsound=""--path "Alert sound" "Wav file (*.wav):wav|Sound"
Script.deathsound=""--path "Death sound" "Wav file (*.wav):wav|Sound"
Script.idlesound=""--path "Idle sound" "Wav file (*.wav):wav|Sound"
Script.attackPause=1000 --int "Attack Pause"
Script.idlePause=1000 --int "Idle Pause"
Script.Path = nil--entity "Path"
Script.PathEndOption = 0--choice "At path end" "Loop,Reverse,Stop"
Script.FOV = 120--int
Script.ShowTargetBox=false--bool
Script.debug=true --bool "Debug"
Script.ShowTargetBox=true--bool
--Script.explosionprefabpath="AddOns/MDG_Explosion/Prefabs/mdg_explosion.pfb"--path "Projectile" "Prefab (*.pfb):pfb
Script.explosionPrefabPath="Addons/MDG_Explosion/Prefabs/pfb_explosion.pfb"--path "Projectile" "Prefab (*.pfb):pfb
Script.explosionSoundPath="AddOns/MDG_Explosion/Sounds/explosion02.wav"--path "Projectile" "Prefab (*.pfb):pfb

--Private values
Script.damage=5
Script.attackrange=1.5
Script.updatefrequency=500
Script.lastupdatetime=0
Script.prevtarget=nil
Script.followingtarget=false
Script.maxaccel=15

Script.lastupdatetargettime=0
Script.attackmode=0
Script.attackbegan=0
Script.verticalHidingDeflection = 3
Script.NavPoints=nil
Script.NavPointDirection = 1
Script.CurrNavPoint = 0
Script.NavPointDelay = 0
Script.NavPointAnimationCount = 0
Script.targetBox=nil
Script.name=""
Script.mode=""
Script.muzzle=nil
Script.muzzleflashscale=3--float "Flash size"
Script.explosionLifeTime=5000		--int 		"explosionLifeTime"
Script.explosionStartTime = nil

function Script:Enable()--in
	if self.enabled==false then
		if self.health>0 then
			self.enabled=true
			if self.target~=nil then
				self:SetMode("roam")
			else
				self:SetMode("idle")
			end
		end
	end
end

function Script:GetNextNavPoint()
	self.CurrNavPoint = self.CurrNavPoint + self.NavPointDirection
	
	if self.CurrNavPoint > #self.NavPoints then
		if self.PathEndOption == 0 then --loop
			self.CurrNavPoint = 1
		elseif self.PathEndOption == 1 then --reverse
			self.NavPointDirection = -1
			self.CurrNavPoint = #self.NavPoints - 1
		elseif self.PathEndOption == 2 then --stop
			self.CurrNavPoint = #self.NavPoints + 1
		end
	elseif self.CurrNavPoint < 1 then
		if self.PathEndOption == 1 then --reverse
			self.NavPointDirection = 1
			self.CurrNavPoint = 2
		end
	end
	
	return self.NavPoints[self.CurrNavPoint]
end

function Script:ChooseTarget()
	local entities = GetEntityNeighbors(self.entity,self.sightradius,true)
	local k,entity
	for k,entity in pairs(entities) do
		if entity.script.teamid~=nil and entity.script.teamid~=0 and entity.script.teamid~=self.teamid then
			if entity.script.health>0 then
				local d = self.entity:GetDistance(entity)
				local pickinfo=PickInfo()
				if self.entity.world:Pick(self.entity:GetPosition()+Vec3(0,1.6,0),entity:GetPosition()+Vec3(0,1.6,0),pickinfo,0,false,Collision.LineOfSight)==false then
					local dir_facing = Transform:Normal(0,0,1,self.entity,nil):Normalize()
					local dir_to_target = (entity:GetPosition() - self.entity:GetPosition()):Normalize()
					local angle = math.acos(dir_facing:Dot(dir_to_target))
					
					if angle > Math:Radians(self.FOV) then
						return entity.script
					end
				end
			end
		end
	end
end

function Script:DistanceToTarget()
	local pos = self.entity:GetPosition()
	local targetpos = self.target.entity:GetPosition()
	--if math.abs(targetpos.y-pos.y)<1.5 then
	if math.abs(targetpos.y-pos.y)<self.verticalHidingDeflection then	
		return pos:xz():DistanceToPoint(targetpos:xz())
	else
		return 100000--if they are on different vertical levels, assume they can't be reached
	end
end

function Script:TargetInRange()
	local pos = self.entity:GetPosition()
	local targetpos = self.target.entity:GetPosition()
	if math.abs(targetpos.y-pos.y)<1.5 then
		if pos:xz():DistanceToPoint(targetpos:xz())<self.attackrange then
			return true
		end
	end
	return false
end

function Script:Start()	

	self.muzzle = self.entity:FindChild("muzzle")
	self.muzzleflash = Sprite:Create()
	self.muzzleflash:SetSize(self.muzzleflashscale,self.muzzleflashscale)
	self.muzzleflash:SetCollisionType(0)
	local material = Material:Load("Materials/Effects/electroplasma.mat")
	self.muzzleflash:SetMaterial(material)
	self.muzzlelight = PointLight:Create()
	self.muzzlelight:SetColor(0.5,0.5,1)
	self.muzzlelight:SetRange(4)
	self.muzzleflash:SetShadowMode(0)
	local tag = self.entity:FindChild("muzzle")
	self.muzzlelight:SetParent(tag)
	self.muzzlelight:SetPosition(0,0,0)
	self.muzzlelight:Hide()
	self.muzzleflash:SetParent(self.muzzlelight,false)


	-- Create table here to make sure we have instance specific table.
	self.NavPoints={}

	self.font = Font:Load("Fonts/TulpenOne-Regular.ttf",32)
	if self.font==nil then
		local context = Context:GetCurrent()
		self.font = context:GetFont()
	end


	self.name = self.entity:GetKeyValue("name")
	if self.entity:GetMass()==0 then
		self.entity:SetMass(10)
	end
	self.entity:SetPickMode(Entity.BoxPick,true)
	self.entity:SetPickMode(0,false)
	self.entity:SetPhysicsMode(Entity.CharacterPhysics)
	self.entity:SetCollisionType(Collision.Prop,true)
	self.entity:SetCollisionType(Collision.Character,false)
	
	if self.ShowTargetBox == true then	
		self.targetBox = Model:Box(0.2,0.2,0.2)
		self.targetBox:SetPhysicsMode(Entity.RigidBodyPhysics)
		self.targetBox:SetCollisionType(Collision.None,true)

		if self.target~=nil then
			self.targetBox:SetPosition(self.target:GetPosition())
			--self.targetBox:SetParent(self.target)
		else
			self.targetBox:SetPosition(self.entity:GetPosition())
			--self.targetBox:SetParent(self.entity)
		end
	end
	
	if self.Path ~= nil then
		local i = 1
		local e
		repeat
			e = self.Path:FindChild(tostring(i))
			if e ~= nil then
				System:Print("navPoint:"..tostring(i))
				table.insert(self.NavPoints, e)
				i = i + 1
			end
		until e == nil
	end

	local count = 0
	for _ in pairs(self.NavPoints) do count = count + 1 end
	
	System:Print("self.NavPoints points:"..tostring(count))

	
	if self.enabled then
		if self.target~=nil then
			self:SetMode("roam")
			--self.targetBox:SetParent(self.target)
		else
			self:SetMode("idle")
		end
	end
	self.sound={}
	if self.alertsound then self.sound.alert = Sound:Load(self.alertsound) end
	self.sound.attack={}
	if self.attack1sound then self.sound.attack[1] = Sound:Load(self.attack1sound) end
	if self.attack2sound then self.sound.attack[2] = Sound:Load(self.attack2sound) end
	if self.idlesound then self.sound.idle = Sound:Load(self.idlesound) end
	if self.deathsound then self.sound.death = Sound:Load(self.deathsound) end

	if self.explosionSoundPath == nil then
		self.explosionSoundPath = "AddOns/MDG_Explosion/Sounds/explosion02.wav"
	end
	if self.explosionSoundPath then
		self.sound.explosion= Sound:Load(self.explosionSoundPath)
		--self.explosionSound:AddRef()
	end
	if self.sound.explosion==nil then Debug:Error("self.sound.explosion could not loaded!") end

	if self.explosionPrefabPath == nil then
		self.explosionPrefabPath = "Addons/MDG_Explosion/Prefabs/pfb_explosion.pfb"
	end
	if self.explosionPrefabPath then
		self.explosionPrefab = Prefab:Load(self.explosionPrefabPath)
		self.explosionPrefab.explosionLifeTime = -1
		self.explosionPrefab:AddRef()
		self.explosionPrefab:Hide()
		if self.disablePrefab and type(self.explosionPrefab.script["Disable"])=="function" then
			self.explosionPrefab.script:Disable()
		end
	end
	if self.explosionPrefab==nil then Debug:Error("Item pefab could not loaded!") end

	self.lastidlesoundtime=Time:GetCurrent()+math.random(1,20000)

end

function Script:Hide()
	self.entity:Hide()
	self.muzzlelight:Hide()
end


function Script:Hurt(damage,distributorOfPain)

	if self.health>0 then
		if self.target==nil then
			
			if distributorOfPain.entity ~= nil then			
				distributorOfPain = distributorOfPain.script
			end
			
			self.target=distributorOfPain
			--self:SetTargetBox(distributorOfPain)
			self:SetMode("attack")
		end
		self.health = self.health - damage
		if self.health<=0 then
			System:Print(tostring(self.entity:GetKeyValue("name")).." is dieing")			
			self.entity:SetMass(0)
			self.entity:SetCollisionType(0)
			self.entity:SetPhysicsMode(Entity.RigidBodyPhysics)
			self:SetMode("dying")
		else
			if self.target==nil then
				self.target=distributorOfPain
				self:SetMode("attack")
			end			
		end
	end

end

function Script:SetTargetBox(target)

	if not self.ShowTargetBox then
		return
	end

	System:Print("** SetTargetBox **")
	--self.target=distributorOfPain
	if target == nil then
		System:Print("target NIL")
	else
		System:Print("setting target")
		local pos = target:GetPosition(true)
		self.targetBox:SetPosition(pos)
		--self.targetBox:SetParent(target)
	end

end

function Script:EndDeath()
	self.entity:EmitSound(self.sound.death)
	self:SetMode("dead")
end

function Script:DirectMoveToTarget()

	self.entity:Stop()
	local targetpos = self.target.entity:GetPosition()
	local pos = self.entity:GetPosition()
	local dir = Vec2(targetpos.z-pos.z,targetpos.x-pos.x):Normalize()
	local angle = -Math:ATan2(dir.y,-dir.x) + self.entity:GetCharacterControllerAngle() + 180.0
	self.entity:SetInput(angle,self.speed)

end

function Script:SetMode(mode)

	if self.debug then
		System:Print("-- MonsterAI:SetMode - mode:"..mode)
	end
	if self.debug and mode == "attack" then
		System:Print("-- MonsterAI:SetMode - mode:"..mode)
	end
	
	if mode~=self.mode then
		local prevmode=self.mode
		self.mode=mode
		if mode=="idle" then
			self.target=nil
			--self.animationmanager:SetAnimationSequence("Idle",0.02)
			--self.animationmanager.animations[1].frameoffset = math.random(1,1000)
			if self.NavPoint == nil then
				self.NavPoint = self:GetNextNavPoint()
			end
			
			if self.NavPoint ~= nil then
				self:SetMode("roam")
			else
				self.entity:Stop()
				self.entity:PlayAnimation("Idle", 0.02)
				self.entity:SetAnimationFrame(math.random(1,1000))
			end

			--self.entity:Stop()--stop following anything
		elseif mode=="roam" then
		
			if self.NavPoint ~= nil then
				--self.entity:PlayAnimation("Run",self.animspeedrun)
				--self.entity:GoToPoint(self.NavPoint:GetPosition(true),5,5)
				self.entity:PlayAnimation("Walk",self.animspeedrun)
				self:SetTargetBox(self.NavPoint)
				local pos = self.NavPoint:GetPosition(true)
				System:Print("navpoint.x:"..tostring(pos.x).."navpoint.y:"..tostring(pos.y).."navpoint.y:"..tostring(pos.z))
				self.entity:GoToPoint(self.NavPoint:GetPosition(true),2.5,3.5)		
		
			--if self.target~=nil then
				--self.animationmanager:SetAnimationSequence("Run",self.animspeedrun)
				--self.entity:PlayAnimation("Run",self.animspeedrun)
				--self.entity:GoToPoint(self.target.entity:GetPosition(true),5,5)
			else
				self:SetMode("idle")
			end
		elseif mode=="attack" then
			self:EndAttack()
		elseif mode=="chase" then
			if self.entity:Follow(self.target.entity,self.speed,self.maxaccel) then
				if prevmode~="chase" then
					if self.sound.alert then self.entity:EmitSound(self.sound.alert) end
				end
				self.followingtarget=true
				--self.animationmanager:SetAnimationSequence("Run",self.animspeedrun,300)
				self.entity:PlayAnimation("Run",self.animspeedrun,300)
				if self:DistanceToTarget()<self.attackrange*2 then
					self.followingtarget=false
					self.entity:Stop()
					self:DirectMoveToTarget()
				end
			else
				self.target=nil
				self:SetMode("idle")
				return
			end
		elseif mode=="dying" then
			self.entity:Stop()
			--self.animationmanager:SetAnimationSequence("Death",0.04,300,1,self,self.EndDeath)			
			if self.entity:FindAnimationSequence("Death")>-1 then
				self:Explosion()
				self.entity:PlayAnimation("Death",0.25,300,1,"EndDeath")
			else
				--self:EndDeath()
				self.entity.visible = false
			end
		elseif mode=="dead" then
			self.entity:SetCollisionType(0)
			self.entity:SetMass(0)
			self.entity:SetShape(nil)
			self.entity:SetPhysicsMode(Entity.RigidBodyPhysics)
			self.enabled=false
			--self.entity:SetRotation(90,0,0)
			self.deathTime = Time:GetCurrent()
		elseif mode=="npa" then
			self.entity:Stop()
			
			if self.NavPoint.script.Arrive ~= nil then
				self.NavPoint.script:Arrive(self)
			end
			
			if self.NavPoint.script.Action == 0 then--delay
				self.NavPointDelay = Time:GetCurrent() + self.NavPoint.script.ActionArg
				self.entity:PlayAnimation(self.NavPoint.script.Animation,self.NavPoint.script.AnimationSpeed,300)
			elseif self.NavPoint.script.Action == 1 then--animation x times
				self.NavPointAnimationCount = self.NavPoint.script.ActionArg
				self.entity:PlayAnimation(self.NavPoint.script.Animation,self.NavPoint.script.AnimationSpeed,300,1,"NpaEnd")
			end
			
		end
	end

end

function Script:NpaEnd()

	self.NavPointAnimationCount = self.NavPointAnimationCount - 1

	if self.NavPointAnimationCount > 0 then
		self.entity:PlayAnimation(self.NavPoint.script.Animation,self.NavPoint.script.AnimationSpeed,300,1,"NpaEnd")
	else
		if self.NavPoint.script.Depart ~= nil then
			self.NavPoint.script:Depart(self)
		end

		self.NavPoint = nil
		self:SetMode("idle")
		
	end

end

function Script:EndAttack()

	System:Print("-- MonsterAI:EndAttack --")
	if self.mode=="attack" then	
		if self.target==nil then
			self:SetMode("idle")
			return
		end
		if self.target.health<=0 then
			self:SetMode("idle")
			return
		end
		local d = self:DistanceToTarget()
		if d>self.attackrange then
			self:SetMode("chase")
			return
		end
		self.entity:Stop()
		self.attackmode = 1-self.attackmode--switch between right and left attack modes	
		--self.animationmanager:SetAnimationSequence("Attack"..tostring(1+self.attackmode),0.05,300,1,self,self.EndAttack)
		

		self.firetime = Time:GetCurrent()
		self.muzzlelight:Point(self.target.entity,1)
		self.muzzlelight:Show()
		self.muzzleflash:SetAngle(math.random(0,360))
		--self.animationmanager:SetAnimationSequence("Fire"

		if self.entity:FindAnimationSequence("Attack")>-1 then
			self.entity:PlayAnimation("Attack"..tostring(1+self.attackmode),0.05,300,1,"EndAttack")
		end
		self.attackbegan = Time:GetCurrent()
		if self.sound.attack[self.attackmode+1] then
			if math.random()>0.75 then
				self.entity:EmitSound(self.sound.attack[self.attackmode+1])
			end
		end
	end

end


function Script:Draw()
	
	local t = Time:GetCurrent()

	if self.muzzlelight:Hidden()==false then
		if t-self.firetime>50 then
			self.muzzlelight:Hide()
		end
	end
end


function Script:UpdatePhysics()
	
	if self.enabled==false then return end
	
	
	--System:Print("mode.."..self.mode)

	if self.deathTime and self.entity:Hidden() == false then 
		if self.deathTime + self.releaseTIme < Time:GetCurrent() then 
			self.entity:Hide()
			self.deathTime = Time:GetCurrent()
			return
		end
		if(self.entity:Hidden() == true and self.deathTime) then  
			if self.deathTime + self.releaseTIme < Time:GetCurrent() then 
				System:Print("now Releasing...")
				self.entity:Release()
			end
		end
		
		return
	end	

	local t = Time:GetCurrent()
	self.entity:SetInput(self.entity:GetRotation().y,0)
	
	if self.sound.idle then
		if t-self.lastidlesoundtime>0 then
			self.lastidlesoundtime=t+20000*Math:Random(0.75,1.25)
			self.entity:EmitSound(self.sound.idle,20)
		end
	end
	
	if self.mode=="idle" then
		if t-self.lastupdatetargettime>self.idlePause then
			self.lastupdatetargettime=t
			local t = self:ChooseTarget()
			if t then
				self.target = t
				--self:SetTargetBox(t)
				self:SetMode("chase")
			end
		end
	elseif self.mode=="roam" then
		--if not self:TargetInRange() then
		if t-self.lastupdatetargettime>250 then
			--System:Print("Update Target")						
			self.lastupdatetargettime=t
			local t = self:ChooseTarget()
			if t then
				self.target = t
				System:Print("Assign Target")				
				--self:SetTargetBox(t)
				self:SetMode("chase")
			end
		elseif self.entity:GetDistance(self.NavPoint)<1 then
			if self.NavPoint.script ~= nil then
				self:SetMode("npa")
			else
				self.NavPoint = nil
				self:SetMode("idle")
			end
		end
	elseif self.mode=="npa" then
		if self.NavPoint.script.Action == 0 and t > self.NavPointDelay then
			if self.NavPoint.script.Depart ~= nil then
				self.NavPoint.script:Depart(self)
			end
			
			self.NavPoint = nil
			self:SetMode("idle")
		end		
		
		--if self.target ~= nil then
			--if self.target.entity:GetDistance(self.entity)<1 then
				--self:SetMode("idle")
			--end
			--if self.target.entity:GetDistance(self.entity) > self.sightradius*1.25 then
				--self:SetMode("idle")
			--end
		--end
	elseif self.mode=="chase" then
		if self.target.health<=0 then
			self:SetMode("idle")
			return
		end
		if self:TargetInRange() then
			self:SetMode("attack")
		elseif self:DistanceToTarget()<self.attackrange*2 then
			self.followingtarget=false
			self.entity:Stop()
			self:DirectMoveToTarget()
		elseif self:DistanceToTarget()>self.sightradius*1.25 then
			self.followingtarget=false
			self:SetMode("roam")
		else
			if self.followingtarget==false then
				if self.entity:Follow(self.target.entity,self.speed,self.maxaccel) then
					self:SetMode("idle")
				end
			end
		end
	elseif self.mode=="attack" then
		--System:Print("-- BEGIN ATTACK PLAYER --")
		if self.attackbegan~=nil then
			if t-self.attackbegan>self.attackdelay then
				if self.target.entity:GetDistance(self.entity)<1.5 then
					self.attackbegan=nil
					--Debug:Error("ATTACK PLAYER")
					self.target:Hurt(self.damage, self)
				else 
					self:EndAttack()					
				end
			end
		else 
			self:EndAttack()								
		end
		local pos = self.entity:GetPosition()
		if self.target ~= nil then
			local targetpos = self.target.entity:GetPosition()			
			local dx=targetpos.x-pos.x
			local dz=targetpos.z-pos.z
			if self.entity:GetCharacterControllerAngle()>90.0 then
				self.entity:AlignToVector(-dx,0,-dz)
			else
				self.entity:AlignToVector(dx,0,dz) 
			end
		end			
	end

	if self.explosionStartTime ~= nil and self.explosionLifeTime > 0 and Time:GetCurrent()-self.explosionStartTime>self.explosionLifeTime then
		if self.entity then
			System:Print("Release expired projectile object")
			--self:Disable()
			self:ReleaseItem()
		else
			System:Print("Entity no longer existing - Release not possible")
		end
	end

end
------------------------------------------------------------------------------
function Script:Disable()--in
	if self.enabled==true then
		System:Print("Projectile Disabled")
		self.enabled=false
	end
end
------------------------------------------------------------------------------
function Script:Explosion()

	-- Do Explosion
	if self.explosionPrefab~=nil then
		local explosion = self.explosionPrefab:Instance()
		explosion:SetPosition(self.entity:GetPosition())
		explosion:Show()
		-- Do explosion sound
		--explosion:EmitSound(self.sound.explosions[math.random(#self.sound.explosions)],30)
		explosion:EmitSound(self.sound.explosion,30)
		self.explosionStartTime=Time:GetCurrent()
	end

	if self.entity then
		System:Print("Release exploded projectile object")
		self:ReleaseItem()
	else
		System:Print("Entity no longer existing - Release not possible")
	end			
		
end

function Script:ReleaseItem()
	if self.explosionPrefab~=nil then
		self.explosionPrefab:Release()
		self.explosionPrefab=nil
		self.explosionStartTime = nil
	end
end


function Script:PostRender(context)

	context:SetBlendMode(Blend.Alpha)
	context:SetColor(1,0,0,1)
	local text
	local prevfont = context:GetFont()
	prevfont:AddRef()
	context:SetFont(self.font)
	local fh=self.font:GetHeight()
	self.timestring=""
	
	if self.debug then
		text="Mode:"..self.mode
		context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))-20,(context:GetHeight()-fh)/2-fh*9)
		text="Health: "..self.health
		context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))-20,(context:GetHeight()-fh)/2-fh*7.5)
		--text="Coins: "..self.coinscollected.."/"..TotalCoins
		--context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))/2,(context:GetHeight()-self.font:GetHeight())/2+fh*1.5)			
	end
	
	context:SetFont(prevfont)
	prevfont:Release()
	context:SetBlendMode(Blend.Solid)
	context:SetColor(1,1,1,1)

end