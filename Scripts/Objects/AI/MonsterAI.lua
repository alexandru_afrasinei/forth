import "Scripts/Functions/GetEntityNeighbors.lua"

--Public values
Script.health=40--int "Health"
Script.enabled=true--bool "Enabled"
Script.target=nil--entity "Target"
Script.sightradius=30--float "Sight Range"
Script.senseradius=2--float "Hearing Range"
Script.speed=4 -- float "Chase Speed"
Script.teamid=2--choice "Team" "Neutral,Good,Bad"
Script.attackdelay=300--int "Attack delay"
Script.animspeedrun=0.04--float "Run anim speed"
Script.attack1sound=""--path "Attack 1 sound" "Wav file (*.wav):wav|Sound"
Script.attack2sound=""--path "Attack 2 sound" "Wav file (*.wav):wav|Sound"
Script.alertsound=""--path "Alert sound" "Wav file (*.wav):wav|Sound"
Script.deathsound=""--path "Death sound" "Wav file (*.wav):wav|Sound"
Script.idlesound=""--path "Idle sound" "Wav file (*.wav):wav|Sound"
Script.idlePause=1000 --int "Idle Pause"
Script.debug=true --bool "Debug"

--Private values
Script.damage=5
Script.attackrange=1.5
Script.updatefrequency=500
Script.lastupdatetime=0
Script.prevtarget=nil
Script.followingtarget=false
Script.maxaccel=15

Script.lastupdatetargettime=0
Script.attackmode=0
Script.attackbegan=0
Script.verticalHidingDeflection = 3

function Script:Enable()--in
	if self.enabled==false then
		if self.health>0 then
			self.enabled=true
			if self.target~=nil then
				self:SetMode("roam")
			else
				self:SetMode("idle")
			end
		end
	end
end

function Script:ChooseTarget()
	local entities = GetEntityNeighbors(self.entity,self.sightradius,true)
	local k,entity
	for k,entity in pairs(entities) do
		if entity.script.teamid~=nil and entity.script.teamid~=0 and entity.script.teamid~=self.teamid then
			if entity.script.health>0 then
				local d = self.entity:GetDistance(entity)
				local pickinfo=PickInfo()
				if self.entity.world:Pick(self.entity:GetPosition()+Vec3(0,1.6,0),entity:GetPosition()+Vec3(0,1.6,0),pickinfo,0,false,Collision.LineOfSight)==false then
					return entity.script
				end
			end
		end
	end
end

function Script:DistanceToTarget()
	local pos = self.entity:GetPosition()
	local targetpos = self.target.entity:GetPosition()
	--if math.abs(targetpos.y-pos.y)<1.5 then
	if math.abs(targetpos.y-pos.y)<self.verticalHidingDeflection then	
		return pos:xz():DistanceToPoint(targetpos:xz())
	else
		return 100000--if they are on different vertical levels, assume they can't be reached
	end
end

function Script:TargetInRange()
	local pos = self.entity:GetPosition()
	local targetpos = self.target.entity:GetPosition()
	if math.abs(targetpos.y-pos.y)<1.5 then
		if pos:xz():DistanceToPoint(targetpos:xz())<self.attackrange then
			return true
		end
	end
	return false
end

function Script:Start()

	self.font = Font:Load("Fonts/TulpenOne-Regular.ttf",32)
	if self.font==nil then
		local context = Context:GetCurrent()
		self.font = context:GetFont()
	end

	if self.entity:GetMass()==0 then
		self.entity:SetMass(10)
	end
	self.entity:SetPickMode(Entity.BoxPick,true)
	self.entity:SetPickMode(0,false)
	self.entity:SetPhysicsMode(Entity.CharacterPhysics)
	self.entity:SetCollisionType(Collision.Prop,true)
	self.entity:SetCollisionType(Collision.Character,false)
	if self.enabled then
		if self.target~=nil then
			self:SetMode("roam")
		else
			self:SetMode("idle")
		end
	end
	self.sound={}
	if self.alertsound then self.sound.alert = Sound:Load(self.alertsound) end
	self.sound.attack={}
	if self.attack1sound then self.sound.attack[1] = Sound:Load(self.attack1sound) end
	if self.attack2sound then self.sound.attack[2] = Sound:Load(self.attack2sound) end
	if self.idlesound then self.sound.idle = Sound:Load(self.idlesound) end
	self.lastidlesoundtime=Time:GetCurrent()+math.random(1,20000)
end

function Script:Hurt(damage,distributorOfPain)
	if self.health>0 then
		if self.target==nil then
			self.target=distributorOfPain
			self:SetMode("attack")
		end
		self.health = self.health - damage
		if self.health<=0 then
			self.entity:SetMass(0)
			self.entity:SetCollisionType(0)
			self.entity:SetPhysicsMode(Entity.RigidBodyPhysics)
			self:SetMode("dying")
		end
	end
end

function Script:EndDeath()
	self:SetMode("dead")
end

function Script:DirectMoveToTarget()
	self.entity:Stop()
	local targetpos = self.target.entity:GetPosition()
	local pos = self.entity:GetPosition()
	local dir = Vec2(targetpos.z-pos.z,targetpos.x-pos.x):Normalize()
	local angle = -Math:ATan2(dir.y,-dir.x) + self.entity:GetCharacterControllerAngle() + 180.0
	self.entity:SetInput(angle,self.speed)
end

function Script:SetMode(mode)
	if self.debug then
		System:Print("-- MonsterAI:SetMode - mode:"..mode)
	end
	if self.debug and mode == "attack" then
		System:Print("-- MonsterAI:SetMode - mode:"..mode)
	end


	
	if mode~=self.mode then
		local prevmode=self.mode
		self.mode=mode
		if mode=="idle" then
			self.target=nil
			--self.animationmanager:SetAnimationSequence("Idle",0.02)
			--self.animationmanager.animations[1].frameoffset = math.random(1,1000)
			self.entity:PlayAnimation("Idle",0.02)
			self.entity:Stop()--stop following anything
		elseif mode=="roam" then
			if self.target~=nil then
				--self.animationmanager:SetAnimationSequence("Run",self.animspeedrun)
				self.entity:PlayAnimation("Run",self.animspeedrun)
				self.entity:GoToPoint(self.target.entity:GetPosition(true),5,5)
			else
				self:SetMode("idle")
			end
		elseif mode=="attack" then
			self:EndAttack()
		elseif mode=="chase" then
			if self.entity:Follow(self.target.entity,self.speed,self.maxaccel) then
				if prevmode~="chase" then
					if self.sound.alert then self.entity:EmitSound(self.sound.alert) end
				end
				self.followingtarget=true
				--self.animationmanager:SetAnimationSequence("Run",self.animspeedrun,300)
				self.entity:PlayAnimation("Run",self.animspeedrun,300)
				if self:DistanceToTarget()<self.attackrange*2 then
					self.followingtarget=false
					self.entity:Stop()
					self:DirectMoveToTarget()
				end
			else
				self.target=nil
				self:SetMode("idle")
				return
			end
		elseif mode=="dying" then
			self.entity:Stop()
			--self.animationmanager:SetAnimationSequence("Death",0.04,300,1,self,self.EndDeath)			
			if self.entity:FindAnimationSequence("Death")>-1 then
				self.entity:PlayAnimation("Death",0.04,300,1,"EndDeath")
			else
				--self:EndDeath()
			end
		elseif mode=="dead" then
			self.entity:SetCollisionType(0)
			self.entity:SetMass(0)
			self.entity:SetShape(nil)
			self.entity:SetPhysicsMode(Entity.RigidBodyPhysics)
			self.enabled=false
		end
	end
end

function Script:EndAttack()
	System:Print("-- MonsterAI:EndAttack --")
	if self.mode=="attack" then	
		if self.target==nil then
			self:SetMode("idle")
			return
		end
		if self.target.health<=0 then
			self:SetMode("idle")
			return
		end
		local d = self:DistanceToTarget()
		if d>self.attackrange then
			self:SetMode("chase")
			return
		end
		self.entity:Stop()
		self.attackmode = 1-self.attackmode--switch between right and left attack modes	
		--self.animationmanager:SetAnimationSequence("Attack"..tostring(1+self.attackmode),0.05,300,1,self,self.EndAttack)
		if self.entity:FindAnimationSequence("Attack")>-1 then		
			self.entity:PlayAnimation("Attack"..tostring(1+self.attackmode),0.05,300,1,"EndAttack")
		end
		self.attackbegan = Time:GetCurrent()
		if self.sound.attack[self.attackmode+1] then
			if math.random()>0.75 then
				self.entity:EmitSound(self.sound.attack[self.attackmode+1])
			end
		end
	end
end

function Script:UpdatePhysics()
	if self.enabled==false then return end

	local t = Time:GetCurrent()
	self.entity:SetInput(self.entity:GetRotation().y,0)
	
	if self.sound.idle then
		if t-self.lastidlesoundtime>0 then
			self.lastidlesoundtime=t+20000*Math:Random(0.75,1.25)
			self.entity:EmitSound(self.sound.idle,20)
		end
	end
	
	if self.mode=="idle" then
		if t-self.lastupdatetargettime>self.idlePause then
			self.lastupdatetargettime=t
			self.target = self:ChooseTarget()
			if self.target then
				self:SetMode("chase")
			end
		end
	elseif self.mode=="roam" then
		if not self:TargetInRange() then
			self:SetMode("idle")	
		end
		--if self.target ~= nil then
			--if self.target.entity:GetDistance(self.entity)<1 then
				--self:SetMode("idle")
			--end
			--if self.target.entity:GetDistance(self.entity) > self.sightradius*1.25 then
				--self:SetMode("idle")
			--end
		--end
	elseif self.mode=="chase" then
		if self.target.health<=0 then
			self:SetMode("idle")
			return
		end
		if self:TargetInRange() then
			self:SetMode("attack")
		elseif self:DistanceToTarget()<self.attackrange*2 then
			self.followingtarget=false
			self.entity:Stop()
			self:DirectMoveToTarget()
		elseif self:DistanceToTarget()>self.sightradius*1.25 then
			self.followingtarget=false
			self:SetMode("roam")
		else
			if self.followingtarget==false then
				if self.entity:Follow(self.target.entity,self.speed,self.maxaccel) then
					self:SetMode("idle")
				end
			end
		end
	elseif self.mode=="attack" then
		--System:Print("-- BEGIN ATTACK PLAYER --")
		if self.attackbegan~=nil then
			if t-self.attackbegan>self.attackdelay then
				if self.target.entity:GetDistance(self.entity)<1.5 then
					self.attackbegan=nil
					--Debug:Error("ATTACK PLAYER")
					self.target:Hurt(self.damage, self)
				else 
					self:EndAttack()					
				end
			end
		else 
			self:EndAttack()								
		end
		local pos = self.entity:GetPosition()
		if self.target ~= nil then
			local targetpos = self.target.entity:GetPosition()			
			local dx=targetpos.x-pos.x
			local dz=targetpos.z-pos.z
			if self.entity:GetCharacterControllerAngle()>90.0 then
				self.entity:AlignToVector(-dx,0,-dz)
			else
				self.entity:AlignToVector(dx,0,dz) 
			end
		end			
	end
end



function Script:PostRender(context)

	context:SetBlendMode(Blend.Alpha)
	context:SetColor(1,0,0,1)
	local text
	local prevfont = context:GetFont()
	prevfont:AddRef()
	context:SetFont(self.font)
	local fh=self.font:GetHeight()
	self.timestring=""
	
	if self.debug then
		text="Mode:"..self.mode
		context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))-20,(context:GetHeight()-fh)/2-fh*9)
		text="Health: "..self.health
		context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))-20,(context:GetHeight()-fh)/2-fh*7.5)
		--text="Coins: "..self.coinscollected.."/"..TotalCoins
		--context:DrawText(text,(context:GetWidth()-self.font:GetTextWidth(text))/2,(context:GetHeight()-self.font:GetHeight())/2+fh*1.5)			
	end
	
	context:SetFont(prevfont)
	prevfont:Release()
	context:SetBlendMode(Blend.Solid)
	context:SetColor(1,1,1,1)
end