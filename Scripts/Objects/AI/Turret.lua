Script.rot = nil
Script.rotOffset = Vec3(90,0,0)
function Script:Start()
	self.base = self.entity:GetChild(0)
	self.moveModelX = self.base:GetChild(0)
	self.moveModelY = self.moveModelX:GetChild(0)
	self.sprite = Sprite:Create()
	self.sprite:SetPosition(self.moveModelY:GetPosition(true))
end

--[[
function Script:UpdateWorld()
	
end
]]

function Script:UpdatePhysics()
	--Motion tracking
	self.moveModelX:Point(player,0)
	local rot = self.moveModelX:GetRotation()
	self.moveModelX:SetRotation(0,0,rot.z)
	--self.moveModelY:Point(player,0)
	--local rot = self.moveModelY:GetRotation()
	--self.moveModelY:SetRotation(rot.x,self.rotOffset.y,self.rotOffset.z)
end


--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	if showstats then
		context:SetBlendMode(Blend.Alpha)
		context:SetColor(1,0,0,1)
		context:DrawText(tostring(self.sprite:GetRotation().x),0,500)
		context:SetColor(1,1,1,1)
		context:SetBlendMode(Blend.Solid)
	end
end


--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]