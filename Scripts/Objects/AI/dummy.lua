Script.health = 100 --int
Script.keyValue = "enemy" --choice KeyValue enemy,player
function Script:Start()
	self.entity:SetKeyValue("type", self.keyValue)

end

function Script:Hurt(damage)
	if self.health>0 then
		self.health = self.health - damage
		if self.health<=0 then
			self.entity:SetMass(0)
			self.entity:SetCollisionType(0)
			self.entity:SetPhysicsMode(Entity.RigidBodyPhysics)
			self.entity:SetRotation(90,0,0)
		end
	end
end