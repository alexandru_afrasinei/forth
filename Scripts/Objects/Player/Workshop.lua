Script.lookspeed = 0.2--float "Look speed"
Script.looksmoothing = 5--float "Look smoothing"
Script.cameradistance = 5--float "Camera distance"
Script.cameraradius = 0
Script.jumpstrength = 8--float "Jump strength"
Script.walkspeed = 5--float "Walk speed"
Script.runspeed = 8--float "Run speed"
Script.camerapitch = 45--float "Camera pitch"
Script.lockcameraangle = false--bool "Lock camera"
Script.cameracollision = true--bool "Camera collision"

function Script:Start()
	if self.entity:GetMass()==0 then
		self.entity:SetMass(10)
	end
	self.entity:SetPhysicsMode(Entity.CharacterPhysics)
	self.camera = Camera:Create()
	self.angle = self.entity:GetRotation().y
	self.cameraangle = self.angle + self.entity:GetCharacterControllerAngle()
end

function Script:UpdateWorld()
	local playerpos = self.entity:GetPosition(true)
	local aabb = self.entity:GetAABB(Entity.GlobalAABB)
	playerpos.y = aabb.center.y
	self.camera:SetPosition(playerpos)
	
	self.cameraangle = Math:CurveAngle(self.angle + self.entity:GetCharacterControllerAngle(),self.cameraangle,self.looksmoothing / Time:GetSpeed())
	
	if self.lockcameraangle==false then
		self.camera:SetRotation(0,self.cameraangle,0)
		self.camera:Turn(self.camerapitch,0,0)
	else
		self.camera:SetRotation(self.camerapitch,0,0)
	end
	
	local camerapos = Transform:Point(0,0,-self.cameradistance,self.camera,nil)
	
	local pickmode = self.entity:GetPickMode()
	self.entity:SetPickMode(0)
	
	if self.cameracollision then
		local pickinfo = PickInfo()
		if self.entity.world:Pick(playerpos,camerapos,pickinfo,self.cameraradius) then
			self.camera:SetPosition(pickinfo.position,true)
		else
			self.camera:SetPosition(camerapos,true)
		end
	else
		self.camera:SetPosition(camerapos,true)
	end

	self.entity:SetPickMode(pickmode)
end

function Script:UpdatePhysics()
	local window = Window:GetCurrent()
	local context = Context:GetCurrent()

	local cx = context:GetWidth()/2
	local cy = context:GetHeight()/2
	
	local newmode
	local animmode=0

	if self.entity:GetAirborne()==false then
		newmode = "idle"
	end
	
	if self.lockcameraangle==false then
		local mousepos = window:GetMousePosition()
		window:SetMousePosition(cx,cy)
		
		local mx = Math:Clamp(mousepos.x - cx,-45,45)
		local my = Math:Clamp(mousepos.y - cy,-45,45)
		
		if self.smoothedmousemovement~=nil then
			self.angle = self.angle + mx * self.lookspeed
		else
			self.smoothedmousemovement=Vec2(0)
		end
		
		self.smoothedmousemovement.x = Math:Curve(mx,self.smoothedmousemovement.x,self.looksmoothing)
		self.smoothedmousemovement.y = Math:Curve(my,self.smoothedmousemovement.y,self.looksmoothing)
	end
	
	local move=0
	local strafe=0
	local movespeed = 5
	if window:KeyDown(Key.W) then
		move = move + 1
	end
	if window:KeyDown(Key.S) then
		move = move - 1
	end
	if window:KeyDown(Key.D) then
		strafe = strafe + 1
	end
	if window:KeyDown(Key.A) then
		strafe = strafe - 1
	end
	
	if self.lockcameraangle==true then
		if move~=0 or strafe~=0 then
			self.angle = Math:IncAngle(Math:ATan2(strafe,move)+self.entity:GetCharacterControllerAngle(),self.angle,5)
			move = 1
			strafe = 0
		end
	end

	local jump = 0
	if window:KeyHit(Key.Space) then
		if self.entity:GetAirborne()==false then
			jump = self.jumpstrength
			newmode = "jump"
			animmode = 1--one-shot animation
		end
	end
	
	if window:KeyDown(Key.Shift) then
		move = move * self.runspeed
		strafe = strafe * self.runspeed
		if self.entity:GetAirborne()==false and (move~=0 or strafe~=0) then
			newmode = "run"
		end
	else
		move = move * self.walkspeed
		strafe = strafe * self.walkspeed
		if self.entity:GetAirborne()==false and (move~=0 or strafe~=0) then
			newmode = "walk"
		end
	end
	
	self.entity:SetInput(self.angle,move,strafe,jump)
	
	if newmode~=self.mode then
		self.mode = newmode
		if self.mode~=nil then
			self.entity:PlayAnimation(self.mode,0.1,200,animmode)
		end
	end
	
end

function Script:Detach()
	self.camera:Release()
end