--[[
****************************************************
 * Third Person Controller script made by Friedrich Betz (Slastraf).
 * 
 * This file is part of Leadwerks Open Project "forth".
 * 
 * This script is public domain but is encouraged to use with Leadwerks Projects.
 ***************************************************
--]]

Script.cameraOffset = Vec3(.5,1.5,-.5) --Vec3 "Camera Offset"

Script.speedMultiplier = 1.5 	--float "Run Multiplier"
Script.moveSpeed = 2.5 		--float "Move Speed"
Script.mouseSensitivity = 10 --float "Mouse Sensitivity"
Script.jumpForce = 8 		--float "Jump Force"

Script.currentMousePos = nil
Script.mouseDifference = Vec2(0,0)
Script.input={}
Script.yRotOffset = 0

function Script:Start()
	--A non childed Pivot which is the origin of the camera pos / rot  axis
	self.originPivot = Pivot:Create()
	self.originPivot:SetPosition(self.entity:GetPosition(true)+Vec3(0,1,0),true)
	
	--Create a camera
	self.camera = Camera:Create(self.originPivot)
	self.camera:SetFOV(90)
	self.camera:SetRange(0.05,1000)
	
	--Set the camera position at correct Position
	self.camera:SetPosition(self.originPivot.position + self.cameraOffset,true)
	self.camera:Point(self.entity)
	self.camera:SetRotation(self.camera:GetRotation().x,0,self.camera:GetRotation().z)

	self.mouseSensitivity = self.mouseSensitivity/50
	self.char = self.entity:GetChild(0)
	self.char:SetParent(nil)

	self.char:SetPickMode(0)
	
end

function Script:UpdatePhysics()
	--char model smoothing
	self.char:SetPosition(self:LerpTo(self.char.position, self.entity.position, 0.3))
	
	--Player Movement
	local movex=0
	local movez=0
	self.input[0]=0
	self.input[1]=0
	if window:KeyDown(Key.W) then self.input[1]=self.input[1]+1 end
	if window:KeyDown(Key.S) then self.input[1]=self.input[1]-1 end
	if window:KeyDown(Key.D) then self.input[0]=self.input[0]+1 end
	if window:KeyDown(Key.A) then self.input[0]=self.input[0]-1 end
	
	local playerMovement = Vec3(0)
	playerMovement.x = self.input[0] * self.moveSpeed
	playerMovement.z = self.input[1] * self.moveSpeed
	
	--This prevents "speed hack" strafing due to lazy programming
	if self.input[0]~=0 and self.input[1]~=0 then
		playerMovement = playerMovement * 0.70710678
	end

	-- Check for jumping
	local jump = 0
	if window:KeyHit(Key.Space) and self:IsAirborne() == 0 then
		jump = self.jumpForce
		--Give the player an extra boost when jumping
		playerMovement = playerMovement * 1.6
	end

	if window:KeyDown(Key.Shift) then
		playerMovement.z = playerMovement.z  * self.speedMultiplier
	end

	--first set the origin rot position
	self.originPivot:SetPosition(self:LerpTo( self.originPivot:GetPosition(), self.entity:GetPosition(true)+Vec3(0,self.cameraOffset.y,0) ,0.3))
	self.char:SetRotation(0,self.originPivot:GetRotation().y,0)
	
	--we then can safely set the rotation to our originRot.y because it is not a child of the player.
	self.entity:SetInput(self.originPivot:GetRotation().y , playerMovement.z, playerMovement.x, jump , false, 1.0, 0.5, true)
	--[[set the camera nearer to the player if theres stuff inbetween
	local pickinfo=PickInfo()
	if (world:Pick(self.originPivot:GetPosition(true), self.camera:GetPosition(true), pickinfo, 0, true)) == true then
		System:Print(pickinfo.entity:GetPosition())
	else
		--Set the camera position at correct Position
		--self.camera:SetPosition(self.originPivot.position + self.cameraOffset)
	end
	--]]
end

function Script:UpdateWorld()
	local window = Window:GetCurrent()
	local context=Context:GetCurrent()

	self.currentMousePos = window:GetMousePosition()
	window:SetMousePosition(Math:Round(context:GetWidth()/2), Math:Round(context:GetHeight()/2))
	local centerpos = window:GetMousePosition()
	self.currentMousePos.x = Math:Round(self.currentMousePos.x)
	self.currentMousePos.y = Math:Round(self.currentMousePos.y)
	
	self.mouseDifference.x = Math:Curve(self.currentMousePos.x - centerpos.x,self.mouseDifference.x,2/Time:GetSpeed())
	self.mouseDifference.y = Math:Curve(self.currentMousePos.y - centerpos.y,self.mouseDifference.y,2/Time:GetSpeed())
	
	-- calculate a new rotation for the originPivot , which is the old one plus the offset from the mouse to the center of screen
	self.originPivot:SetRotation(self.originPivot:GetRotation().x + (self.mouseDifference.y * self.mouseSensitivity),
								self.originPivot:GetRotation().y + (self.mouseDifference.x * self.mouseSensitivity),
								self.originPivot:GetRotation().z,true)
end

--Return whether the player is airborne
function Script:IsAirborne()
	return self.entity:GetAirborne() and 1 or 0
end

function Script:LerpTo(Vec3from, Vec3to, Psmoothing)
	return Vec3(
		Math:Lerp(Vec3from.x, Vec3to.x, Psmoothing),
		Math:Lerp(Vec3from.y, Vec3to.y, Psmoothing),
		Math:Lerp(Vec3from.z, Vec3to.z, Psmoothing))
end