--[[
****************************************************
 * Third Person Controller script made by Friedrich Betz (Slastraf).
 * 
 * This file is part of Leadwerks Open Project "forth".
 * 
 * This script is public domain but is encouraged to use with Leadwerks Projects.
 ***************************************************
--]]

--Cam rotation speed respective to x and y axis of window
Script.rotationSpeed = Vec2(7,6) --Vec2 "Cam Rotation Speed"
Script.mouseSensitivity = 15 	--float "Mouse sensitivity"
Script.lookAtOffset = nil
Script.lookAtOffsetVector = Vec3(0.7, 2.0, 0) --Vec3 "Camera Position Offset"
Script.camDistance = 1 --float "Camera Distance Behind"
Script.camPositionOffset = nil
Script.camPositionOffsetVector = Vec3(0, 0, 0)

Script.char = nil
Script.thirdPerson = true
Script.camSmoothing = 	.1	--float "Cam Smooth Speed"

Script.health = 100		--float "Health"
Script.maxHealth = 100		--float "Max Health" 
Script.moveSpeed = 2.5 		--float "Move Speed"
Script.speedMultiplier = 1.5 	--float "Run Multiplier"
Script.strafeSpeed = 4 		--float "Strafe Speed"
Script.playerHeight = 1.8 	--float "Player Height"
Script.jumpForce = 8 		--float "Jump Force"
--Script.lookDistance = 20

-- Private Variables
Script.oRot = 0.0
Script.addRotation = Vec2(0,0)
Script.flashlighton = false	
Script.useDistance = 2
Script.alive=true
Script.footstepwalkdelay = 500
Script.footsteprundelay = 300
Script.maxcarryweight=5
Script.throwforce = 500
Script.isairborne=false
Script.input={}
Script.mouseDifference = Vec2(0,0)
Script.currentMousePos = Vec2(0,0)
Script.playerMovement = Vec3(0)
function Script:Start()
	--[[
	****************************************************
	 * Hierarchy of Generated Pivots
	 * 
	 * self.entity
	 * 		self.lookAtOffset -- is the pivot the camera points toward
	 * 		self.originPivot -- rotates the camera on relative to player axis
	 * 			self.camPositionOffset -- is the pivot the camera follows smoothly
	 * 
	 ***************************************************
	--]]
	
	self.char = self.entity:GetChild(0)
	self.char:SetParent(nil)
	self.camPositionOffsetVector = Vec3(self.lookAtOffsetVector.x, self.lookAtOffsetVector.y,-self.camDistance)
	self.lookAtOffset = Pivot:Create(self.entity)
	self.lookAtOffset:SetPosition(Vec3(self.lookAtOffsetVector.x, self.lookAtOffsetVector.y, 0))
	
	--We want to make a new dummy pivot at the same position of the player-
	--to calculate where the position offset of the camera is without actually rotating the player
	--when we move the mouse
	self.originPivot = Pivot:Create(self.entity)
	self.originPivot:SetPosition(self.entity:GetPosition(true),true)
	self.camPositionOffset = Pivot:Create(self.originPivot)
	self.camPositionOffset:SetPosition(self.camPositionOffsetVector)
	
	local window = Window:GetCurrent()
	
	self.camRotation = self.entity:GetRotation(true)
	self.entity:SetPickMode(0)
	
	--Set the type for this object to player
	self.entity:SetKeyValue("type","player")
	
	local mass = self.entity:GetMass()
	if self.entity:GetMass()==0 then Debug:Error("Player mass should be greater than 0.") end

	--Create a camera
	self.camera = Camera:Create()
	self.camera:SetFOV(90)
	self.camera:SetRange(0.05,1000)

	--Set the camera position at correct Position
	self.camera:SetPosition(self.entity:GetPosition(false)+self.camPositionOffset:GetPosition(false))

	--self.camera:SetMultisampleMode((System:GetProperty("multisample","1")))
	local material = Material:Create()
	material:SetBlendMode(5)--Blend.Invisible
	self.entity:SetMaterial(material)
	material:Release()
	self.entity:SetShadowMode(0)
	--Create listener
	self.listener = Listener:Create(self.camera)	
	--[[
	--Hitbox for bullets
	self.entity:SetCollisionType(Collision.Character)
	local hitbox = Model:Box(0.5,1.8,0.5)
	hitbox:SetPosition(self.entity:GetPosition(true))
	hitbox:Translate(0,0.9,0)
	hitbox:SetParent(self.entity,true)	
	hitbox:SetCollisionType(Collision.Prop)
	hitbox:SetShadowMode(0)
	--]]
	local context = Context:GetCurrent()
	window:SetMousePosition(Math:Round(context:GetWidth()/2), Math:Round(context:GetHeight()/2))
	
	self.camera:AlignToVector(Vec3(0,0,0))--make Camera look forward
end


--[[
function Script:UpdateWorld()
	
end
]]

function Script:UpdatePhysics()
	--System:Print(self.originPivot:GetPosition(true))
	
	--Player Movement
	local movex=0
	local movez=0
	self.input[0]=0
	self.input[1]=0
	if window:KeyDown(Key.W) then self.input[1]=self.input[1]+1 end
	if window:KeyDown(Key.S) then self.input[1]=self.input[1]-1 end
	if window:KeyDown(Key.D) then self.input[0]=self.input[0]+1 end
	if window:KeyDown(Key.A) then self.input[0]=self.input[0]-1 end

	self.playerMovement.x = self.input[0] * self.moveSpeed
	self.playerMovement.z = self.input[1] * self.moveSpeed
	
	--This prevents "speed hack" strafing due to lazy programming
	if self.input[0]~=0 and self.input[1]~=0 then
		self.playerMovement = self.playerMovement * 0.70710678
	end

	--if self.entity:GetAirborne() then
	--	self.playerMovement = self.playerMovement * 0.2
	--end
	
	--[[Check for running with shift and when not carrying anything
	if self.carryingEntity == nil and window:KeyDown(Key.Shift) then
		self.playerMovement.z = self.playerMovement.z  * self.speedMultiplier
	end
	--]]
	--[[
	local pSelf = self.camera:GetPosition(true)
	local pForward = Transform:Point(0,0,self.lookDistance,self.camera,nil)
	--]]

end

function Script:UpdateWorld()
	if self.thirdPerson == true then --if third person
		--System:Print(self.lookAtOffsetVector.position)
		--self.originPivot:SetPosition(Vec3(self.originPivot.position.x-(Math:Sin(Time:GetCurrent()/10)/500.0),self.originPivot.position.y,self.originPivot.position.z))
		if window:KeyHit(Key.Q) then -- make camera appear on the left side, and where the camera looks at
			self.lookAtOffset:SetPosition(self:RecalcPivotPos(-1),false)
			self.originPivot:SetPosition(Vec3(-self.lookAtOffsetVector.x*2,0,0))
			System:Print(self.originPivot.position)
		end
		
		if window:KeyHit(Key.E) then -- make camera appear on the right side, and where the camera looks at
			self.lookAtOffset:SetPosition(self:RecalcPivotPos( 1),false)
			-- the camera is a child of the originPivot, so position needs to be set on global axis
			self.originPivot:SetPosition(Vec3(0,0,0))
		end
		
		--here we calculate a value to actually add to the rotation depending from the mouse difference
		self.addRotation = 
		Vec2( Math:Clamp(self.mouseDifference.x / self.mouseSensitivity,-self.rotationSpeed.x, self.rotationSpeed.x),
		Math:Clamp(self.mouseDifference.y / self.mouseSensitivity,-self.rotationSpeed.y, self.rotationSpeed.y) )
		
		self.oRot = self.originPivot:GetRotation()
		self.originPivot:SetRotation(Math:Clamp(self.oRot.x,-40,20), 0, 0,false)
		--invert x axis movement
		--if(self.camMode == 2) then self.addRotation.x = -self.addRotation.x end
		--y camera axis rotation
		--we don't want to rotate all around, just from -40 to 40 degrees
		
		-- Third Person Mouse Look
		-- We want to calculate character Rotation depending on Mouse position vector to screen center
		self.currentMousePos = window:GetMousePosition()

		--this part is basically from fpsplayer script
		window:SetMousePosition(Math:Round(context:GetWidth()/2), Math:Round(context:GetHeight()/2))
		local centerpos = window:GetMousePosition()
		self.currentMousePos.x = Math:Round(self.currentMousePos.x)
		self.currentMousePos.y = Math:Round(self.currentMousePos.y)
		self.mouseDifference.x = Math:Curve(self.currentMousePos.x - centerpos.x,self.mouseDifference.x,2/Time:GetSpeed())
		self.mouseDifference.y = Math:Curve(self.currentMousePos.y - centerpos.y,self.mouseDifference.y,2/Time:GetSpeed())
		
		-- Check for jumping
		local jump = 0
		if window:KeyHit(Key.Space) and self:IsAirborne() == 0 then
			jump = self.jumpForce
			
			--Give the player an extra boost when jumping
			self.playerMovement = self.playerMovement * 1.6
		end
		
		--y camera axis rotation
		self.oRot = self.originPivot:GetRotation()
		--we don't want to rotate all around, just from -40 to 40 degrees
		self.originPivot:SetRotation(Math:Clamp(self.oRot.x+self.addRotation.y,-40,40),self.oRot.y,self.oRot.z)
		self.char:SetRotation(0,self.entity.rotation.y,0)
		self.char:SetPosition(self:LerpTo(self.char.position, self.entity.position, 0.1))
		--x axis rotation and input movement of character
		self.entity:SetInput(self.entity:GetRotation(true).y+self.addRotation.x, self.playerMovement.z, self.playerMovement.x, jump , false, 1.0, 0.5, true)

		self.camera:SetPosition(self:LerpTo(self.camera.position, self.camPositionOffset:GetPosition(true), self.camSmoothing))
		self.camera:Point(self.lookAtOffset)
	end
	--[[
	else --if fps make first person camera
			self.char:Hide()
			self.camPositionOffset:SetPosition(0, 1.6, 0, false)
			self.lookAtOffset:SetPosition(self.lookAtOffsetVector.x, self.lookAtOffsetVector.y, 0, false) 
	end
	--]]
end

--Return whether the player is airborne
function Script:IsAirborne()
	return self.entity:GetAirborne() and 1 or 0
end

function Script:RecalcPivotPos(multiplierOnX)
	return Vec3(self.lookAtOffsetVector.x*multiplierOnX,  self.lookAtOffsetVector.y, 0) 
end	

function Script:LerpTo(Vec3from, Vec3to, Psmoothing)
	return Vec3(
		Math:Lerp(Vec3from.x, Vec3to.x, Psmoothing),
		Math:Lerp(Vec3from.y, Vec3to.y, Psmoothing),
		Math:Lerp(Vec3from.z, Vec3to.z, Psmoothing))
end

--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]