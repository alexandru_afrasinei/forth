-- Public
Script.intensity = 0.5--float "Intensity"
Script.decay = 0.05--float "Decay"
Script.enabled = false--bool "Enabled"
-- Private
Script.shake = 0


function Script:Start()
	
end

function Script:Shake()--in
	self.shake = self.intensity
	self.enabled = true
end

function Script:UpdateWorld()
	if self.enabled then
		if self.shake > 0 then
			self.newPos = self.entity:GetPosition()
			self.newPos.x = self.newPos.x + Math:Random(-.5, .5) * self.shake
			self.newPos.y = self.newPos.y + Math:Random(-.5, .5) * self.shake
			self.newPos.z = self.newPos.z + Math:Random(-.5, .5) * self.shake

			self.entity:SetPosition(self.newPos)

			self.shake = self.shake - self.decay
		else
			self.enabled = false
		end
	else
		self:Shake()
	end
end