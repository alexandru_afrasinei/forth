Option 1:
-- F?ull spritesheet size
Script.texture_W = 300
Script.texture_H = 100
-- Size of 1 frame
Script.original_w = 50
Script.original_H = 50
-- Current Frame Position
Script.currentAnim_X = 0
Script.currentAnim_Y = 0
Script.animate = false
function Start()
self.sprite = Sprite:Create()
self.sprite_Material = Material:Load("*material name*.mat")
self.sprite:SetMaterial(self.sprite_Material)
local texture = self.sprite_Material:GetTexture()
self.texture_W = texture:GetWidth()
self.texture_H = texture:GetHeight()
self.sprite.Shader = self.sprite_Material:GetShader()
end
function UpdateWorld()
if window:KeyHit(Key.Space) then self.animate = true end
if self.animate then
 self.currentAnim_X = Math:Inc((self.texture_W/self.original_W)-1,self.currentAnim_X,1)
 if self.currentAnim_X == (self.texture_W/self.original_W)-1 then
  self.animate = false
  -- I used this method to check if the animation reached the last spot of the spritesheet or not. If not, reset it to 0 on the next layer. If
  --yes, keep it at the end.
  self.currentAnim_X = (self.currentAnim_Y == 2 and (self.texture_W/self.original_W)-1 or 0) 

  -- Check if this is on the last row of the animation, this can probably be done better but i have very limited time in my project hence
  -- the dirty way ( I would recommended checking the Height of your sheet and dividing it by the height of the frame )
  if self.currentAnim_Y < 2 then self.currentAnim_Y = self.currentAnim_Y + 1 end
 end
end

self.shader_Spritesheet:SetVec2("uvcoords", Vec2(self.currentAnim_X,self.currentAnim_Y))--set uvcoords in shader
self.shader_Spritesheet:SetVec2("zoom", Vec2(self.texture_W/self.original_W,self.texture_H/self.original_H))--set zoom in shader
end