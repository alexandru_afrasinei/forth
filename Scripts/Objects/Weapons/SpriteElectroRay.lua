Script.lifetime=1000
Script.enabled=false--bool "Enabled"
Script.distributor = nil
Script.enemy = nil

function Script:Start()
	self.starttime = Time:GetCurrent()
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
	end
end

function Script:UpdatePhysics()
	if Time:GetCurrent()-self.starttime>self.lifetime then
		self.entity:Release()
	end
end