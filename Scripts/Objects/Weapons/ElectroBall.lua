Script.damageRange = 3
Script.damage = 10 --int "Damage per 50 ms"
Script.movespeed = .05
Script.pickradius = 0
Script.damage = 500
Script.lifetime=5000
Script.enabled=true--bool "Enabled"
Script.directionVector = Vec3(0,0,0) 
Script.camera = nil
Script.face = nil


function Script:Start()
	self.gravity = world:GetGravity().y
	self.mass = self.entity:GetMass()
	self.starttime=Time:GetCurrent()
	self.entity:SetRotation(self.directionVector)
	self.directionVector = self.directionVector * Vec3(self.movespeed,self.movespeed,self.movespeed)
	self.face = self.entity:GetChild(0)
	self.face:Show()
	self.world=World:GetCurrent()
	
	if self.camera==nil then
		for i=0,self.world:CountEntities()-1 do
			if self.world:GetEntity(i):GetClass()==Object.CameraClass then
				self.camera=self.world:GetEntity(i)
				tolua.cast(self.camera,"Camera")
				break
			end
		end
	end
	self.repeatPick = 50
	self.repeatAABB = 500
	self.lastUpdateTime = 0
end

function Script:FindScriptedParent(entity1,func)
	while entity1~=nil do
		--System:Print("Iterating: " ..tostring(entity1:GetKeyValue("name")))
		if entity1.script then
			if type(entity1.script[func])=="function" then
				--System:Print("found "..tostring(entity1:GetKeyValue("name")))
				return entity1
			end
		end
		entity1 = entity1:GetParent()
	end
	return nil
end

function Script:UpdateWorld()
	if Time:GetCurrent()-self.starttime > self.lifetime then self.entity:Release() end

	if self.camera then self.face:Point(self.camera,1,1) end
	if self.directionVector == nil then return end
	
	if(Time:GetCurrent() > self.lastUpdateTime + self.repeatPick) then
		if self.enabled == false then
			if self.face:GetScale().x > 0.001 then 
				local newscale = Math:Lerp(self.face:GetScale().x,0,.1)
				self.face:SetScale(Vec3(newscale,newscale,newscale))
			end
		else
			if self.entity:Hidden() then return end
			local pickinfo=PickInfo()	
			local pos = self.entity:GetPosition(true)
			local targetpos = Transform:Point(self.directionVector, self.entity, nil)
			local result = self.entity.world:Pick(pos,targetpos,pickinfo,self.pickradius,true,Collision.Projectile)
			if result then
				--System:Print("collided with " ..tostring(pickinfo.entity:GetKeyValue("name")))
				self.enabled = false
				
				local enemy = self:FindScriptedParent(pickinfo.entity,"Hurt")
				if enemy then
					enemy.script:Hurt(self.damage, player)
				end
				if result then
					--TODO make a bullet pop animation
				else
					self.entity:SetPosition(targetpos)
				end
			else
				self.entity:SetPosition(targetpos)
			end
		end
	end
	
	if(Time:GetCurrent() > self.lastUpdateTime + self.repeatAABB) then
		self.lastUpdateTime = Time:GetCurrent()
		local selfpos = self.entity:GetPosition(true)
		local dv = Vec3(self.damageRange, self.damageRange, self.damageRange)
		local aabb = AABB(selfpos - self.damageRange, selfpos + self.damageRange)
		self.entity.world:ForEachEntityInAABBDo(aabb,"RangeDmg", self.entity)
	end
end

function Script:CastRay(other)
	local sprite = Sprite:Create()
	sprite:SetMaterial(player.script.image.electroRay)
	sprite:SetViewMode(6)
	local p0 = self.entity:GetPosition(true)
	local p1 = other:GetPosition(true)

	self.allignvector = p0 - p1
	sprite:SetPosition((p0+p1)/2)
	sprite:AlignToVector(self.allignvector:Normalize(),2)
	
	-- Find distance between the two points.
	local total_distance = p0:DistanceToPoint(p1)
	
	-- Modify the size of the beam based on the 2 points.
	-- Argument #1 is beamwidth, default 1
	sprite:SetSize(1, total_distance)
	sprite:SetScript("Scripts/Objects/Effects/fadeToBlack.lua", true)
end

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]