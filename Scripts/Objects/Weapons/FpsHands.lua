Script.offset=Vec3(0,0,0)--Vec3 "Offset"
Script.rotation=Vec3(0,0,0)--Vec3 "Rotation"
Script.mode = ""
Script.maxswayamplitude=0.01
Script.amplitude=0
Script.swayspeed=0
Script.timeunits=0
Script.smoothedposition=Vec3(0)
Script.smoothedrotation=Vec3(0)
Script.verticalbob=0
Script.jumpoffset=0
Script.landoffset=0
Script.startedFire = -1
Script.firstPunchTime = 400
Script.secondPunchTime = 600
Script.currentPunch = 0
Script.sprint = false
Script.speed = 0

function Script:Start()
	self:SetMode("idle")
	self.entity:SetPickMode(0)
end

function Script:BeginJump()
	self.jumpoffset = -180
end

function Script:BeginLand()
	self.landoffset = -180
end

function Script:Fire()
	if self.startedFire > -1 then return end
	if(self.sprint == false) then
		self.startedFire = Time:GetCurrent()
		self:SetMode("fire") 
	end
end

function Script:SetMode(pMode)
	if (self.mode ~= pMode) then
		if (pMode == "idle") then
			self.entity:PlayAnimation("Idle",0.02,500)
			--self.entity:PlayAnimation("Punch",0.05,100,1,"EndPunch")
		elseif (pMode == "fire") then
			self.entity:PlayAnimation("Punch",0.05,400,1,"EndPunch")
		elseif (pMode == "sprint") then
			self.entity:PlayAnimation("Run",0.03,600,0)
		else
			self.mode = "idle"
			System:Print(">> WRONG INPUT, PLEASE CHECK "..tostring(self.entity:GetKeyValue("name")))
		end
		self.mode = pMode
	end
end

function Script:EndPunch()
	self:SetMode("idle")
	self.startedFire = -1
end

function Script:UpdatePhysics()
	if (self.sprint and self.speed > 0.1) then
		self:SetMode("sprint")
		return
	elseif (self.sprint == false and self.mode == "sprint") then
		self:SetMode("idle")
	elseif (self.speed <= 0.1 and self.mode == "sprint") then 
		self:SetMode("idle")
	end
	
	local t = Time:GetCurrent()
	
	if (self.startedFire > 0) then
		local pickinfo=PickInfo()
		--throw punches picks
		--ignore player hitbox for this time
		self.player.hitbox:SetPickMode(0)
		local targetpos = Transform:Point(0, 0, 5, self.player.camera, nil)
		
		if (t - self.startedFire >= self.firstPunchTime and self.currentPunch == 0) then
			self.currentPunch = 1
			if (self.entity.world:Pick(self.player.camera:GetPosition(true), targetpos, pickinfo, 0, true)) then
				local enemy = self:FindScriptedParent(pickinfo.entity,"Hurt")
				if enemy then enemy.script:Hurt(self.player.leftPunchDamage) end
			end
		elseif (t - self.startedFire >= self.secondPunchTime and self.currentPunch == 1) then
			self.currentPunch = 0
			self.startedFire = -1
			if (self.entity.world:Pick(self.player.camera:GetPosition(true), targetpos, pickinfo, 0, true)) then
				local enemy = self:FindScriptedParent(pickinfo.entity,"Hurt")
				if enemy then enemy.script:Hurt(self.player.rightPunchDamage) end
			end
		end
		self.player.hitbox:SetPickMode(1)
	end
end

function Script:Draw()
	--Scripted Animation
	
	local t = Time:GetCurrent()
		local jumpbob = 0
	
	if self.jumpoffset<0 then
		jumpbob = (Math:Sin(self.jumpoffset))*0.01
		self.jumpoffset = self.jumpoffset + 8*Time:GetSpeed()
	end
	
	if self.landoffset<0 then
		jumpbob = jumpbob + (Math:Sin(self.landoffset))*0.01
		self.landoffset = self.landoffset + 10*Time:GetSpeed()
	end

	local bob = 0;
	self.speed = math.max(0.1,self.player.entity:GetVelocity():xz():Length())
	if self.player.entity:GetAirborne() then self.speed = 0.1 end
	self.swayspeed = Math:Curve(self.speed,self.swayspeed,20)
	self.swayspeed = math.max(0.5,self.swayspeed)
	self.amplitude = math.max(2,Math:Curve(self.speed,self.amplitude,20))
	self.timeunits = self.timeunits + self.swayspeed*4*Time:GetSpeed()
	local sway = math.sin(self.timeunits/120.0) * self.amplitude * self.maxswayamplitude
	bob = (1-math.cos(self.timeunits/60.0)) * self.maxswayamplitude * 0.1 * self.amplitude
	local campos = self.player.camera:GetPosition(true)
	
	self.smoothedposition.x = campos.x
	self.smoothedposition.y = Math:Curve(campos.y,self.smoothedposition.y,2)
	self.smoothedposition.z = campos.z
	self.entity:SetRotation(self.rotation)
	self.entity:SetPosition(sway*self.entity.scale.x,bob+jumpbob,0)
	self.entity:Translate(self.offset,false)
end

function Script:FindScriptedParent(entity,func)
	while entity~=nil do
		if entity.script then
			if type(entity.script[func])=="function" then
				return entity
			end
		end
		entity = entity:GetParent()
	end
	return nil
end

function Script:PostRender(context)
	if showstats then
		context:SetBlendMode(Blend.Alpha)
		context:SetColor(1,0,0,1)
		context:DrawText(self.mode,200,300)
		context:DrawText("Sprinting : "..tostring(self.sprint),200,350)
		context:DrawText("Speed : "..tostring(self.speed),200,400)
		context:SetColor(1,1,1,1)
		context:SetBlendMode(Blend.Solid)
	end
end

function Script:Release()
	--PUT HERE ANY SOUNDS / IMAGES ETC LOADED TO RELEASE
end