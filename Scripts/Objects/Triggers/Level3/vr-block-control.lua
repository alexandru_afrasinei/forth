function Script:Start()

	System:Print("vr-block-control:Start")
	System:Print("*** in VR level self.levelState is:"..gworld:getLevelState())
	self.levelState = gworld:getLevelState()

	-- Get state blocks
	self.state1 = self.entity:FindChild("CSG_1")
	self.state2 = self.entity:FindChild("CSG_2")
	self.state3 = self.entity:FindChild("CSG_3")

	if self.levelState == "1" then
		System:Print("self.levelState:"..tostring(self.levelState))
		self:ShowWithChildren(self.state1)
		self:HideWithChildren(self.state2)
		self:HideWithChildren(self.state3)
	elseif self.levelState == "2" then
		System:Print("self.levelState:"..tostring(self.levelState))
		self:ShowWithChildren(self.state1)			
		self:ShowWithChildren(self.state2)
		self:HideWithChildren(self.state3)
	
	elseif self.levelState == "3" then
		System:Print("self.levelState:"..tostring(self.levelState))
		self:ShowWithChildren(self.state1)
		self:ShowWithChildren(self.state2)
		self:ShowWithChildren(self.state3)
	end

	--System:Print("in VR level self.levelState is:"..gworld:getLevelState())

	

end


function Script:ShowWithChildren(anEntity)

	local childCount = anEntity:CountChildren()
	if childCount == 0 then anEntity:Show() return end

	for i=0, anEntity:CountChildren()-1 do
		anEntity:GetChild(i):Show()
	end
	anEntity:Show()
end

function Script:HideWithChildren(anEntity)

	local childCount = anEntity:CountChildren()
	if childCount == 0 then anEntity:Hide() return end

	for i=0, anEntity:CountChildren()-1 do
		anEntity:GetChild(i):Hide()
	end
	anEntity:Hide()
end

--[[
function Script:UpdateWorld()
	
end
]]

--[[
function Script:UpdatePhysics()
	
end
]]

--[[
--This can be used to select which objects an entity collides with.  This overrides collision types completely.
function Script:Overlap(e)
	return Collision:Collide
end
]]

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]

--[[
function Script:Draw()
	
end
]]

--[[
function Script:DrawEach(camera)
	
end
]]

--[[
--This function will be called after the world is rendered, before the screen is refreshed.
--Use this to perform any 2D drawing you want the entity to display.
function Script:PostRender(context)
	
end
]]

--[[
--This function will be called when the entity is deleted.
function Script:Detach()
	
end
]]

--[[
--This function will be called when the last instance of this script is deleted.
function Script:Cleanup()
	
end
]]

--[[
--This function will be called upon completion of a one-shot animation played with the PlayAnimation() command.
function Script:EndAnimation(sequence)
	
end
]]