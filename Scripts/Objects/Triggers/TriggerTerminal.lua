--[[
 Terminal script
]]--

Script.target=""--string "Target"


function Script:Start()
	self.enabled=true
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()

		local ename = self.entity:GetKeyValue("name")

		guimanager:loadDialogData(TerminalDialog)
        guimanager:showDialog()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self:CallOutputs("Enable")
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self:CallOutputs("Disable")
	end
end