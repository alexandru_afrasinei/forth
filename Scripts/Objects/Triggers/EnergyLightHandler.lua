Script.counter = 0
Script.lighte = {}
Script.player = {}
Script.once = true

function Script:Start()
    self.lighte = world:FindEntity("energylight")
    self.lighte:Hide()
    self.player = world:FindEntity("simplefpsplayer")

    if librarianEnd == true then
        world:FindEntity("Spot Light 3").script.enable = true
        world:FindEntity("Spot Light 4").script.enable = true
        world:FindEntity("Spot Light 5").script.enable = true
        world:FindEntity("Spot Light 6").script.enable = true

        world:FindEntity("Point Light 1").script.enable = true
        world:FindEntity("Point Light 2").script.enable = true
        world:FindEntity("Point Light 3").script.enable = true

        world:FindEntity("End").script.enabled = true
        self.player.script.enableShake = true
    end
end

function Script:UpdateWorld()
    -- librarianEnd = true is end of computer simulation level
    if librarianEnd == true then
        if self.once == true then
            self.lighte:Show()
            self.once = false
        end

        if self.lighte:GetPosition(true).z < 9 then
            self.lighte:SetPosition(self.lighte:GetPosition(true).x, self.lighte:GetPosition(true).y, self.lighte:GetPosition(true).z + self.counter )
            self.counter = self.counter + Time:GetSpeed()*0.00005
        else
            self.player.script.enableShake = false
            guimanager:hideDialog()
            changemapname = "GameMaps/level04/level"
            gstate:set(LEVEL4_STATE)
            changenextstate = LEVEL4_STATE
            guimanager:ShowStartLevel()
            librarianEnd = false;
        end
    end
end