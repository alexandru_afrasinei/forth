--[[
 Level Mutation
]]--

Script.target=""--string "Target"


function Script:Start()
	self.enabled=true
	self.justonce=true
end

function Script:HideActivatedEmitters()
	if gstate.activatedTeleportIndex == 1 then
		world:FindEntity("NorthGate_sparkles"):Hide()
		world:FindEntity("NorthGate_activation"):Hide()
	elseif gstate.activatedTeleportIndex == 2 then
		world:FindEntity("SouthGate_sparkles"):Hide()
		world:FindEntity("SouthGate_activation"):Hide()
	elseif gstate.activatedTeleportIndex == 3 then
		world:FindEntity("WestGate_sparkles"):Hide()
		world:FindEntity("WestGate_activation"):Hide()
	elseif gstate.activatedTeleportIndex == 4 then
		world:FindEntity("EastGate_sparkles"):Hide()
		world:FindEntity("EastGate_activation"):Hide()
	elseif gstate.activatedTeleportIndex == 5 then
		world:FindEntity("UpGate_sparkles"):Hide()
		world:FindEntity("UpGate_activation"):Hide()
	elseif gstate.activatedTeleportIndex == 6 then
		world:FindEntity("DownGate_sparkles"):Hide()
		world:FindEntity("DownGate_activation"):Hide()
	end
end

function Script:Collision(entity, position, normal, speed)
	if justonce then
		if self.target == "EastGate" then
			ents = {"WestGate", "NorthGate", "SouthGate", "UpGate", "DownGate", "Monolith", "Pistol", "FlashLight"}

			if activateCommandExecuted == true then
				if eastInput % 2 == 0 then
					entity:SetPosition(9, 0, 0, true)
					entity.script.camRotation.x = 0
					entity.script.camRotation.y = -90
					entity.script.camRotation.z = 0
					eastInput = eastInput + 1

					for i = 1,8 do
						world:FindEntity(ents[i]):Hide()
					end
					world:FindEntity("PedestalBook"):Show()
				else
					entity:SetPosition(9, 0, 0, true)
					entity.script.camRotation.x = 0
					entity.script.camRotation.y = -90
					entity.script.camRotation.z = 0
					eastInput = eastInput + 1

					for i = 1,8 do
						world:FindEntity(ents[i]):Show()
					end
				end

			else
				self:HideActivatedEmitters()

				entity:SetPosition(-9, 0, 0, true)
				entity.script.camRotation.x = 0
				entity.script.camRotation.y = 90
				entity.script.camRotation.z = 0
				eastInput = eastInput + 1
			end

		elseif self.target == "WestGate" then
			self:HideActivatedEmitters()

			entity:SetPosition(9, 0, 0, true)
			entity.script.camRotation.x = 0
			entity.script.camRotation.y = -90
			entity.script.camRotation.z = 0
			westInput = westInput + 1

		elseif self.target == "NorthGate" then
			self:HideActivatedEmitters()

			entity:SetPosition(0, 0, -9, true)
			entity.script.camRotation.x = 0
			entity.script.camRotation.y = 0
			entity.script.camRotation.z = 0
			northInput = northInput + 1

		elseif self.target == "SouthGate" then
			self:HideActivatedEmitters()

			entity:SetPosition(0, 0, 9, true)
			entity.script.camRotation.x = 0
			entity.script.camRotation.y = 180
			entity.script.camRotation.z = 0
			southInput = southInput + 1

		elseif self.target == "UpGate" then
			self:HideActivatedEmitters()

			entity:SetPosition(-8.5, 0, 6, true)
			entity.script.camRotation.x = 0
			entity.script.camRotation.y = 0
			entity.script.camRotation.z = 0
			upInput = upInput + 1

		elseif self.target == "DownGate" then
			self:HideActivatedEmitters()

			entity:SetPosition(8.5, 0, 6, true)
			entity.script.camRotation.x = 0
			entity.script.camRotation.y = 0
			entity.script.camRotation.z = 0
			downInput = downInput + 1
		end

		justonce = false
	end

--ent = world:FindEntity("simplefpsplayer")
--System:Print("x: " .. ent.script.camRotation.x .. " y: " .. ent.script.camRotation.y .. " z: " .. ent.script.camRotation.z)
end

function Script:Use()
	if self.enabled then
		justonce = true
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()


		local ename = self.entity:GetKeyValue("name")

		if ename == "NorthGate" then
			Databank_Input_Description_l1 = "NORTH  DATABANK  " .. ">>>  INPUT  " .. northInput .."\n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("NorthGate_sparkles"):Show()
			world:FindEntity("NorthGate_activation"):Show()
			gstate.activatedTeleportIndex = 1
		elseif ename == "SouthGate" then
			Databank_Input_Description_l1 = "SOUTH  DATABANK  " .. ">>>  INPUT  " .. southInput .."\n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("SouthGate_sparkles"):Show()
			world:FindEntity("SouthGate_activation"):Show()
			gstate.activatedTeleportIndex = 2
		elseif ename == "WestGate" then
			Databank_Input_Description_l1 =  "WEST DATABANK  " .. ">>>  INPUT  " .. westInput .." \n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("WestGate_sparkles"):Show()
			world:FindEntity("WestGate_activation"):Show()
			gstate.activatedTeleportIndex = 3
		elseif ename == "EastGate" then
			Databank_Input_Description_l1 =  "EAST DATABANK  " .. ">>>  INPUT  " .. eastInput .." \n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("EastGate_sparkles"):Show()
			world:FindEntity("EastGate_activation"):Show()
			gstate.activatedTeleportIndex = 4
		elseif ename == "UpGate" then
			Databank_Input_Description_l1 =  "UP DATABANK  " .. ">>>  INPUT  " .. upInput .." \n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("UpGate_sparkles"):Show()
			world:FindEntity("UpGate_activation"):Show()
			gstate.activatedTeleportIndex = 5
		elseif ename == "DownGate" then
			Databank_Input_Description_l1 =  "DOWN DATABANK  " .. ">>>  INPUT  " .. downInput .." \n\n" .. Gate_Description_l1 ..  "\n\n"
			world:FindEntity("DownGate_sparkles"):Show()
			world:FindEntity("DownGate_activation"):Show()
			gstate.activatedTeleportIndex = 6
		end
		guimanager:loadReaderData(Databank_Input_Description_l1)

		guimanager:showReader()
		gui:GetBase():Redraw()
		System:Print("Test")
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self:CallOutputs("Enable")
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self:CallOutputs("Disable")
	end
end