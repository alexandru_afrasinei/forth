Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"
Script.changemapname="GameMaps/level03temp/level"--string "Map"
Script.levelState=0--choice "Position" "0,1,2,3"
Script.setVRLevel = false -- bool "Set State"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
	System:Print("in VR level self.levelState is:"..tostring(self.levelState))
end

function Script:Use()
	if self.enabled then

		if self.sound then self.entity:EmitSound(self.sound) end
		System:Print("ABOUT TO SET LEVEL STATE"..tostring(self.levelState))
		if self.setVRLevel == true then
			gworld:setLevelState(self.levelState)
		end

		changemapname = self.changemapname
		gstate:set(LEVEL3_STATE)
		changenextstate = LEVEL3_STATE
		--guimanager:ShowStartLevel()

		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end