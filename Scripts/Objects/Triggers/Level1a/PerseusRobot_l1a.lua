Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()

		talkall3[3] = true

		guimanager:loadReaderData(Perseus_talk_l1a[perseus_talk_index])
		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end