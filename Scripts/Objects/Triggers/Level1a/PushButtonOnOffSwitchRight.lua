--public
Script.enabled=true--bool "Enabled"
Script.locked=false--bool "Locked"
Script.soundfileLock=""--path "Sound Lock" "Wav Files (*.wav):wav"
Script.soundfileUnlock=""--path "Sound Unlock" "Wav Files (*.wav):wav"
Script.soundfileLocked=""--path "Sound Locked" "Wav Files (*.wav):wav"
Script.soundfileUnlocked=""--path "Sound Unlocked" "Wav Files (*.wav):wav"
Script.soundfileOn=""--path "Sound On" "Wav Files (*.wav):wav"
Script.soundfileOff=""--path "Sound Off" "Wav Files (*.wav):wav"
Script.pauseDuration=1000--int "Pause Lock Duration"
Script.pauseTime=nil
Script.on = false--bool "Begin On"
Script.playAudio = true--bool "Play Audio"
--private
Script.onbutton=nil
Script.offbutton=nil
Script.listening=true

function Script:Start()
	self.sound={}

	if self.soundfileLock ~= nil then
		self.sound.lock = Sound:Load(self.soundfileLock)
	end
	if self.soundfileUnlock ~= nil then
		self.sound.Unlock = Sound:Load(self.soundfileUnlock)
	end
	if self.soundfileLocked ~= nil then
		self.sound.locked = Sound:Load(self.soundfileLocked)
	end
	if self.soundfileUnlocked ~= nil then
		self.sound.Unlocked = Sound:Load(self.soundfileUnlocked)
	end
	if self.soundfileOn ~= nil then
		self.sound.on = Sound:Load(self.soundfileOn)
	end
	if self.soundfileOff ~= nil then
		self.sound.off = Sound:Load(self.soundfileOff)
	end
	
	-- get child entities
	self.onbutton = self.entity:FindChild("switch_on")
	self.offbutton = self.entity:FindChild("switch_off")	

	if self.locked then
		self:Lock()
	else
		self:Unlock()
	end


	self.listening = true
	if self.on then
		self.onbutton:Show()
		self.offbutton:Hide()
	else
		self.onbutton:Hide()
		self.offbutton:Show()
	end
end

function Script:Use()
	if self.locked then
		if self.sound.locked and self.playAudio then self.entity:EmitSound(self.sound.locked) end
		return
	end
	if self.enabled then
		self.component:CallOutputs("Use")
		self:Toggle()
	end
end

function Script:Toggle()--in
	if self.enabled then
		self.on = not self.on
		if self.on then
			self:On()
		else
			self:Off()
		end		
	end
end

function Script:ToggleLock()--in
	if self.enabled then
		if self.locked then
			self:Unlock()
		else
			self:Lock()
		end		
	end
end


function Script:Lock()--in
	if self.enabled and self.listening then
		System:Print("Locked")
		self:Pause()
		self.locked = true
		if self.sound.lock and self.playAudio then self.entity:EmitSound(self.sound.lock) end
		self.component:CallOutputs("Locked")
	end
end

function Script:Unlock()--in
	if self.enabled and self.listening then
		System:Print("Unlocked")
		self:Pause()
		self.locked = false
		if self.sound.Unlock and self.playAudio then self.entity:EmitSound(self.sound.Unlock) end
		self.component:CallOutputs("Unlocked")
	end
end

function Script:On()--in
	if self.sound.locked and self.playAudio and self.locked then self.entity:EmitSound(self.sound.locked) end
	if self.enabled and self.listening and self.locked == false then
		System:Print("On")
		self:Pause()
		self.onbutton:Show()			
		self.offbutton:Hide()
		if self.sound.on and self.playAudio then self.entity:EmitSound(self.sound.on) end

		world:FindEntity("DestinationTeleporter3_bars"):Show()
		world:FindEntity("DestinationTeleporter3_sparkles"):Show()

		world:FindEntity("SourceTeleporter4_bars"):Show()
		world:FindEntity("SourceTeleporter4_sparkles"):Show()

		self.component:CallOutputs("On")
	end
end

function Script:Off()--in
	if self.sound.locked and self.playAudio and self.locked then self.entity:EmitSound(self.sound.locked) end
	if self.enabled and self.listening and self.locked == false then
		System:Print("Off")
		self:Pause()
		self.onbutton:Hide()	
		self.offbutton:Show()			
		if self.sound.off and self.playAudio then self.entity:EmitSound(self.sound.off) end

		world:FindEntity("DestinationTeleporter3_bars"):Hide()
		world:FindEntity("DestinationTeleporter3_sparkles"):Hide()

		world:FindEntity("SourceTeleporter4_bars"):Hide()
		world:FindEntity("SourceTeleporter4_sparkles"):Hide()

		self.component:CallOutputs("Off")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	--if self.sound then self.sound:Release() end
end

	--local t = Time:GetCurrent()
	--if t > self.pauseTime + self.pauseDuration

function Script:Pause()
	System:Print("Button:Pause")
	self.listening = false
	self.pauseTime = Time:GetCurrent()
end

function Script:UpdateWorld()
	local currenttime = Time:GetCurrent()
	if self.pauseTime ~= nil and currenttime > self.pauseTime + self.pauseDuration then
		System:Print("Button:PauseComplete")
		self.listening = true
		self.pauseTime = nil
	end
end