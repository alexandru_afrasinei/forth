Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()

		entname = tostring(self.entity:GetKeyValue("name"))
		if entname == "Monolith0" then
			l1a_monolith_talk_status = 1
			guimanager:loadReaderData(Monolith_Discussion_l1a[1])
		elseif entname == "Monolith1" then
			guimanager:loadReaderData(Monolith_Discussion_l1a[2])
			l1aNorthInput1 = "0" l1aSouthInput1 = "0"
		elseif entname == "Monolith2" then
			guimanager:loadReaderData(Monolith_Discussion_l1a[3])
			l1aWestInput1 = "0" l1aEastInput1 = "0" 
		elseif entname == "Monolith3" then
			guimanager:loadReaderData(Monolith_Discussion_l1a[4])
			l1aUpInput1 = "0" l1aDownInput1 = "0"
		end

		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end