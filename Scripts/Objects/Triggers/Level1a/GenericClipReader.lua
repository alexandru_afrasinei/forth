Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()
		
		entname = tostring(self.entity:GetKeyValue("name"))
		if entname == "RPLClip" then
			guimanager:loadReaderData(RPLClipText_l1a)
			l1aNorthInput0 = 1
		elseif entname == "RPRClip" then
			guimanager:loadReaderData(RPRClipText_l1a)
			l1aSouthInput0 = 0
		elseif entname == "BPLClip" then
			guimanager:loadReaderData(BPLClipText_l1a)
			l1aWestInput0 = 0
		elseif entname == "BPRClip" then
			guimanager:loadReaderData(BPRClipText_l1a)
			l1aEastInput0 = 0
		elseif entname == "LPLClip" then
			guimanager:loadReaderData(LPLClipText_l1a)
			l1aUpInput0 = 2
		elseif entname == "LPRClip" then
			guimanager:loadReaderData(LPRClipText_l1a)
			l1aNorthInput0 = 1
		elseif entname == "clipboard1" then
			guimanager:loadReaderData(ClipboardText_l1a)
			l1aSouthInput2 = 1
		elseif entname == "clipboard2" then
			guimanager:loadReaderData(ClipboardText_l1a)
			l1aWestInput2 = 1
		elseif entname == "clipboard3" then
			guimanager:loadReaderData(ClipboardText_l1a)
			l1aEastInput2 = 1
		elseif entname == "clipboard4" then
			guimanager:loadReaderData(ClipboardText_l1a)
			l1aDownInput2 = 1
		elseif entname == "clipboard5" then
			guimanager:loadReaderData(ClipboardText_l1a)
			l1aUpInput2 = 1
		end
		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end