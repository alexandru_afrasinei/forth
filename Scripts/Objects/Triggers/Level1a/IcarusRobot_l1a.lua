Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()

		world:FindEntity("trigger_respawn").script.pivot = world:FindEntity("respawn4")

		if bellerophon_talk_index == 2 and genstatus == 0 then
			icarus_talk_index = 2
			bellerophon_talk_index = 5
			cadmus_talk_index = 4
		end

		if bellerophon_talk_index == 3 and genstatus == 1 then
			icarus_talk_index = 4
			bellerophon_talk_index = 5
			cadmus_talk_index = 4
		end

		guimanager:loadReaderData(Icarus_talk_l1a[icarus_talk_index])
		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end