--[[
This script will make any entity act as a collision trigger.  If it collides with the player
the player will be set to the referenced pivot position below.
]]--
Script.pivot = nil --entity "Pivot to tp to"
Script.soundPath = nil --path "Sound At Coll"

function Script:Start()
	self.enabled = true
	---------------------------------------------------------------------------
	--We want the trigger model visible in the editor, but invisible in the game
	--We can achieve this by creating a material, setting the blend mode to make
	--it invisible, and applying it to the model.
	---------------------------------------------------------------------------
	local material = Material:Create()
	material:SetBlendMode(5)--Blend.Invisible
	self.entity:SetMaterial(material)
	material:Release()
	self.entity:SetShadowMode(0)
	if(self.soundPath) then
		self.sound = Sound:Load(self.soundPath)
	end

	if bellerophon_talk_index == 0 then
		self.pivot = world:FindEntity("respawn1")
	end
end

function Script:Collision(entity, position, normal, speed)
	if self.enabled then
		if(entity:GetKeyValue("type") == "player")then
			if(self.sound) then self.sound:Play() end
			if(self.pivot) then 
				entity:SetVelocity(0,0,0)
				entity:SetPosition(self.pivot:GetPosition(true))
			end
		end
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
	end
end