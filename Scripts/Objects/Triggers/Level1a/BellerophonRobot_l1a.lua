Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()

		talkall3[1] = true

		if bellerophon_talk_index == 0 then
			bellerophon_talk_index = 1
			world:FindEntity("trigger_respawn").script.pivot = world:FindEntity("respawn3")
			world:FindEntity("LastPyramidRoad"):Show()
		end

		if bellerophon_talk_index == 1 then
			if is_all_contacted_l1a() then
					bellerophon_talk_index = 2
					reset_contacted_l1a()

					world:FindEntity("ButtonTeleportSource1").script:Unlock()
					world:FindEntity("ButtonTeleportDestination1").script:Unlock()

					world:FindEntity("DestinationTeleporter1_bars"):Show()
					world:FindEntity("DestinationTeleporter1_sparkles"):Show()
					
					world:FindEntity("SourceTeleporter2_bars"):Show()
					world:FindEntity("SourceTeleporter2_sparkles"):Show()

					guimanager:TriggerTimer(250)
			else
				cadmus_talk_index = 2
				perseus_talk_index = 2
			end
		end

		guimanager:loadReaderData(Bellerophon_talk_l1a[bellerophon_talk_index])
		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end