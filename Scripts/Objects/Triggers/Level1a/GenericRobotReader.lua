Script.enabled=true--bool "Enabled"
Script.soundfile=""--path "Sound" "Wav Files (*.wav):wav"

function Script:Start()
	if self.soundfile then
		self.sound = Sound:Load(self.soundfile)
	end
end

function Script:Use()
	if self.enabled then
		if self.sound then self.entity:EmitSound(self.sound) end
		gui:GetBase():SetImage(nil)
		gui:GetBase():Redraw()
		Time:Pause()
		guimanager:getMainMenu():ShowInGame()
		context:GetWindow():ShowMouse()
		
		entname = tostring(self.entity:GetKeyValue("name"))
		if entname == "FirstRobotSelector" then
			guimanager:loadReaderData(First_l1a)
		elseif entname == "WatcherRobotSelector" then
			guimanager:loadReaderData(Watcher_l1a)
		elseif entname == "ThirdRobotSelector" then
			guimanager:loadReaderData(Third_l1a)
		elseif entname == "FourthRobotSelector" then
			guimanager:loadReaderData(Fourth_l1a)
		elseif entname == "FifthRobotSelector" then
			guimanager:loadReaderData(Fifth_l1a)
		elseif entname == "SixthRobotSelector" then
			guimanager:loadReaderData(Sixth_l1a)
		elseif entname == "SeventhRobotSelector" then
			if world:FindEntity("switch_pillars").script:IsOn() == true then
				guimanager:loadReaderData(Seventh_ok_l1a)
			else
				guimanager:loadReaderData(Seventh_nok_l1a)
			end
		elseif entname == "EigthRobotSelector" then
			world:FindEntity("trigger_respawn").script.pivot = world:FindEntity("respawn2")
			guimanager:loadReaderData(Eigth_l1a)
		elseif entname == "NinthRobotSelector" then
			guimanager:loadReaderData(Nineth_l1a)
		elseif entname == "TenthRobotSelector" then
			guimanager:loadReaderData(Tenth_l1a)
		elseif entname == "EleventhRobotSelector" then
			guimanager:loadReaderData(Eleventh_l1a)
		end
		guimanager:showReader()

		gui:GetBase():Redraw()
		self.component:CallOutputs("Use")
	end
end

function Script:Enable()--in
	if self.enabled==false then
		self.enabled=true
		self.component:CallOutputs("Enable")
		self.health=1
	end
end

function Script:Disable()--in
	if self.enabled then
		self.enabled=false
		self.component:CallOutputs("Disable")
		self.health=0
	end
end

function Script:Release()
	if self.sound then self.sound:Release() end
end