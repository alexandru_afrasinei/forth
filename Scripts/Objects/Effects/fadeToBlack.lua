Script.starttime = 0
Script.lifetime = 1000
Script.lastUpdateTime = 0

function Script:Start() 
	self.starttime = Time:GetCurrent()
	self.material = self.entity:GetMaterial()
 end

function Script:UpdatePhysics()
	local t = Time:GetCurrent()
	if t-self.starttime > self.lifetime then self.entity:Release() end

	if(t > self.lastUpdateTime + 300) then
		local color = self.entity:GetColor()
		self.entity:SetColor(color.r*0.3, color.g*0.3, color.b*0.3)	
		self.lastUpdateTime = t
	end
end