
function FindEntity(entityName)
    local x
    local world = World:GetCurrent()
    
    for x = 0, world:CountEntities() - 1 do
        local entity = world:GetEntity(x)
 
        if entity:GetKeyValue("name") == entityName then
            return entity
        end
    end
    
    return nil
end