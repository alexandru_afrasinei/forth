Serializer = {serpent = {}}

function Serializer:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   local lserpent = require("Scripts.Utils.serpent") -- if this crashes on you, disable lua sandboxing from editor
   self.serpent = lserpent
   return o
end

function Serializer:save(data , filename)
    local stream = FileSystem:WriteFile(filename)
	stream:WriteLine(self.serpent.dump(data))
	stream:Release() 
end

function Serializer:getSerpent()
    return self.serpent
end