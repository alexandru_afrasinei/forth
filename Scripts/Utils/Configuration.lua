import "Scripts/Utils/Serializer.lua"


Configuration = {serializer = {}, config = {}, fname = ""}

function Configuration:new (o, fname)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   self.serializer = Serializer:new(nil)
   self.config = {}
   self.fname = fname
   return o
end

-- I changed this line above to not make it crash
function Configuration:save()
	self.serializer:save(self.config, self.fname)
end

function Configuration:load()
	local stream = FileSystem:ReadFile(self.fname)
	local data = ""
	if (stream) then
		data = stream:ReadLine(); 
		stream:Release() 
	end
	local fun, err = loadstring(data)
	if err then error(err) end
	self.config = fun()
end

function Configuration:getConfig()
	return self.config
end

function Configuration:getSerializer()
	return self.serializer
end