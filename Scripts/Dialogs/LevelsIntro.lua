Intro_l1 = "What is this place .. \n Am i dreaming ? As dizziness starts to fade you start looking around\n"

Intro_l1a = "Blackout then light such is the wheel of life\n\n Same place but different"

Intro_l2 = "You manage to exit the pod \n, fall on the ground, room swirling around.\n Vomiting, headache and confusion Who am i ? Where am i ?\n \n"

Intro_l3 = "You enter the computer simulation,\n this feel familiar youve been here before.\n\n The monolith in the middle of room is the computer avatar\nSeveral teleporter pads are on the ground\nThe terminal on wall is awaiting commands\n\n Go the the monolith and press E"

Intro_l4 = "The cryo room was destroyed by the energy field,\n a loud noise of crashing metal and rock ,\n you barely escaped through the hatch , the way to the cryo room is closed now\n\n Its dark but this seem to be a cave system \n You begin to feel claustrophobic"
