l1aNorthInput0 = "?" l1aSouthInput0 = "?" l1aWestInput0 = "?" l1aEastInput0 = "?" l1aUpInput0 = "?" l1aDownInput0 = "?"
l1aNorthInput1 = "?" l1aSouthInput1 = "?" l1aWestInput1 = "?" l1aEastInput1 = "?" l1aUpInput1 = "?" l1aDownInput1 = "?"
l1aNorthInput2 = "?" l1aSouthInput2 = "?" l1aWestInput2 = "?" l1aEastInput2 = "?" l1aUpInput2 = "?" l1aDownInput2 = "?"

RPLClipText_l1a = "Restricted area\n Request access to the current trinity\n\n 'This pyramid was constructed by Bellerophon the First'.\n\nLooking below you find some human writing ... \nA number and a letter is scribbled:  N 1.\n\nYou noticed something changed"
RPRClipText_l1a = "The old text is mostly not readable, only one word , island.\n\n Another more recent writing below\n A number and a letter is scribbled:  S 0."

BPLClipText_l1a = "Nothing readable , the scribble is here also: W 0\n\nAnd something changed"
BPRClipText_l1a = "Ancient text mostly not readable: 'Constructed ... Cadmus the Fir..'.\nAnd another scribble:  E 0. Change ..."

LPRClipText_l1a = "Ancient text: 'compl... imortality ... trinity ... Perse ...'.\nAnd another scribble:  U 2.\n\nSomething changed"
LPLClipText_l1a = "Some gibberish 'island ... spheres ...' , scribble: D 1\n\n What is the meaning of this ? "

ClipboardText_l1a = "The more things change the more they stay the same" 

Watcher_l1a = "Behaviour analysis in progress ...\nWaking up Bellerophon, Cadmus, Perseus\n\nMatching pillars online"
First_l1a = "Initial analysis complete , a human ... \n Surveillance mode active \n\n Watching"
Third_l1a = "I am Sisif, telemetry gathering nominal"
Fourth_l1a = "Realm insertion detected! Class 5 AI. \n Countermeasures deployed ..."
Fifth_l1a = "Rubicon reporting in, the die is cast ...\n The tower looms in the distance"
Sixth_l1a = "Daedalus here, cannot talk much energy depleting ... \nIcarus need to have a talk with you , hes on the right of the generator."
Seventh_nok_l1a = "Joker dzzt rzzt vzzt ...  overload overload ... \n Unauthorized teleport modification, security breach, reverting change  \n When ? Where ? Why ? what happend ..."
Seventh_ok_l1a = "Good, ship AI here, modifications succeded Joker wont notice what happend\nYou will be teleported closer to the pyramids\n\nTalk to my shards , and remember"
Eigth_l1a = "Welcome, i am Cerberus, guardian of the tower, pyramids warden, grand protector ...\nand very fond of humans, unlike others here\n\nGo forth! Bellerophon awaits you"
Nineth_l1a = "I dont trust you human ... you have something to do with the realm insertion\nUnfortunatly i dont have any evidence\n\n I will let you pass to the upper level"
Tenth_l1a = "Countermeasure program active, taking over ... \nYou shall not pass!!!"
Eleventh_l1a = "Ship AI reporting in, this will be the last robot hack, its becoming too dangerous \n Road ahead is blocked, they have countermeasures all over the place \nI need that generator up to access other systems and hide\n\n That tower on left may hold the key."
EleventhNoHack_l1a = "No, what happend ... , clock difference suggest i was disabled\n They locked the place up again\n \n The robot ignores you"

Monolith_Discussion_l1a = {
    "Ship AI here , i entered this realm with great difficulty \n Just a shard , there are 3 more like me , all with partial information for you\n\nI tampered with the Joker robot , activate the switch near him\n and will get you closer to the pyramids.\n\nThis is vital for your survival , talk to my shards!\n The realm has allready activated countermesures and i dont know how much i will hold.",
    "Good to see you, remember this information \n Remember ... \n\nN 0 S 0",
    "Farewell, i will be stuck in this realm\nThis is part of a command \n\nE 0 W 0",
    "Another shard in the wall,\n\n U 0 D 0"
}

icarus_talk_index = 1
Icarus_talk_l1a =
{
    "My name is Icarus, part of the upper trinity once, but i have fallen ...\n I will have my revenge on the usurper Bellerophon\n\n I cannot trust you , i will let your actions speak, stay out of my way for now.",
        "You did the right thing not activating that generator,\n i thank you. \n\n Now i can challenge Bellerophon and take my rigthfull place in the trinity\n\nThe writings on the pyramids was made by a previous human\n Its a command that will help you, i cant tell you more",
        "No no no ... , you reactivated the damn generator, need to hide, my efforts were in vain ... i will remember this affront human",
    "I anticipated this, Bellerophon is really disperate.\nNot this time\n\nIcarus simply dissapears into thin aer."
}

atalanta_talk_index = 1
Atalanta_talk_l1a =
{
    "Hey, im Atalanta.\n Good to have a human around\nI see they send you with bussines.\n\n Allright ill modify you ..., you have some good potential here \n, Kinetic and electricity modifiers grafted",
    "Farewell human\nLots of things to fix around this realm."
}

bellerophon_talk_index = 0
Bellerophon_talk_l1a = 
{
    "Greetings, i am Bellerophon the one in this trinity,\n the others are my brothers Perseus and Cadmus\n I have followed your course and know why you are here\n.We will tell you but first we require some service.\n\n The pesky Icarus is again staging a rebbelion to split this trinity\nTalk to my brothers and tell them to activate the teleporter near me\n\nMake haste and come back to me after is done",
    "Good, teleporters usage is authorized \n Our energy generator was sabotaged.\n\n Pass through the teleporter and restart the generator for us all.\n ",
        "Great work human, the generator is back online\n\nIcarus must be eliminated\nI need you to be a harbinger of destruction. \n Do this and i will give the information you need\n\n A shimering ligth enveloped you\n Cadmus has some upgrades for you\n\n Make your way to Icarus, good luck you will need it.",
            "Unfortunate, he escaped ... , ill have to deal with him later.  Our systens are active , you are not needed anymore human",
        "I see, you sided with the enemy or you are really incompetent.\n Either way, leave this realm.\n Bellerophon ignores you"
}

cadmus_talk_index = 1
Cadmus_talk_l1a = 
{
    "Cadmus the second in the trinity, our common link doesnt work\n Be carefull around human.",
    "What do you want ? \n I see, Icarus causing trouble again ...\nI activated my part of the teleport \n\n Perseus is up there in the distance.Send my regards to him.\n\n Farewell",  
    "Good work , the trinity is fully working now, common link established.\nFor your help i open the way to our realm engineer.\n\n She will make some improvments, i want you to be prepared human.",
    "You were blacklisted human, im am sorry cant do anything about it\nI let the teleports open for you\nExplore this place and try to remember\n\n The writings on the pyramids was made by a previous human\n Its a command that will help you, i cant tell you more\n\n Farewell"
}

perseus_talk_index = 1
Perseus_talk_l1a =
{
    "Im am Perseus, this power outage is really disrupting my work.The link must be restored.",
    "Teleport is now ready\n Bring back power to our realm so the trinity can communicate again as one\n\n The pyramids will be active once more\n Remember this place when you wake up , we will meet again human.",
    "Finally, i can continue my experiments , i thank you."
}

talkall3 = {false , false , false}

function is_all_contacted_l1a()
    ret = true
    for i = 1, #talkall3 do
        if talkall3[i] == false then
            ret = false
        end
    end

    return ret
end

function reset_contacted_l1a()
    for i = 1, #talkall3 do
        talkall3[i] = false
    end
end

genstatus = 0
tele1status = 0
tele2status = 0
allclipsreadstatus = 0
l1a_monolith_talk_status = 0