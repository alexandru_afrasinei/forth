--TerminalDialog handlers
function UpdateTerminalDialogData()
    local cmdinputs = "\n N " .. mNorthInput .. "\n S " .. mSouthInput .. "\n E " .. mEastInput .. "\n W " .. mWestInput .. "\n U " .. mUpInput .. "\n D " .. mDownInput
    TerminalDialog["1111"].text = "Command:\n" .. cmdinputs

    TerminalDialog["2111"].text = "Current databanks content:\n\n N " .. northInput .. "\n S " .. southInput .. "\n E " .. eastInput .. "\n W " .. westInput .. "\n U " .. upInput .. "\n D " .. downInput

    local inputs = "Data pushed: \n\n N " .. northInput .. "\n S " .. southInput .. "\n E " .. eastInput .. "\n W " .. westInput .. "\n U " .. upInput .. "\n D " .. downInput
    if northInput == 0 and southInput == 0 and westInput == 0 and eastInput == 0 and upInput == 0 and downInput == 0 then
        TerminalDialog["4111"].text = inputs .. "\n\nDefault data pushed. Use the gates to imput proper data for forming commands and execute" 
    else 
        TerminalDialog["4111"].text = inputs
    end

--activate monolith
    if mNorthInput == "1" and mSouthInput == "1" and mWestInput == "0" and mEastInput == "0" and mUpInput == "0" and mDownInput == "0" then
        TerminalDialog["6111"].text = "Activate command executed. Some mechanisms are activated Monolith is online now."
        return true
    end

--restart monolith
    if mNorthInput == "1" and mSouthInput == "1" and mWestInput == "1" and mEastInput == "1" and mUpInput == "1" and mDownInput == "1" then
        TerminalDialog["6111"].text = "Restart command executed. Monolith is back online."
        return true
    end

--exit vr
    if mNorthInput == "0" and mSouthInput == "0" and mWestInput == "0" and mEastInput == "0" and mUpInput == "2" and mDownInput == "0" then
        TerminalDialog["6111"].text = "Exit VR command executed."
        return true
    end

--activate healing station
    if mNorthInput == "1" and mSouthInput == "0" and mWestInput == "0" and mEastInput == "0" and mUpInput == "2" and mDownInput == "1" then
        TerminalDialog["6111"].text = "Healing station activate in cryo room."
        return true
    end

--NOP command
    if mNorthInput == "0" and mSouthInput == "0" and mWestInput == "0" and mEastInput == "0" and mUpInput == "0" and mDownInput == "0" then
        TerminalDialog["6111"].text = "Command executed: \n" .. cmdinputs .. "\n\nDefault command, no input was added to the databanks.\n This is a NOP(no operation) command.\n Input data by gates and push to execute a command"
        return true
    end

    TerminalDialog["6111"].text = "Not a valid command: \n" .. cmdinputs
end

function ClearTerminalDatabanks()
    mNorthInput = "0" mSouthInput = "0" mWestInput = "0" mEastInput = "0" mUpInput = "0" mDownInput = "0"
    TerminalDialog["1111"].text = "Command:\n\n N " .. mNorthInput .. " S " .. mSouthInput .. " E " .. mEastInput .. " W " .. mWestInput .. " U " .. mUpInput .. " D " .. mDownInput

    northInput = 0 southInput = 0 westInput = 0 eastInput = 0 upInput = 0 downInput = 0
    TerminalDialog["2111"].text = "Current databanks content:\n\n N " .. northInput .. "\n S " .. southInput .. "\n E " .. eastInput .. "\n W " .. westInput .. "\n U " .. upInput .. "\n D " .. downInput
end

function PushTerminalData()
    if mNorthInput == "0" then
        mNorthInput = tostring(northInput)
    else
        if selectedOperation == 0 then
            mNorthInput = mNorthInput .. northInput
        elseif selectedOperation == 1 then
            mNorthInput = mNorthInput + northInput
        elseif selectedOperation == 2 then
            mNorthInput = mNorthInput - northInput
        elseif selectedOperation == 3 then
            mNorthInput = mNorthInput * northInput
        end
    end

    if mSouthInput == "0" then
        mSouthInput = tostring(southInput)
    else
        if selectedOperation == 0 then
            mSouthInput = mSouthInput .. southInput
        elseif selectedOperation == 1 then
            mSouthInput = mSouthInput + southInput
        elseif selectedOperation == 2 then
            mSouthInput = mSouthInput - southInput
        elseif selectedOperation == 3 then
            mSouthInput = mSouthInput * southInput
        end
    end
    
    if mWestInput == "0" then
        mWestInput = tostring(westInput)
    else
        if selectedOperation == 0 then
            mWestInput = mWestInput .. westInput
        elseif selectedOperation == 1 then
            mWestInput = mWestInput + westInput
        elseif selectedOperation == 2 then
            mWestInput = mWestInput - westInput
        elseif selectedOperation == 3 then
            mWestInput = mWestInput * westInput
        end
    end

    if mEastInput == "0" then
        mEastInput = tostring(eastInput)
    else
        if selectedOperation == 0 then
            mEastInput = mEastInput .. eastInput
        elseif selectedOperation == 1 then
            mEastInput = mEastInput + eastInput
        elseif selectedOperation == 2 then
            mEastInput = mEastInput - eastInput
        elseif selectedOperation == 3 then
            mEastInput = mEastInput * eastInput
        end
    end

    if mUpInput == "0" then
        mUpInput = tostring(upInput)
    else
        if selectedOperation == 0 then
            mUpInput = mUpInput .. upInput
        elseif selectedOperation == 1 then
            mUpInput = mUpInput + upInput
        elseif selectedOperation == 2 then
            mUpInput = mUpInput - upInput
        elseif selectedOperation == 3 then
            mUpInput = mUpInput * upInput
        end
    end

    if mDownInput == "0" then
        mDownInput = tostring(downInput)
    else
        if selectedOperation == 0 then
            mDownInput = mDownInput .. downInput
        elseif selectedOperation == 1 then
            mDownInput = mDownInput + downInput
        elseif selectedOperation == 2 then
            mDownInput = mDownInput - downInput
        elseif selectedOperation == 3 then
            mDownInput = mDownInput * downInput
        end
    end
    TerminalDialog["1111"].text = "Monolith command:\n\n N " .. mNorthInput .. "\n S " .. mSouthInput .. "\n E " .. mEastInput .. "\n W " .. mWestInput .. "\n U " .. mUpInput .. "\n D " .. mDownInput

    northInput = 0 southInput = 0 westInput = 0 eastInput = 0 upInput = 0 downInput = 0
    TerminalDialog["2111"].text = "Current databanks content:\n\n N " .. northInput .. "\n S " .. southInput .. "\n E " .. eastInput .. "\n W " .. westInput .. "\n U " .. upInput .. "\n D " .. downInput
    if pushedData == false then
        MD_Index = 2
        pushedData = true
    end
end


function ExecuteCommand()
--activate monolith
        if mNorthInput == "1" and mSouthInput == "1" and mWestInput == "0" and mEastInput == "0" and mUpInput == "0" and mDownInput == "0" then
            MD_Index = 3
            ClearTerminalDatabanks()
            return true
        end

--restart
        if mNorthInput == "1" and mSouthInput == "1" and mWestInput == "1" and mEastInput == "1" and mUpInput == "1" and mDownInput == "1" then
            MD_Index = 4
            ClearTerminalDatabanks()
            return true
        end

--exit vr
        if mNorthInput == "0" and mSouthInput == "0" and mWestInput == "0" and mEastInput == "0" and mUpInput == "2" and mDownInput == "0" then
            MD_Index = 4
            ClearTerminalDatabanks()

            guimanager:endDialog()
    
            alphaInitial = 1.1
            world:Clear(true)
            changemapname = "GameMaps/level02/level"
            gstate:set(LEVEL2_STATE)
            changenextstate = LEVEL2_STATE
    
            return true
        end

--activate healing station in cryo room
        if mNorthInput == "1" and mSouthInput == "0" and mWestInput == "0" and mEastInput == "0" and mUpInput == "2" and mDownInput == "1" then
            return true
        end
end

function LibrarianEnd()
    guimanager:endDialog()
    
    alphaInitial = 1.1
    world:Clear(true)
    changemapname = "GameMaps/level02/level"
    gstate:set(LEVEL2_STATE)
	changenextstate = LEVEL2_STATE

    librarianEnd = true
end