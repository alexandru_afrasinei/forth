import("Scripts/Dialogs/DialogsHandlersLevel3.lua")


Monolith_Description_l1 = "I am the Monolith, i will explain how to operate the systens here.\n\nAs you seen there are some teleporters on the ground\nPress E and pass through the N teleporter\n The staging command will be: N 1 S 0 E 0 W 0 U 0 D 0\n By passing throught teleporters you form commands.\n\n Go on try some of this, pass through some teleporters to see what happens.\n When ready go to the terminal on wall and press [Input command data].\n\nCome back to me after. "
Monolith_ActivateMe_l1 = "The staging numbers are appended to the command when pressing:\n[Input command data].\n Once you have the command you want, press [Execute command].\n\n I need you to fully activate my systems asap\n Go to the terminal and press [Clear command] it will clear all data\n Then input/execute the following activation command: \n\n N 1 S 1 E 0 W 0 U 0 D 0 \n\n Come back when this is done."
Monolith_Restart_l1 = "Monolith online. Self test failed.\n This could be solved by a restart command: \n\n N 1 S 1 E 1 W 1 U 1 D 1"
Monolith_Done_l1 = "All internal subsystems are go. This is the VR room\n You can control the ship subsystems from here\n In cryo room there is a red book describing usual commands.\n\n The command to exit VR:\n N 0 S 0 E 0 W 0 U 2 D 0\n Come back at any time."
--Monolith_FirstPrime_l1 = "What is the first prime number ? Input the result in databank S"
--Monolith_Fib_l1 = "What is the 5th element in the Fibonnaci sequence ? Input the result in databank W."
--Monolith_Emergency_l1 = "Alarm alarm!\n Sanity test terminated. Initiate emergency awakening.\nI do not have time to explain.\n Access granted to The Library. To activate the east gate use this command N 0 S 0 E 1 W 0 U 12 D 12"
--Monolith_Offline_l1 = "Make the most or your time here. Farewell. A scrathing noise echos all around.\n The monolith is online but silent, it is totally ignoring you.\n"

MD_Index = 1;
Monolith_Disscussion = {Monolith_Description_l1, Monolith_ActivateMe_l1, Monolith_Restart_l1, Monolith_Done_l1}

Gate_Description_l1 = " A strange inscription. \n As you step on the device you get teleported on other side of the room.\n You notice the INPUT number increased by one."

northInput = 0 southInput = 0 westInput = 0 eastInput = 0 upInput = 0 downInput = 0
mNorthInput = "0" mSouthInput = "0" mWestInput = "0" mEastInput = "0" mUpInput = "0" mDownInput = "0"

--states
pushedData = false
activateCommandExecuted = false
librarianEnd = false
selectedOperation = 0

TerminalDialog = {
    ["11"] = { speaker = "Monolith", text = "Terminal active. Awaiting commands: ", navButtonType = "End Dialog", choices = {"Print command", "Print command input", "Select operation", "Input command data", "Clear command", "Execute command"}, func = UpdateTerminalDialogData},
        ["1111"] = { speaker = "Monolith", text = "Monolith command:\n\n N " .. mNorthInput .. "\n S " .. mSouthInput .. "\n E " .. mEastInput .. "\n W " .. mWestInput .. "\n U " .. mUpInput .. "\n D " .. mDownInput, navButtonType = "End Dialog", choices = {}, func = nil},
        ["2111"] = { speaker = "Monolith", text = "Current databanks content:\n\n N " .. northInput .. "\n S " .. southInput .. "\n E " .. eastInput .. "\n W " .. westInput .. "\n U " .. upInput .. "\n D " .. downInput, navButtonType = "End Dialog", choices = {}, func = nil},
        ["3111"] = { speaker = "Monolith", text = "Pick type of operation performed when pushing data:", navButtonType = "End Dialog", choices = {"Concatenation", "Addition", "Substraction", "Multiplication"}, func = nil},
            ["123111"] = { speaker = "Monolith", text = "Concatenation active", navButtonType = "End Dialog", choices = {}, func = function() selectedOperation = 0 end},
            ["223111"] = { speaker = "Monolith", text = "Addition active", navButtonType = "End Dialog", choices = {}, func = function() selectedOperation = 1 end},
            ["323111"] = { speaker = "Monolith", text = "Substraction active", navButtonType = "End Dialog", choices = {}, func = function() selectedOperation = 2 end},
            ["423111"] = { speaker = "Monolith", text = "Multiplication active", navButtonType = "End Dialog", choices = {}, func = function() selectedOperation = 3 end},
        ["4111"] = { speaker = "Monolith", text = "All data pushed", navButtonType = "End Dialog", choices = {}, func = PushTerminalData},
        ["5111"] = { speaker = "Monolith", text = "Databanks cleared", navButtonType = "End Dialog", choices = {}, func = ClearTerminalDatabanks},
        ["6111"] = { speaker = "Monolith", text = "Execute command", navButtonType = "End Dialog", choices = {}, func = ExecuteCommand}
}

LibraryDialog = {
    ["11"] = {speaker = "Aldrin", text = "Greetings i was expecting your arrival. \nIm the librarian and here to brief and guide you.", navButtonType = "Continue", choices={}, func = nil},
    ["21"] = {speaker = "Aldrin", text = "Radiation alarm!!!!! \n\nSystem failure detected, fault correction subsystem active.\n Library system severely affected.\n Implement physical backup procedure.", navButtonType = "Continue", choices={}, func = nil},
    ["31"] = {speaker = "Aldrin", text = "My databanks are beeing destroyed,\n the Monolith is doing damage control but all systems seem affected", navButtonType = "Continue", choices={}, func = nil},
    ["41"] = {speaker = "Aldrin", text = "I dont know how much time till my core systems will be destroyed, \n i suspect we dont have much time.", navButtonType = "Continue", choices={}, func = nil},
    ["51"] = {speaker = "Aldrin", text = "Pitty i didnt have a human in here for a long time...", navButtonType = "Continue", choices={}, func = nil},
    ["61"] = {speaker = "Aldrin", text = "I dont have the time to explain,\n we were suppose to spend couple of months here for the learning process.", navButtonType = "Continue", choices={}, func = nil},
    ["71"] = {speaker = "Aldrin", text = "Pay attention!\n i know is confusing for you. im uploading the data to a small terminal near cryo room 004.\n Remember this!", navButtonType = "Continue", choices={}, func = nil},
    ["81"] = {speaker = "Aldrin", text = "I dont want to die... protect ..fdsrfdsfd . 004... fsdf... rewr.. fdfsf .island.. .. .", navButtonType = "End Dialog", choices={}, func = LibrarianEnd}
}