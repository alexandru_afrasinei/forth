import("Scripts/Dialogs/DialogsDataLevel1a.lua")
import("Scripts/Dialogs/DialogsDataLevel3.lua")
import("Scripts/Dialogs/LevelsIntro.lua")

GenericTextUi = {}

function GenericTextUi:new(o, context, gui)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui

    self.showFirstReader = true
    self.loadPlayerPosition = true
    self.alphaInitial = 1.1

    self.gfxmode = System:GetGraphicsMode(System:CountGraphicsModes()-1)

    self.gfxmode.x = System:GetProperty("screenwidth",self.gfxmode.x)
    self.gfxmode.y = System:GetProperty("screenheight",self.gfxmode.y)

   return o
end

--function GenericTextUi:ProcessEvent(event, gstate)
--end

function GenericTextUi:ShowStartLevel()
    self.alphaInitial = 1.1
    self.showFirstReader = true
end

function GenericTextUi:Update(event, guiself)
	if self.loadPlayerPosition then
        --Load from disk
        Time:Resume()
        local lpos = gworld:getPlayerPosition()
        local lrot = gworld:getPlayerRotation()

        selfloadPlayerPosition = false
    end

    if guiself.gstate:get() == LEVEL1_STATE or
       guiself.gstate:get() == LEVEL1A_STATE or 
	   guiself.gstate:get() == LEVEL2_STATE or
	   guiself.gstate:get() == LEVEL3_STATE or
	   guiself.gstate:get() == LEVEL4_STATE then

		--Fade in
		self.context:SetBlendMode(Blend.Alpha)
		self.context:SetColor(0, 0, 0, self.alphaInitial);
		self.context:DrawRect(0, 0, self.context:GetWidth(), self.context:GetHeight());

        if self.alphaInitial > 0 then
            self.context:SetFont(guiself.titleFont)
            self.context:SetColor(0.7,0.7,0.7,0.7)

            self.alphaInitial = self.alphaInitial - Time:GetSpeed() * 0.009;
            
            self.context:SetColor(0.7,0.7,0.7,0.7)
            if guiself.gstate:get() == LEVEL1_STATE then
                self.context:DrawText("Dream world - Trials", self.gfxmode.x/3, self.gfxmode.y/7)
            elseif guiself.gstate:get() == LEVEL1A_STATE then
                self.context:DrawText("Dream world - The watchers", self.gfxmode.x/3, self.gfxmode.y/7)
            elseif guiself.gstate:get() == LEVEL2_STATE then
                self.context:DrawText("Cryo room", self.gfxmode.x/3, self.gfxmode.y/7)
            elseif guiself.gstate:get() == LEVEL3_STATE then
                self.context:DrawText("Island control simulation", self.gfxmode.x/3, self.gfxmode.y/7)
            elseif guiself.gstate:get() == LEVEL4_STATE then
                self.context:DrawText("The caves", self.gfxmode.x/3, self.gfxmode.y/7)
            end
        else
            if self.showFirstReader then
                self.context:SetFont(guiself.defaultFont)

                if guiself.gstate:get() == LEVEL2_STATE and librarianEnd == true then
                    return
                end
                guiself:getMainMenu():ShowInGame()
                self.context:GetWindow():ShowMouse()

                if guiself.gstate:get() == LEVEL1_STATE then
                    guiself:loadReaderData(Intro_l1)
                elseif guiself.gstate:get() == LEVEL1A_STATE then
                    guiself:loadReaderData(Intro_l1a)
                elseif guiself.gstate:get() == LEVEL2_STATE then
                    guiself:loadReaderData(Intro_l2)
                elseif guiself.gstate:get() == LEVEL3_STATE then
                    guiself:loadReaderData(Intro_l3)
                elseif guiself.gstate:get() == LEVEL4_STATE then
                    guiself:loadReaderData(Intro_l4)
                end

                guiself:showReader()

                self.showFirstReader = false
            end

            if guiself.gstate:get() == LEVEL1A_STATE then
                self.context:SetColor(0.9,0.1,0.9,0.9)
                self.context:DrawText("Activate health station:", 0, 0)
                self.context:SetColor(0.9,0.9,0.1,0.9)
                self.context:DrawText("N " .. l1aNorthInput0 , 0 , 20)
                self.context:DrawText("S " .. l1aSouthInput0, 0, 40)
                self.context:DrawText("E " .. l1aEastInput0, 0, 60)
                self.context:DrawText("W " .. l1aWestInput0, 0, 80)
                self.context:DrawText("U " .. l1aUpInput0, 0, 100)
                self.context:DrawText("D " .. l1aDownInput0, 0, 120)

                self.context:SetColor(1,1,1,1)
                if l1a_monolith_talk_status == 1 then
                    self.context:SetColor(0.9,0.1,0.9,0.9)
                    self.context:DrawText("Open cryo door:", 0, 160)
                else
                    self.context:DrawText("Unknown command:", 0, 160)
                end
                
                self.context:SetColor(0.9,0.9,0.1,0.9)
                self.context:DrawText("N " .. l1aNorthInput1, 0 , 180)
                self.context:DrawText("S " .. l1aSouthInput1, 0, 200)
                self.context:DrawText("E " .. l1aEastInput1, 0, 220)
                self.context:DrawText("W " .. l1aWestInput1, 0, 240)
                self.context:DrawText("U " .. l1aUpInput1, 0, 260)
                self.context:DrawText("D " .. l1aDownInput1, 0, 280)
               

                self.context:SetColor(1,1,1,1)
                self.context:DrawText("Unknown command:", 0, 320)
                self.context:SetColor(0.9,0.9,0.1,0.9)
                self.context:DrawText("N " .. l1aNorthInput2, 0 , 340)
                self.context:DrawText("S " .. l1aSouthInput2, 0, 360)
                self.context:DrawText("E " .. l1aEastInput2, 0, 380)
                self.context:DrawText("W " .. l1aWestInput2, 0, 400)
                self.context:DrawText("U " .. l1aUpInput2, 0, 420)
                self.context:DrawText("D " .. l1aDownInput2, 0, 440)
            end

            if guiself.gstate:get() == LEVEL3_STATE then
                self.context:SetColor(0.9,0.1,0.9,0.9)
                self.context:DrawText("Staging:", 0, 0)
                self.context:SetColor(0.9,0.9,0.1,0.9)
                self.context:DrawText("N " .. northInput, 0 , 20)
                self.context:DrawText("S " .. southInput, 0, 40)
                self.context:DrawText("E " .. eastInput, 0, 60)
                self.context:DrawText("W " .. westInput, 0, 80)
                self.context:DrawText("U " .. upInput, 0, 100)
                self.context:DrawText("D " .. downInput, 0, 120)

                --[[self.context:SetColor(0.4,0.9,0.3,0.9)
                self.context:DrawText("Interact with the monolith in center of room and the terminal on wall", 0, 200)
                self.context:DrawText("Pass through teleporters to input data into staging command", 0, 230)
                self.context:DrawText("To make the staging command ready to be executed [Input command data]", 0, 250)
                self.context:DrawText("To execute command [Execute command]", 0, 270)--]]

                self.context:SetColor(0.9,0.1,0.9,0.9)
                self.context:DrawText("Command:", 300, 0)
                self.context:SetColor(0.9,0.9,0.1,0.9)
                self.context:DrawText("N " .. mNorthInput, 300 , 20)
                self.context:DrawText("S " .. mSouthInput, 300, 40)
                self.context:DrawText("E " .. mEastInput, 300, 60)
                self.context:DrawText("W " .. mWestInput, 300, 80)
                self.context:DrawText("U " .. mUpInput, 300, 100)
                self.context:DrawText("D " .. mDownInput, 300, 120)
            end
        end
    else
        self.context:SetFont(guiself.defaultFont)
    end
end
