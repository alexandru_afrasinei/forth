import("Scripts/Dialogs/DialogsDataLevel1a.lua")

SimpleTimer = {}

function SimpleTimer:new(o, context, gui, guiself)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui

    self.posx = 0
    self.posy = 0

    self.counter = 0
    self.trigger_time = 0

    self.once = false

    self.guis = guiself

   return o
end

function SimpleTimer:SetPosition(posx, posy)
    self.posx = posx
    self.posy = posy
end

function SimpleTimer:Trigger(seconds)
    self.once = true
    self.posx = self.context:GetWidth() - self.guis.timerFont:GetTextWidth(tostring(seconds))
    self.trigger_time = Time:GetCurrent() + seconds*1000
    counter = Math:Round(self.trigger_time/1000)

    System:Print("Trigger time: " .. self.trigger_time)
end

function SimpleTimer:Execute()
    if self.once then
        if Time:GetCurrent() >= self.trigger_time then
            System:Print("Timer executed: " .. self.trigger_time)

            --if gstate:get() == LEVEL1A_STATE then
                bellerophon_talk_index = 5
           -- end

            self.once = false
            self.trigger_time = 0
        end
    end
end

function SimpleTimer:Update(event)
    if context:GetWindow():KeyHit(Key.Enter) then
        self:Trigger(10000)
    end

    self.context:SetFont(self.guis.timerFont)
    self.context:SetColor(0.9,0.1,0.1,0.9)

    self.context:SetBlendMode(Blend.Alpha)
    if Time:GetCurrent() < self.trigger_time then
        self.context:DrawText(Math:Round(self.trigger_time/1000) - Math:Round(Time:GetCurrent()/1000) , self.posx, self.posy)
    end
    self.context:SetFont(self.guis.defaultFont)

    self:Execute()
end
