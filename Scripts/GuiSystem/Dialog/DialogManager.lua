DialogManager = {}

function DialogManager:new(o, dialogData)
   o = o or {}
   setmetatable(o, self)
   self.__index = self

   self.dindex = 1
   self.depth = 1
   self.dialog = Dialog:new(nil, context, gui, 0, "Test", "test")
   self.dialogData = {}
   return o
end

function DialogManager:getIndex()
   return self.dindex
end

function DialogManager:setIndex(dindex)
    self.dindex = index
end

function DialogManager:getDepth()
    return self.depth
 end
 
 function DialogManager:setDepth(depth)
     self.depth = depth
 end

 function DialogManager:incrementDepth()
    self.depth = self.depth + 1
    print("depth: " .. self.depth)
 end

 function DialogManager:decrementDepth()
    self.depth = self.depth - 1
 end

function DialogManager:build(dialogData)
    self.dialogData = dialogData

    self.dialog:setId(dialogData[self.dindex .. "1"].id)
    self.dialog:setSpeaker(dialogData[self.dindex .. "1"].speaker)
    self.dialog:setText(self.dialogData[self.dindex .. "1"].text)
    self.dialog:setNavButtonType(self.dialogData[self.dindex .. "1"].navButtonType)

    self.dialog:build()

    if self.dialogData[self.dindex .. "1"].choices ~= nil then
        for i,entry in ipairs(self.dialogData[self.dindex .. "1"].choices) do
            self.dialog.qbuttons[i]:SetText(entry)
            self.dialog.qbuttons[i]:Show()
        end
    end

    if self.dialog:getNavButtonType() ~= "None" then
        self.dialog:ShowNavButton()
        if self.dialog:getNavButtonType() == "Continue" then
            self.dialog:HideReturnButton()
        else
            self.dialog:ShowReturnButton()
        end
    end
    self.dialog:HideReturnButton()
    self.dialog:HideNavButton()
end

function DialogManager:load(dialogData)
    self.dialogData = dialogData
    self.dindex = 1
    self.depth = 1
    self.dialog.lastGoto = 11

    self.dialog:setFunction(dialogData[self.dindex .. "1"].func)
    if self.dialog.func ~= nil then
        self.dialog.func()
    end

    self.dialog:setId(dialogData[self.dindex .. "1"].id)
    self.dialog:setSpeaker(dialogData[self.dindex .. "1"].speaker)
    self.dialog:setText(self.dialogData[self.dindex .. "1"].text)
    self.dialog:setNavButtonType(self.dialogData[self.dindex .. "1"].navButtonType)

    for i,b in ipairs(self.dialog.qbuttons) do
        b:Hide()
    end

    --self.dialog:redraw()
    if self.dialogData[self.dindex .. "1"].choices ~= nil then
        for i,entry in ipairs(self.dialogData[self.dindex .. "1"].choices) do
            self.dialog.qbuttons[i]:SetText(entry)
            self.dialog.qbuttons[i]:Show()
        end
    end

    if self.dialog:getNavButtonType() ~= "None" then
        self.dialog:ShowNavButton()
        if self.dialog:getNavButtonType() == "Continue" then
            self.dialog:HideReturnButton()
        elseif self.dialog:getNavButtonType() == "End Dialog" then
            self.dialog:HideReturnButton()
        else
            self.dialog:ShowReturnButton()
        end
    end
    self.dialog:redraw()
end


function DialogManager:goto(index)
        print("index: " .. index)
        
        self.dialog:setId(self.dialogData[index].id)
        self.dialog:setSpeaker(self.dialogData[index].speaker)
        self.dialog:setNavButtonType(self.dialogData[index].navButtonType)
        self.dialog:setText(self.dialogData[index].text);
        self.dialog:setFunction(self.dialogData[index].func)

        for i,b in ipairs(self.dialog.qbuttons) do
            b:Hide()
        end

        self.dialog:redraw()
        if self.dialogData[index].choices ~= nil then
            for i,entry in ipairs(self.dialogData[index].choices) do
                self.dialog.qbuttons[i]:SetText(entry)
                self.dialog.qbuttons[i]:Show()
            end
        end
        if self.dialog:getNavButtonType() ~= "None" then
            self.dialog:ShowNavButton()
            self.dialog:ShowReturnButton()
        end
end

function DialogManager:next()
    if self.dialogData[self.dindex .. "1"].func ~= nil then
        self.dialogData[self.dindex .. "1"].func()
    end
    
    if self.dialogData[self.dindex .. "1"].id == nil and self.dialogData[self.dindex .. "1"].navButtonType ~= "Continue" then
        return true
    end
    self.dindex = self.dindex + 1

    self.dialog:setId(self.dialogData[self.dindex .. "1"].id)
    self.dialog:setSpeaker(self.dialogData[self.dindex .. "1"].speaker)
    self.dialog:setNavButtonType(self.dialogData[self.dindex .. "1"].navButtonType)
    self.dialog:setText(self.dialogData[self.dindex .. "1"].text);

    self.dialog:redraw()
    for i,b in ipairs(self.dialog.qbuttons) do
        self.dialog.qbuttons[i]:Hide()
    end
    if self.dialogData[self.dindex .. "1"].choices ~= nil then
        for i,entry in ipairs(self.dialogData[self.dindex .. "1"].choices) do
            self.dialog.qbuttons[i]:SetText(entry)
            sself.dialog.qbuttons[i]:Show()
        end
    end
end

function DialogManager:getDialog()
    return self.dialog
end

function DialogManager:getData()
    local o = {id=self.dindex}
    return  o
end
