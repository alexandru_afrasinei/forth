Dialog = {}

function Dialog:new(o, context, gui, id, speaker, text)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui
    self.id = id
    
    self.speaker = speaker
    self.text = text
    self.lastSelection = 1
    self.lastGoto = 11
    self.choices = {}
    self.func = nil


    self.dialogpanel = nil
    self.textlabel = nil
    self.qbuttons = nil
    self.navbutton = nil
    self.returnbutton = nil

    self.NavButtonContinueText = "None"
    self.NavButtonContinueText = "Continue"
    self.NavButtonEndDialogueText = "End Dialog"
    self.navButtonType = self.NavButtonContinueText

   return o
end

function Dialog:build()
    self.dialogpanel = Widget:Panel(self.gui:GetBase():GetClientSize().x/4,self.gui:GetBase():GetClientSize().y/1.5,self.gui:GetBase():GetClientSize().x/2,self.gui:GetBase():GetClientSize().y/4,self.gui:GetBase())
    self.dialogpanel:SetAlignment(0,0,0,0)
    self.dialogpanel:SetObject("backgroundcolor",Vec4(0.15,0.15,0.15,1))

    self.text = self.speaker .. " - " .. self.text
    self.textlabel = Widget:Label(self.text,20,20,self.gui:GetBase():GetClientSize().x/2,self.gui:GetBase():GetClientSize().y/4,self.dialogpanel)

    self.q1button = Widget:Button("",20,50,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)
    self.q2button = Widget:Button("",20,80,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)
    self.q3button = Widget:Button("",20,110,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)
    self.q4button = Widget:Button("",20,140,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)
    self.q5button = Widget:Button("",20,170,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)
    self.q6button = Widget:Button("",20,200,self.gui:GetBase():GetClientSize().x/4,25,self.dialogpanel)

    self.qbuttons = {self.q1button, self.q2button, self.q3button, self.q4button, self.q5button, self.q6button}
    for i,b in ipairs(self.qbuttons) do
        b:Hide()
    end
    
    self.navbutton = Widget:Button(self.navButtonType, self.gui:GetBase():GetClientSize().x/2 - 100,self.gui:GetBase():GetSize().y/1.5 + self.gui:GetBase():GetClientSize().y/4,200,30,self.gui:GetBase())
    self.returnbutton = Widget:Button("Back", self.gui:GetBase():GetClientSize().x/2 - 300,self.gui:GetBase():GetSize().y/1.5 + self.gui:GetBase():GetClientSize().y/4,200,30,self.gui:GetBase())
    self.gui:Show()
    
    self.dialogpanel:Hide()
    self.navbutton:Hide()
    self.returnbutton:Hide()
end

function Dialog:redraw()
    self.textlabel:SetText(self.speaker .. " - " .. self.text)
    
    self.navbutton:SetText(self.navButtonType)
    self.textlabel:Redraw()
    self.navbutton:Redraw()
    self.returnbutton:Redraw()

    self.qbuttons = {self.q1button, self.q2button, self.q3button, self.q4button, self.q5button, self.q6button}
    for i,b in ipairs(self.qbuttons) do
        b:Redraw()
    end
end

function Dialog:setId(id)
    self.id = id
end

function Dialog:getId()
    return self.id
end

function Dialog:setSpeaker(speaker)
   self.speaker = speaker
end

function Dialog:getSpeaker()
    return self.speaker
end

function Dialog:setText(text)
   self.text = text
end
 
function Dialog:getText()
    return self.text
end

function Dialog:setFunction(funct)
    self.func = funct
end

function Dialog:setNavButtonType(type)
    self.navButtonType = type
end

function Dialog:getNavButtonType()
    return self.navButtonType
end

function Dialog:setChoices(choices)
    self.choices = choices
end

function Dialog:getChoices()
    return self.choices
end

function Dialog:getQButtons()
    return self.qbuttons
end

function Dialog:Hide()
    self.dialogpanel:Hide()
    self.navbutton:Hide()
    self.returnbutton:Hide()
end

function Dialog:End()
    self.dialogpanel:Hide()
    self.navbutton:Hide()
    self.returnbutton:Hide()
    self.gui:Hide()
    self.context:GetWindow():FlushMouse()
    self.context:GetWindow():FlushKeys()
    self.context:GetWindow():HideMouse()
    self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
    Time:Resume()
end

function Dialog:Show()
    self.dialogpanel:Show()
end

function Dialog:ShowNavButton()
    self.navbutton:Show()
end

function Dialog:HideNavButton()
    self.navbutton:Hide()
end

function Dialog:ShowReturnButton()
    self.returnbutton:Show()
end

function Dialog:HideReturnButton()
    self.returnbutton:Hide()
end

function Dialog:ProcessEvent(event, dialogManager, gstate)
    if event.id == Event.WidgetAction then
        if event.source == self.navbutton then
            if self.navButtonType == self.NavButtonEndDialogueText then
                self:Hide()

                self.gui:Hide()
                self.context:GetWindow():FlushMouse()
                self.context:GetWindow():FlushKeys()
                self.context:GetWindow():HideMouse()
                self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
                Time:Resume()
                dialogManager:next()
                return true
            end

            dialogManager:next()
        end

        if event.source == self.returnbutton then
            dialogManager:decrementDepth()
            if dialogManager:getDepth() == 1 then
                self:HideNavButton()
                self:HideReturnButton()
                guimanager:loadDialogData(TerminalDialog)
                guimanager:showDialog()
        
                gui:GetBase():Redraw()
            end
            return true
        end
        
            if event.source == self.q1button then
                if self.choices ~= nil then
                    self.lastGoto = "1" .. dialogManager:getDepth() .. self.lastGoto
                    print("Last goto: " .. self.lastGoto)
                    dialogManager:goto(self.lastGoto)
                    
                    dialogManager:incrementDepth()
                    self.lastSelection = 1

                    if self.func ~= nil then
                        self.func()
                    end
                end
                
            end
            if event.source == self.q2button then
                if self.choices ~= nil then
                    self.lastGoto = "2" .. dialogManager:getDepth() .. self.lastGoto
                    dialogManager:goto(self.lastGoto)

                    dialogManager:incrementDepth()
                    self.lastSelection = 2

                    if self.func ~= nil then
                        self.func()
                    end    
                end
            end
            if event.source == self.q3button then
                if self.choices ~= nil then
                    self.lastGoto = "3" .. dialogManager:getDepth() .. self.lastGoto
                    dialogManager:goto(self.lastGoto)

                    dialogManager:incrementDepth()
                    self.lastSelection = 3

                    if self.func ~= nil then
                        self.func()
                    end    
                end
            end
            if event.source == self.q4button then
                if self.choices ~= nil then
                    self.lastGoto = "4" .. dialogManager:getDepth() .. self.lastGoto
                    dialogManager:goto(self.lastGoto)

                    dialogManager:incrementDepth()
                    self.lastSelection = 4

                    if self.func ~= nil then
                        self.func()
                    end    
                end
            end
            if event.source == self.q5button then
                if self.choices ~= nil then
                    self.lastGoto = "5" .. dialogManager:getDepth() .. self.lastGoto
                    dialogManager:goto(self.lastGoto)

                    dialogManager:incrementDepth()
                    self.lastSelection = 5

                    if self.func ~= nil then
                        self.func()
                    end    
                end
            end
            if event.source == self.q6button then
                if self.choices ~= nil then
                    self.lastGoto = "6" .. dialogManager:getDepth() .. self.lastGoto
                    dialogManager:goto(self.lastGoto)

                    dialogManager:incrementDepth()
                    self.lastSelection = 6

                    if self.func ~= nil then
                        self.func()
                    end    
                end
            end
    end

    return true
end

function Dialog:Update(event, gstate)
    --if context:GetWindow():KeyHit(Key.Space) then
    --    self:Hide()
     --   return true
    --end
    --[[if context:GetWindow():KeyHit(Key.Escape) then
        gstate:set(MAIN_MENU_STATE)
        return true
    end

    if context:GetWindow():KeyHit(Key.Space) then
        self:Show()
        return true
    end--]]
end

function Dialog:load(data)
    self.id = data.id
    self.speaker = data.speaker
    self.text = data.text
    self.navButtonType = data.navButtonType
    self.choices = data.choices
end

function Dialog:getData()
    local o = { id=self.id, speaker = self.speaker, text = self.text, self.navButtonType,  choices = self.choices }
    return  o
end
