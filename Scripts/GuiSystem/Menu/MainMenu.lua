MainMenu = {}

function MainMenu:new(o, context, gui, image)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui
   return o
end

function MainMenu:build()
	self.continuebutton = Widget:Button("Continue",100,self.gui:GetBase():GetSize().y/2-60,200,30,self.gui:GetBase())
    self.continuebutton:SetAlignment(1,0,0,0)
    if FileSystem:GetFileType("gworld") == 0 then
        self.continuebutton:Hide()
    end

    self.newbutton = Widget:Button("New Game",100,self.gui:GetBase():GetSize().y/2-10,200,30,self.gui:GetBase())
    self.newbutton:SetAlignment(1,0,0,0)

	self.options = Widget:Button("Options",100,self.gui:GetBase():GetSize().y/2+40,200,30,self.gui:GetBase())
	self.options:SetAlignment(1,0,0,0)
	
	self.credits = Widget:Button("Credits",100, self.gui:GetBase():GetSize().y/2+90,200,30,self.gui:GetBase())
	self.credits:SetAlignment(1,0,0,0)

	self.quit = Widget:Button("Exit",100,self.gui:GetBase():GetSize().y/2+140,200,30,self.gui:GetBase())
	self.quit:SetAlignment(1,0,0,0)

	optionspanel = Widget:Panel(self.gui:GetBase():GetClientSize().x/2-250,gui:GetBase():GetClientSize().y/2-300,500,600,self.gui:GetBase())
	optionspanel:SetAlignment(0,0,0,0)
	self.optionspanel=optionspanel
	
	indent=8
	
	--Create a panel
	widget = Widget:Tabber(indent,indent,optionspanel:GetClientSize().x-indent*2,optionspanel:GetClientSize().y-indent*2,optionspanel)
	widget:AddItem("Options",true)
	optionspanel:SetObject("backgroundcolor",Vec4(0.15,0.15,0.15,1))
	
	self.tabber = widget

	local indent = 12
	panel = Widget:Panel(indent,indent,widget:GetClientSize().x-indent*2,widget:GetClientSize().y-indent*2-30,widget)
	panel:SetBool("border",true)
	
	self.panel={}
	self.panel.general=panel

	self.closeoptions = Widget:Button("Close",widget:GetClientSize().x-72-indent,widget:GetClientSize().y-28-5,72,28,widget)
	self.applyoptions = Widget:Button("Apply",widget:GetClientSize().x-72*2-4-indent,widget:GetClientSize().y-28-5,72,28,widget)
	
	local y=20
	local sep=40
	
	--Graphics resolution
	Widget:Label("Screen Resolution",20,y,200,16,panel)
	y=y+16
	self.screenres = Widget:ChoiceBox(20,y,200,30,panel)
	local count = System:CountGraphicsModes()
	local window = self.context:GetWindow()
	for n=0,count-1 do
		local gfx = System:GetGraphicsMode(n)
		local selected=false
		if window:GetWidth()==gfx.x and window:GetHeight()==gfx.y then selected=true end
		self.screenres:AddItem( gfx.x.."x"..gfx.y,selected)
	end
	y=y+sep
	
	--Antialias
	Widget:Label("Antialias",20,y,200,16,panel)
	y=y+16
	self.antialias = Widget:ChoiceBox(20,y,200,30,panel)
	self.antialias:AddItem("None")
	self.antialias:AddItem("2x")
	self.antialias:AddItem("4x")
	y=y+sep
	
	--Texture quality
	Widget:Label("Texture Detail",20,y,200,16,panel)
	y=y+16
	self.texturequality = Widget:ChoiceBox(20,y,200,30,panel)
	self.texturequality:AddItem("Low")
	self.texturequality:AddItem("Medium")
	self.texturequality:AddItem("High")
	self.texturequality:AddItem("Very High")
	y=y+sep
	
	--Lighting quality
	Widget:Label("Lighting Quality",20,y,200,16,panel)
	y=y+16
	self.lightquality = Widget:ChoiceBox(20,y,200,30,panel)
	self.lightquality:AddItem("Low")
	self.lightquality:AddItem("Medium")
	self.lightquality:AddItem("High")
	y=y+sep
	
	--Anisotropy
	Widget:Label("Anisotropic Filter",20,y,200,16,panel)
	y=y+16
	self.afilter = Widget:ChoiceBox(20,y,200,30,panel)
	self.afilter:AddItem("None")
	self.afilter:AddItem("2x")
	self.afilter:AddItem("4x")
	self.afilter:AddItem("8x")
	self.afilter:AddItem("16x")
	self.afilter:AddItem("32x")
	y=y+sep
	
	--Create a checkbox
	self.tfilter = Widget:Button("Trilinear Filter",20,y,200,30,panel)
	self.tfilter:SetString("style","Checkbox")
	y=y+sep
	
	--Create a checkbox
	self.vsync = Widget:Button("Vertical Sync",20,y,200,30,panel)
	self.vsync:SetString("style","Checkbox")
	y=y+sep
	
	optionspanel:Hide()
	
	--Load settings
	local world = World:GetCurrent()
	local quality
	if world~=nil then
		
		quality = tonumber((System:GetProperty("lightquality")))
		if quality~=nil then world:SetLightQuality(quality) end
		
	end
	
	--Texture detail
	quality = tonumber((System:GetProperty("texturedetail")))
	if quality~=nil then Texture:SetDetail(quality) end
	
	--Anisotropic filter
	quality = tonumber((System:GetProperty("anisotropicfilter")))
	if quality~=nil then Texture:SetAnisotropy(quality) end
	
	--TriLinear Filter
	quality = tonumber((System:GetProperty("trilinearfilter")))
	if quality~=nil then
		if quality>0 then
			quality=true
		else
			quality=false
		end
		Texture:SetTrilinearFilterMode(quality)
	end

	--Vertical sync
	quality = tonumber((System:GetProperty("verticalsync")))
	if quality~=nil then
		if quality>0 then
			VSyncMode=true
		else
			VSyncMode=false
		end
    end
    
    changemapname = "GameMaps/menu/level"
end

function MainMenu:Show()
    self.gui:Show()
    self.context:GetWindow():ShowMouse()
end

function MainMenu:ShowInGame()
    self.gui:Show()

    self.continuebutton:Hide()
    self.newbutton:Hide()
    self.credits:Hide()
	self.options:Hide()
    self.quit:Hide()
    
    self.context:GetWindow():ShowMouse()
end

function MainMenu:Hidden()
    return self.gui:Hidden()
end

function MainMenu:Hide()
    self.gui:Hide()

    self.context:GetWindow():HideMouse()
    self.context:GetWindow():FlushMouse()
    self.context:GetWindow():FlushKeys()
    self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
end

function SaveWorldData()
    local lsfp = World:GetCurrent():FindEntity("simplefpsplayer")
    if lsfp ~= nil then 
        local pos = lsfp:GetPosition()
        local rot = lsfp:GetRotation()
        
        gworld:setPlayerLocationId(changenextstate)
        gworld:setPlayerPosition(pos.x, pos.y, pos.z)
        gworld:setPlayerRotation(rot.x, rot.y, rot.z)

        gconf:getSerializer():save(gworld:getData(), "gworld")
    end
end

function MainMenu:GetSettings()
		
    local world = World:GetCurrent()
    local n,i
    
    if VSyncMode==nil then VSyncMode=true end
    self.vsync:SetState(VSyncMode)
    
    --Antialias
    local aa = (System:GetProperty("antialias","1")).."x"
    if aa=="0x" or aa=="1x" then aa="None" end
    for i=0,self.antialias:CountItems()-1 do
        if self.antialias:GetItemText(i)==aa then
            self.antialias:SelectItem(i)
            break
        end
    end
    
    --Screen resolution
    local w = self.context:GetWindow():GetWidth()
    local h = self.context:GetWindow():GetHeight()	
    local gfxmode = tostring(w).."x"..tostring(h)
    self.screenres:SelectItem(-1)
    for n=0,self.screenres:CountItems()-1 do
        if self.screenres:GetItemText(n)==gfxmode then
            self.screenres:SelectItem(n)
            break
        end
    end
    
    local anisotropy = tostring(Texture:GetAnisotropy()).."x"
    if anisotropy=="0x" then anisotropy="None" end
    for n=0,self.afilter:CountItems()-1 do
        if anisotropy==self.afilter:GetItemText(n) then
            self.afilter:SelectItem(n)
            break
        end
    end
    self.tfilter:SetState(Texture:GetTrilinearFilterMode())
    self.lightquality:SelectItem(world:GetLightQuality())
    self.texturequality:SelectItem(self.texturequality:CountItems()-1-Texture:GetDetail())
end

function MainMenu:splitstring(str,sep)
    local array = {}
    local reg = string.format("([^%s]+)",sep)
    for mem in string.gmatch(str,reg) do
        table.insert(array, mem)
    end
    return array
end

function MainMenu:ApplySettings()
    local world = World:GetCurrent()
    local item=nil
    
    --Antialias
    item = self.antialias:GetSelectedItem()
    if item>-1 then
        local aa = self.antialias:GetItemText(item)
        aa = string.gsub(aa,"x","")
        if aa=="None" then aa="1" end
        aa = tonumber(aa)
        local count = world:CountEntities()
        for n=0,count-1 do
            local entity=world:GetEntity(n)
            if entity:GetClass()==Object.CameraClass then
                local camera = tolua.cast(entity,"Camera")
                camera:SetMultisampleMode(aa)
            end
        end
        System:SetProperty("antialias",aa)
    end
    
    --Graphics mode
    item=self.screenres:GetSelectedItem()
    if item>-1 then
        local gfxmode = self.screenres:GetItemText(item)
        gfxmode = self:splitstring(gfxmode,"x")
        local window = self.context:GetWindow()
        if window:GetWidth()~=tonumber(gfxmode[1]) or window:GetHeight()~=tonumber(gfxmode[2]) then
            window:SetLayout(0,0,gfxmode[1],gfxmode[2])
            System:SetProperty("screenwidth",gfxmode[1])
            System:SetProperty("screenheight",gfxmode[2])
        end
    end
    
    --Light quality
    world:SetLightQuality(self.lightquality:GetSelectedItem())
    System:SetProperty("lightquality",self.lightquality:GetSelectedItem())
    
    --Texture detail
    quality = self.texturequality:CountItems()-1-self.texturequality:GetSelectedItem()
    Texture:SetDetail(quality)
    System:SetProperty("texturedetail",quality)
    
    --Anisotropy
    item = self.afilter:GetSelectedItem()
    if item>-1 then
        quality = self.afilter:GetItemText(item)
        quality = string.gsub(quality,"x","")
        if quality=="None" then quality = "0" end
        Texture:SetAnisotropy(tonumber(quality))
        System:SetProperty("anisotropicfilter",quality)
    end
    
    --Trilinear filter
    quality = self.tfilter:GetState()
    Texture:SetTrilinearFilterMode(quality)
    if quality then
        System:SetProperty("trilinearfilter","1")
    else
        System:SetProperty("trilinearfilter","0")
    end		
    
    --Vertical synv
    if (self.vsync:GetState()) then
        VSyncMode=true
        System:SetProperty("verticalsync","1")
    else
        VSyncMode=false
        System:SetProperty("verticalsync","0")
    end
end

function MainMenu:ProcessEvent(event, gstate)

    if event.id == Event.WindowSize then
				
        --if event.source == self.context:GetWindow() then
        --	local sz = self.gui:GetBase():GetSize()
        --	local wsz = self.optionspanel:GetSize()
            --self.optionspanel:SetLayout((sz.x-wsz.x)/2,(sz.y-wsz.y)/2,wsz.x,wsz.y)
        --end
    
    elseif event.id == Event.WidgetSelect then
        

        
    elseif event.id == Event.WidgetAction then
        
        if event.source == self.tabber then
            
            if event.data==0 then
                self.panel.general:Show()
            else
                self.panel.general:Hide()
            end
            
        end
        
        if event.source == self.options then
            self:GetSettings()
            self.tabber:SelectItem(0)
            self:ProcessEvent(Event(Event.WidgetAction,self.tabber,0), gstate)
			self.credits:Disable()
            self.continuebutton:Disable()
            self.newbutton:Disable()
            self.options:Disable()
            self.quit:Disable()
            self.optionspanel:Show()
        
        elseif event.source == self.newbutton then
            if self.newbutton:GetText()=="New Game" then
                guimanager:showSpellselector()
                continue_once = false

                context:SetFont(titleFont)
                if System:GetProperty("loadlevel")~= "" then
                    toload = System:GetProperty("loadlevel")
                    gstate:set(toload)
                    changemapname = "GameMaps/level0" .. toload .. "/level"
                    changenextstate = toload
                else
                    gstate:set(LEVEL1_STATE)
                    changemapname = "GameMaps/level0" .. LEVEL1_STATE .. "/level"
                    changenextstate = LEVEL1_STATE

                    gworld:setPlayerLocationId(changenextstate)
			gworld:setPlayerPosition(0,0,0)
			gworld:setStartPlayerPosition(0,0,0)
			gworld:setStartPlayerRotation(0, 0)

			gworld:addIsland("Principium")
			gworld:addIsland("Secundus")
			gconf:getSerializer():save(gworld:getData(), "gworld")
                end


                self.gui:Hide()
                self.context:GetWindow():FlushMouse()
                self.context:GetWindow():FlushKeys()
                self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
                self.context:GetWindow():HideMouse()

                Time:Resume()
                
                newgame = true
            end

        elseif event.source == self.continuebutton then
            self.context:GetWindow():HideMouse()

            if self.continuebutton:GetText()=="Continue" then
                guimanager:showSpellselector()
                
                context:SetFont(titleFont)
                gstate:set(changenextstate)

                --load map state from save if necessary
                
                    if FileSystem:GetFileType("gworld") ~= 0 then
                        if continue_once then
                            changemapname = "GameMaps/level0" .. gworld:getPlayerLocationId() .. "/level"
                            changenextstate = gworld:getPlayerLocationId()
                            continue_once = false
                        end
                    end

                self.gui:Hide()
                self.context:GetWindow():FlushMouse()
                self.context:GetWindow():FlushKeys()
                self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)

                Time:Resume()
            end
            
            if self.continuebutton:GetText()=="Back" then
                self.gui:Show()

                self.continuebutton:SetText("Continue");
                self.continuebutton:Hide()
                self.newbutton:Show()
                self.options:Show()
                self.credits:Show()
                self.quit:Show()

                self.continuebutton:Redraw()
                self.newbutton:Redraw()
                self.options:Redraw()
                self.credits:Redraw()
                self.quit:Redraw()
                self.context:GetWindow():ShowMouse()

                changemapname = "GameMaps/menu/level"
                changenextstate = LEVEL1_STATE
            end
		elseif event.source == self.credits then
            if self.credits:GetText()=="Credits" then
                SaveWorldData()
                guimanager:hideSpellselector()

                changenextstate = MAIN_MENU_STATE
                gstate:set(MAIN_MENU_STATE)
                changemapname = "GameMaps/credits/level"                

                if FileSystem:GetFileType("gworld") == 0 then
                    self.continuebutton:SetText("Back")
                    self.continuebutton:Show()
                end
                continue_once = true
            end

			Time:Resume()				
						
			
        elseif event.source == self.applyoptions then
            self:ApplySettings()
            self:ProcessEvent(Event(Event.WidgetAction,self.closeoptions), gstate)
            
        elseif event.source == self.closeoptions then
            self.credits:Enable()
            self.continuebutton:Enable()
            self.options:Enable()
            self.quit:Enable()
            self.optionspanel:Hide()
            
        elseif event.source == self.quit then
            SaveWorldData()

            return false
        end
    end

    return true
end

function MainMenu:Update(event, gstate)
		if context:GetWindow():KeyHit(Key.Escape) then
			if self.gui:Hidden() or self.optionspanel:Hidden() then
                if self.credits:GetText()=="Credits" then
					self.gui:Show()
					context:SetFont(gfonts.default)

					guimanager:hideDialog()
                    guimanager:hideReader()
                    guimanager:hideConsole()
					
                    self.continuebutton:Show()
                    self.newbutton:Show()
                    self.options:Show()
                    self.credits:Show()
					self.quit:Show()

                    self.context:GetWindow():ShowMouse()
                elseif self.newbutton:GetText()=="New Game" then
                    gstate:set(changenextstate)

                    self:ProcessEvent(Event(Event.WidgetAction,self.quit), gstate)
                    self.continuebutton:Show()                    
                elseif self.continuebutton:GetText()=="Continue" then
                    gstate:set(changenextstate)

                    self:ProcessEvent(Event(Event.WidgetAction,self.quit), gstate)
                else
                    if self.gui:Hidden() then
                        Time:Pause()

                        self.gui:Show()
                        context:SetFont(gfonts.default)

                        guimanager:hideDialog()
                        guimanager:hideReader()
                        guimanager:hideConsole()

                        self.continuebutton:Enable()
                        self.continuebutton:Show()
                        self.newbutton:Show()
                        self.options:Show()
                        self.credits:Show()
                        self.quit:Show()

                        self.continuebutton:Redraw()
                        self.newbutton:Redraw()
                        self.options:Redraw()
                        self.credits:Redraw()
                        self.quit:Redraw()

                        --self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
                        self.context:GetWindow():ShowMouse()
                    else
                        guimanager:hideDialog()
                        guimanager:hideReader()
                        guimanager:hideConsole()
                        self.gui:Hide()
						self.context:GetWindow():FlushMouse()
						self.context:GetWindow():FlushKeys()
						self.context:GetWindow():HideMouse()
						self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
						
                        gstate:set(changenextstate)

                        Time:Resume()
					end
				end
			else
				self:ProcessEvent(Event(Event.WidgetAction,self.closeoptions), gstate)
			end
		end
end
