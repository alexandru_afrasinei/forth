SpellSelector = {}

function SpellSelector:new(o, context)
    o = o or {}
    setmetatable(o, self)
	self.visible = true
    self.__index = self

    self.context = context

    self.spells={}
    self.spells[1] = Texture:Load("Materials/Forth/Library/Fire Orb2.tex")
    self.spells[2] = Texture:Load("Materials/Forth/Library/Water Orb3.tex")

    self.spelliconsize = context:GetWidth()/20
    self.currentSpell = 1
    self.nrspells = 3
	local world = World:GetCurrent()
	
	return o
end

function SpellSelector:Hide()
    self.visible = false
end

function SpellSelector:Show()
    self.visible = true
end

function SpellSelector:GetSelection()
    return self.currentSpell
end

function SpellSelector:Update(event, gstate)
    if self.visible ~= true then
		return
	end

    if context:GetWindow():KeyHit(Key.Tab) then
        self.currentSpell = self.currentSpell + 1
        if self.currentSpell == self.nrspells then
            self.currentSpell = 1
        end
		if(player) then player.script.currentSpell = self.currentSpell-1 end
    end

    if self.currentSpell == 1 then
        context:SetColor(0.7,0.7,0.7,1)
    else
        context:SetColor(0.7,0.7,0.7,0.3)
    end
    context:DrawImage(self.spells[1],context:GetWidth()/2 - 2 * self.spelliconsize,context:GetHeight()-self.spelliconsize,self.spelliconsize,self.spelliconsize)

    if self.currentSpell == 2 then
        context:SetColor(0.7,0.7,0.7,1)
    else
        context:SetColor(0.7,0.7,0.7,0.3)
    end
    context:DrawImage(self.spells[2],context:GetWidth()/2 - self.spelliconsize,context:GetHeight()-self.spelliconsize,self.spelliconsize,self.spelliconsize)
    
end