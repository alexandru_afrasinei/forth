Reader = {}

function Reader:new(o, context, gui, id, text)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui
    self.id = id

    self.text = text


    self.readerpanel = nil
    self.textlabel = nil
    self.navbutton = nil

    self.navButtonType = "Close"

   return o
end

function Reader:build()
    self.readerpanel = Widget:Panel(self.gui:GetBase():GetClientSize().x/3,self.gui:GetBase():GetClientSize().y/1.5,self.gui:GetBase():GetClientSize().x/3,self.gui:GetBase():GetClientSize().y/4,self.gui:GetBase())
    self.readerpanel:SetAlignment(0,0,0,0)
    self.readerpanel:SetObject("backgroundcolor",Vec4(0.15,0.15,0.15,1))

    self.textlabel = Widget:Label(self.text,20,20,self.gui:GetBase():GetClientSize().x/3-40,self.gui:GetBase():GetClientSize().y/4-40,self.readerpanel)
    
    self.navbutton = Widget:Button(self.navButtonType, self.gui:GetBase():GetClientSize().x/2 - 100,self.gui:GetBase():GetSize().y/1.5 + self.gui:GetBase():GetClientSize().y/4,200,30,self.gui:GetBase())
    self.gui:Show()

    self.readerpanel:Hide()
    self.navbutton:Hide()
end

function Reader:redraw()
    self.textlabel:SetText(self.text)
    self.navbutton:SetText(self.navButtonType)
    
    self.textlabel:Redraw()
    self.navbutton:Redraw()
end

function Reader:setId(id)
    self.id = id
end

function Reader:getId()
    return self.id
end

function Reader:setText(text)
   self.text = text
end
 
function Reader:getText()
    return self.text
end

function Reader:Hide()
    self.readerpanel:Hide()
    self.navbutton:Hide()
end

function Reader:Show()
    self.readerpanel:Show()
    self.navbutton:Show()
end

function Reader:ShowNavButton()
    self.navbutton:Show()
end

function Reader:HideNavButton()
    self.navbutton:Hide()
end

function Reader:ProcessEvent(event, gstate)
    if context:GetWindow():KeyHit(Key.Escape) then
        self:Hide()
    end
    if event.id == Event.WidgetAction then
        if event.source == self.navbutton then
            if self.navButtonType == "Close" then
                self:Hide()

                self.gui:Hide()
                self.context:GetWindow():FlushMouse()
                self.context:GetWindow():FlushKeys()
                self.context:GetWindow():HideMouse()
                self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
                Time:Resume()
                gstate:set(changenextstate)
                return true
            end
        end
    end

    return true
end

function Reader:Update(event, gstate)
    if context:GetWindow():KeyHit(Key.Escape) then
        self:Hide()
        return true
    end
end

function Reader:load(data)
    self.id = data.id
    self.text = data.text
end

function Reader:getData()
    local o = { id=self.id,text = self.text, self.navButtonType }
    return  o
end
