Console = {}

function Console:new(o, context, gui)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.context = context
    self.gui = gui

    self.consolepanel = nil
    self.intro = nil
    self.input = nil --input text as string
    self.output = nil
    self.execbtn = nil

   return o
end

function Console:build()
    self.consolepanel = Widget:Panel(self.gui:GetBase():GetClientSize().x/70,self.gui:GetBase():GetClientSize().y/70,self.gui:GetBase():GetClientSize().x - self.gui:GetBase():GetClientSize().x/30,self.gui:GetBase():GetClientSize().y/7,self.gui:GetBase())
    self.consolepanel:SetAlignment(0,0,0,0)
    self.consolepanel:SetObject("backgroundcolor",Vec4(0.15,0.15,0.15,1))

    self.intro = Widget:Label("Console active ...",10,10,300,30,self.consolepanel)

    self.input = Widget:TextField("",10,40,self.gui:GetBase():GetClientSize().x/4,30,self.consolepanel)
    self.execbtn = Widget:Button("Execute",self.gui:GetBase():GetClientSize().x/4 + 30,40,70,30, self.consolepanel)
    self.output = Widget:Label("",10,80,self.gui:GetBase():GetClientSize().x/4,30,self.consolepanel)

    self.consolepanel:Hide()
end

function Console:redraw()
end

function Console:Hide()
    self.consolepanel:Hide()
    self.input:SetText("")
    self.output:SetText("")
end

function Console:Show()
    self.consolepanel:Show()
    self.input:Show()
end

function Console:HandleMapChange(pMapname)
    if pMapname ~= "" then
		System:Print(">> console input of loading map :" ..pMapname..".map")
		changemapname = tostring(pMapname)
        self.input:SetText("")
		self.output:SetText("map " .. pMapname .. " loaded ok")
    end
end

function Console:HandleInputs(inputtext)
	--string.match(inputtext, "[command]%s+(%S+)") returns string that is after [command] after spacebar
	if inputtext == "exit" then
		return false
	elseif (string.match(inputtext, "loadmap%s+(%S+)")) ~= "" then
		local levelnumber = tonumber(string.match(inputtext, "loadmap%s+(%S+)"))
		if(levelnumber) then
			self:HandleMapChange("GameMaps/level"..string.match(inputtext, "loadmap%s+(%S+)").."/level") return true
		else
			self:HandleMapChange(string.match(inputtext, "loadmap%s+(%S+)")) return true
		end
	end
end

function Console:ProcessEvent(event, gstate)
	
    if context:GetWindow():KeyHit(Key.Escape) then
        self:Hide()
    end

    if context:GetWindow():KeyHit(Key.Enter) then
		local inputtext =  self.input:GetText()
        if(self:HandleInputs(self.input:GetText())) == false then return false end
    end
    
    if event.id == Event.WidgetAction then
        if event.source == self.execbtn then
            if self.execbtn:GetText() == "Execute" then
                if(self:HandleInputs(self.input:GetText())) == false then return false end
            end
            --[[if self.navButtonType == "Close" then
                self:Hide()

                self.gui:Hide()
                self.context:GetWindow():FlushMouse()
                self.context:GetWindow():FlushKeys()
                self.context:GetWindow():HideMouse()
                self.context:GetWindow():SetMousePosition(self.context:GetWidth()/2,self.context:GetHeight()/2)
                Time:Resume()
                gstate:set(LEVEL1_STATE)
                return true
            end--]]
        end
    end

    return true
end

function Console:Update(event, gstate)
    if gstate:get() ~= MAIN_MENU_STATE then 
        if context:GetWindow():KeyHit(Key.Escape) then
            self:Hide()
            return true
        end
		if(showstats) then
			if (window:KeyHit(Key.C)) then
				Time:Pause()
				context:SetFont(defaultFont)
				guimanager:getMainMenu():ShowInGame()
				context:GetWindow():ShowMouse()
				guimanager:showConsole()
				gui:GetBase():Redraw()
			end
		end
    end
end

function Console:getData()
    local o = { id=self.id,text = self.text, self.navButtonType }
    return  o
end