import("Scripts/GuiSystem/Menu/MainMenu.lua")
import("Scripts/GuiSystem/Dialog/Dialog.lua")
import("Scripts/GuiSystem/Dialog/DialogManager.lua")
import("Scripts/GuiSystem/Reader/Reader.lua")
import("Scripts/GuiSystem/Console/Console.lua")
import("Scripts/GuiSystem/SpellSelector/SpellSelector.lua")
import("Scripts/GuiSystem/GenericTextUi/GenericTextUi.lua")
import("Scripts/GuiSystem/Timer/SimpleTimer.lua")

GuiManager = {}

function GuiManager:new(o, context, gui, gstate, gfonts)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.gstate = gstate
    self.alphaInitial = 1.0

    local scale = 1
	gui:Hide()
    gui:SetScale(scale)
    	
	gui:GetBase():SetScript("Scripts/GUI/Panel.lua")
    gui:GetBase():SetObject("backgroundcolor",Vec4(0,0,0,0.5))

    self.context = context

    self.mainMenuManager = MainMenu:new(nil, context, gui)
    self.mainMenuManager:build()

    self.dialogManager = DialogManager:new(nil)

    self.reader = Reader:new(nil, context, gui, 1, "")
    self.reader:build()

    self.console = Console:new(nil, context, gui)
    self.console:build()

    self.spellselector = SpellSelector:new(nil, context)
    self.spellselector:Hide()
    self.generictextui = GenericTextUi:new(nil, context)

    self.defaultFont = gfonts.default
    self.titleFont = gfonts.title
    self.timerFont = gfonts.timer

    context:SetFont(self.defaultFont)

    self.simpletimer = SimpleTimer:new(nil, context, gui, self)

   return o
end

function GuiManager:getDialog()
    return self.dialogManager
end

function GuiManager:buildDialog(data)
    return self.dialogManager:build(data)
end

function GuiManager:loadDialogData(data)
    self.dialogManager:load(data)
    self.dialogManager:getDialog():redraw()
end

function GuiManager:loadReaderData(data)
    self.reader:setText(data)
    self.reader:redraw()
end

function GuiManager:showDialog()
    if self.reader ~= nil then
        self.reader:Hide()
    end
    self.dialogManager:getDialog():Show()
end

function GuiManager:hideDialog()
    self.dialogManager:getDialog():Hide()
end

function GuiManager:endDialog()
    self.dialogManager:getDialog():End()
end

function GuiManager:showReader()
    if self.dialogManager:getDialog() ~= nil then
        self.dialogManager:getDialog():Hide()
    end
    self.reader:Show()
end

function GuiManager:hideReader()
    self.reader:Hide()
end

function GuiManager:showConsole()
    if self.dialogManager:getDialog() ~= nil then
        self.dialogManager:getDialog():Hide()
    end
    self.console:Show()
end

function GuiManager:hideConsole()
    self.console:Hide()
end

function GuiManager:showSpellselector()
    self.spellselector:Show()
end

function GuiManager:hideSpellselector()
    self.spellselector:Hide()
end


function GuiManager:getMainMenu()
    return self.mainMenuManager
end

function GuiManager:getSpellSelection()
    return self.spellselector:GetSelection()
    
end

function GuiManager:UpdateAfterRender()
    self.generictextui:Update(event, self)
    self.spellselector:Update(event, self.gstate)
    self.simpletimer:Update(event)
end

function GuiManager:ShowStartLevel()
    self.generictextui:ShowStartLevel()
end

function GuiManager:TriggerTimer(seconds)
    self.simpletimer:Trigger(seconds)
end

function GuiManager:TimerSetPostion(x, y)
    self.simpletimer:SetPosition(x, y)
end

function GuiManager:Update()
    self.mainMenuManager:Update(event, self.gstate)
    self.dialogManager:getDialog():Update(event, self.gstate)
    self.reader:Update(event, self.gstate)
    self.console:Update(event, self.gstate)
    
    --[[if context:GetWindow():KeyHit(Key.Space) then
        self:loadReaderData("This is the reader\n test testing testing testing\n more text here  \n even more text \n boom")
        self:showReader()
        return true
    end

    if context:GetWindow():KeyHit(Key.Enter) then
        self:loadDialogData(TestOneChoice)
        self:showDialog()
        return true
    end--]]

    while EventQueue:Peek() do
        local event = EventQueue:Wait()

        if self.mainMenuManager:ProcessEvent(event, self.gstate)==false then return false end
        if self.dialogManager:getDialog():ProcessEvent(event, self.dialogManager, self.gstate)==false then return false end
        if self.reader:ProcessEvent(event, self.gstate)==false then return false end
        if self.console:ProcessEvent(event, self.gstate)==false then return false end
    end

    return true
end
