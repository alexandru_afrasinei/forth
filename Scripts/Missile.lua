Missile = {}

function Missile:new(o, mexpl, mtrail, pos, direction, launch)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self:Init()
    self.mexpl = mexpl
    self.mtrail = mtrail
    self:SetPosition(pos);
    self:SetDirection(direction);

    if launch == true then
        nd = self.targetDirection:Normalize()
        --[[self.emtrail = Emitter:Create(100)
        self.emtrail:SetMaterial(self.mtrail)
        self.emtrail:SetEmissionVolume(Vec3(.0005, .0005, .0005))
        self.emtrail:SetDuration(500)
        self.emtrail:SetVelocity(-nd[0], -nd[1], -nd[2], 0)
        self.emtrail:SetEmissionShape(1)
        self.emtrail:ClearScaleControlPoints() 
        self.emtrail:AddScaleControlPoint(.3, 1)       
        self.emtrail:AddScaleControlPoint(1, 0)
        self.emtrail:SetRotationSpeed(30)
        self.emtrail:SetLoopMode(true)--]]

        self.model:AddForce(direction[0] - pos[0], direction[1] - pos[1], direction[2] - pos[2], true)
    end

    return o
end

function Missile:Init()
    self.model = Model:Box(0.1, 0.1, 0.1)
    self.model:SetColor(0.0, 0.3, 0.0)
    self.mass = 0.09
    self.model:SetMass(self.mass)
    self.model:SetGravityMode(false)
    local shape = Shape:Box(0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1)
    self.model:SetShape(shape)
    shape:Release()
    self.creationTime = Time:Millisecs()
    self.detonationTime = -1
end

function Missile:Effect()
    self.emtrail:SetLoopMode(false)

    nd = self.targetDirection:Normalize()
    self.emexpl = Emitter:Create(200)
    self.emexpl:SetMaterial(self.mexpl)
    self.emexpl:SetEmissionShape(1)
    self.emexpl:SetEmissionVolume(Vec3(1.30, 1.30, 1.30))
    self.emexpl:SetDuration(500)
    self.emexpl:SetVelocity(nd[0],nd[1],nd[2],0)
    self.emexpl:SetColor(.0, .03, .0, .5, 0)
    self.emexpl:SetColor(.0, .10, .0, 0.4, 1)
    self.emtrail:SetRotationSpeed(500)
    self.emexpl:SetLoopMode(false)
end

function Missile:SetDetonationTime(detonationTime)
    self.detonationTime = self.creationTime + detonationTime
end

function Missile:SetPosition(pos)
    self.model:SetPosition(pos)
end

function Missile:SetDirection(targetDirection)
    self.targetDirection = targetDirection;
end

function Missile:SetMass(mass)
    self.mass = mass
end

function Missile:GetPosition()
    return self.model:GetPosition();
end

function Missile:GetTargetPosition()
    return self.targetDirection
end

function Missile:Launch()
    self.model:AddForce(self.targetDirection.x, self.targetDirection.y, self.targetDirection.z, false)
end

function Missile:Loop()
    --self.emtrail:SetPosition(self:GetPosition().x, self:GetPosition().y, self:GetPosition().z,true)

    if self.detonationTime > 0 then
        if Time:Millisecs() >= self.detonationTime then
        --self:Effect()
        --self.emexpl:SetPosition(self:GetPosition().x, self:GetPosition().y, self:GetPosition().z, true)

        self.model:Hide()
        selfdetonationTime = -1
        end
    else 
        if self.model:GetPosition():DistanceToPoint(self.targetDirection) < 0.6 then
            --self:Effect()
            --self.emexpl:SetPosition(self.model:GetPosition().x, self.model:GetPosition().y, self.model:GetPosition().z, true)
            self.model:Hide()
            --self.emtrail:Hide()
        end
    end
end