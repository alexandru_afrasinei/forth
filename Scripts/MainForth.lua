import("Scripts/Utils/Serializer.lua")
import("Scripts/Utils/Configuration.lua")
import("Scripts/GameWorld.lua")
import("Scripts/GameState.lua")
import("Scripts/GuiSystem/GuiManager.lua")
import("Scripts/Dialogs/DialogsDataLevel1a.lua")
import("Scripts/Dialogs/DialogsDataLevel2.lua")
import("Scripts/Dialogs/DialogsDataLevel3.lua")
import("Scripts/Missile.lua")
import("Scripts/Objects/Physics/globalFunctionsForSpells.lua")

title="Forth"

--default game start , dont modify
changemapname = nil
changenextstate = LEVEL1_STATE

--Game state
gstate = GameState:new(nil)
gstate:set(MAIN_MENU_STATE)

continue_once = true

--Game config
gconf = Configuration:new(nil, "gconfig")
if FileSystem:GetFileType("gconfig") == 0 then
	-- this is the default config
	gconf:getConfig()["sound"] = 1
	gconf:getConfig()["fullscreen"] = 1
	gconf:save()
end
gconf:load()

--Game world
gworld = GameWorld:new(nil)

mtrail = Material:Load("Materials/Effects/smoke.mat");

--Create a window
local windowstyle = 0
local winwidth
local winheight
local gfxmode = System:GetGraphicsMode(System:CountGraphicsModes()-1)

if System:GetProperty("devmode")=="1" then
	gfxmode.x = math.min(1280,gfxmode.x)
	gfxmode.y = Math:Round(gfxmode.x * 9 / 16)
	windowstyle = Window.Titlebar
else
	gfxmode.x = System:GetProperty("screenwidth",gfxmode.x)
	gfxmode.y = System:GetProperty("screenheight",gfxmode.y)
	windowstyle = Window.Fullscreen
end

if System:GetProperty("loadlevel")~= "" then
	os.remove("gworld")
	System:Print("test: " .. System:GetProperty("loadlevel"))
	changenextstate = System:GetProperty("loadlevel")
end

window=Window:Create(title,0,0,gfxmode.x,gfxmode.y,windowstyle)

context=Context:Create(window,0)
if context==nil then return end

camera = {}

gfonts = {}
gfonts.default = Font:Load("Fonts/Arial.ttf",12)
gfonts.title = Font:Load("Fonts/Arial.ttf",20)
gfonts.timer = Font:Load("Fonts/Arial.ttf",30)
context:SetFont(gfonts.default)

world=World:Create()
gui = GUI:Create(context) 

-- Gui manager
guimanager = GuiManager:new(nil, context, gui, gstate, gfonts)
guimanager:buildDialog(TerminalDialog)

player = nil --variable to store the player
--write here where it gets accessed at
	-- SpellSelector.lua

-- Loop
while window:Closed()==false do
	
	if guimanager:Update()==false then return end

	--Handle map change
	if changemapname~=nil then
		System:Print("INFO - Loading new map : "..tostring(changemapname))
		
		Time:Pause()
		System:GCSuspend()		
		world:Clear()

		if Map:Load("Maps/"..changemapname..".map")==false then return end

		--no gworld, create gworld with default data, feed loadlevel cmdline option
		if FileSystem:GetFileType("gworld") == 0 then
			gworld:setPlayerLocationId(changenextstate)
			gworld:setPlayerPosition(0,0,0)
			gworld:setStartPlayerPosition(0,0,0)
			gworld:setStartPlayerRotation(0, 0)

			gworld:addIsland("Principium")
			gworld:addIsland("Secundus")
			gconf:getSerializer():save(gworld:getData(), "gworld")
		end

		System:GCResume()
		Time:Resume()

		gworld:load()
		changenextstate  = gworld:getPlayerLocationId();

		if FileSystem:GetFileType("gworld") ~= 0 then
			local lsfp = World:GetCurrent():FindEntity("simplefpsplayer")
			if lsfp ~= nil then                 			
				if newgame then
					lsfp:SetPosition(gworld:getPlayerData()[4].sx, gworld:getPlayerData()[4].sy, gworld:getPlayerData()[4].sz)
					lsfp.script.camRotation.x = gworld:getPlayerData()[5].srx
					lsfp.script.camRotation.y = gworld:getPlayerData()[5].sry
					newgame = false
				else
					lsfp:SetPosition(gworld:getPlayerData()[2].x, gworld:getPlayerData()[2].y, gworld:getPlayerData()[2].z)
					lsfp.script.camRotation.x = gworld:getPlayerData()[3].rx
					lsfp.script.camRotation.y = gworld:getPlayerData()[3].ry
				end
			end
		end
		
		changemapname = nil
	end	
	
	Time:Update()

	if gstate:get() == MAIN_MENU_STATE then
		world:Update()
	else
		if guimanager:getMainMenu():Hidden() then
			world:Update()
		end
	end	

	--Render the world
	world:Render()
	
	if guimanager:UpdateAfterRender()==false then return end
	
	--Render statistics

	--Toggle statistics on and off
	if (window:KeyHit(Key.F11)) then
		 showstats = not showstats
	end
	if showstats then
		context:SetBlendMode(Blend.Alpha)
		context:SetColor(1,0,0,1)
		context:DrawText("Debug Mode",2,2)
		context:SetColor(1,1,1,1)
		context:DrawStats(2,22)
		context:SetBlendMode(Blend.Solid)
	end

	if mis ~= nil then
		mis:Loop()
	end
	
	--Refresh the screen
	if VSyncMode==nil then VSyncMode=true end
	context:Sync(VSyncMode)
	
end