GameState = {}

MAIN_MENU_STATE = 0
LEVEL1_STATE = "1"
LEVEL1A_STATE = "1a"
LEVEL2_STATE = "2"
LEVEL3_STATE = "3"
LEVEL4_STATE = "4"

function GameState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    self.state = MAIN_MENU_STATE

    --level 3 , activated teleport index 
    self.activatedTeleportIndex = 0

    return o
end

function GameState:get()
    return self.state
end

function GameState:set(state)
    self.state = state
end

function GameState:getData()
    local o = {state=self.state}
    return  o
end
