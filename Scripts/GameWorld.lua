import("Scripts/Island.lua")


GameWorld = {}

function GameWorld:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self

   --Make sure to document the structure below when you add/modify
   self.data = {player = {1, {x = 0, y = 0, z = 0}, {rx = 0, ry = 0} , {sx = 0, sy = 0, sz = 0}, {srx = 0, sry = 0}} ,
                level1 ={},
                level01a = {genstatus = 0, tele1status = 0, tele2status = 0, allclipsreadstatus = 0, pillarpointerpos = 0},
                level2 = {},
                level3 = {},
                nrislands = 0,
                islands = {},
				levelstate=""
				}
   -- player {level id, {position}, {rotation}, {map original position}, {map original rotation}}
   -- level1 {}
   -- level01 { genstatus - generator status,
   --           tele1status - teleporter 1 status
   --           tele2status - teleporter 2 status
   --           allclipsreadstatus,
   --           pillarpointerpos - [0,1,2,3] central pillar pointer position}
   -- level2 {}
   -- level 3 {}
   -- nr islands
   -- islands {}
	-- levelstate 
   return o
end


function GameWorld:setLevelState(state)
    self.data["levelstate"] = tostring(state)
	System:Print("setLevelState ="..self.data["levelstate"])
end

function GameWorld:getLevelState(state)
    return self.data["levelstate"]
end

function GameWorld:addIsland(name)
    self.data["nrislands"] = self.data["nrislands"] + 1
    
    local ni = Island:new(nil, self.data["nrislands"], name):getData()
    table.insert(self.data["islands"], ni)
end

function GameWorld:getIslands()
    return self.data["islands"]
end

function GameWorld:getNrIslands()
    return self.data["nrislands"]
end

function GameWorld:setPlayerLocationId(id)
    self.data["player"][1] = id
end

function GameWorld:getPlayerLocationId()
    return self.data["player"][1]
end


function GameWorld:getPlayerPosition()
    return self.data["player"][2]
end

function GameWorld:setPlayerPosition(x, y, z)
    self.data["player"][2].x = x
    self.data["player"][2].y = y
    self.data["player"][2].z = z
end

function GameWorld:getPlayerRotation()
    return self.data["player"][3]
end

function GameWorld:setPlayerRotation(rx, ry)
    self.data["player"][3].rx = rx
    self.data["player"][3].ry = ry
end

function GameWorld:getStartPlayerPosition()
    return self.data["player"][4]
end

function GameWorld:setStartPlayerPosition(x, y, z)
    self.data["player"][4].sx = x
    self.data["player"][4].sy = y
    self.data["player"][4].sz = z
end

function GameWorld:getStartPlayerRotation()
    return self.data["player"][5]
end

function GameWorld:setStartPlayerRotation(rx, ry)
    self.data["player"][5].rsx = rx
    self.data["player"][5].rsy = ry
end

function GameWorld:getPlayerData()
    return self.data["player"]
end

function GameWorld:getLevel1aData()
    return self.data["level01a"]
end

function GameWorld:getLevel1aDataPillarPointerPos()
    return self.data["level01a"]["pillarpointerpos"]
end

function GameWorld:setLevel1DataPillarPointerPos(pos)
    self.data["level01a"]["pillarpointerpos"] = pos
end

function GameWorld:load()
	local stream = FileSystem:ReadFile("gworld")
	local data = ""
	if (stream) then
		data = stream:ReadLine(); 
		stream:Release() 
	end
	local fun, err = loadstring(data)
	if err then error(err) end
	self.data = fun()
end

function GameWorld:getData()
    return self.data
end