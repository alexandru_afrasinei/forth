
Island = {id = 0,
          name = ""}

function Island:new(o, id, name)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   self.id = id
   self.name = name
   return o
end

function Island:getId()
   return self.id
end

function Island:getName()
    return self.name
end

function Island:getData()
    local o = {id=self.id, name=self.name}
    return  o
end
